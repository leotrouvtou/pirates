from . import DBAccess as DB
from . import DD
from . import sail
from . import boat as boatutils
from . import events
from . import building
from . import maputils
from flask import flash
import math

def getTargets(boatid):
    targets = []
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    boattargets = DB.request_db('sql/getBoats.sql', (boat['lat']-boat['range'], boat['lat']+boat['range'], boat['long']-boat['range'], boat['long']+boat['range'], boatid))
    harbourtargets = DB.request_db('sql/getHarbourTargets.sql', (boat['lat']-boat['range'], boat['lat']+boat['range'], boat['long']-boat['range'], boat['long']+boat['range']))

    for boattarget in boattargets:
        item={}
        item['name']='Bateau -'+boattarget['boatname']
        item['id']='b-'+str(boattarget['id'])
        targets.append(item)
    for harbour in harbourtargets:
        item={}
        item['name']='Port   -'+harbour['name']
        item['id']='h-'+str(harbour['id'])
        targets.append(item)
    return targets


def shootByCanon(targetid, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if "b" == targetid.split('-')[0]:
        target = DB.request_db('sql/getBoat.sql', (targetid.split('-')[1],))[0]
        shootB2B(target, boat)
    else:
        target = DB.request_db('sql/getHarbour.sql', (targetid.split('-')[1],))[0]
        shootB2H(target, boat)

def useMunition(boat):
    ressource=boatutils.getRessourceInHold('Munition', boat['stockid'])
    DB.write_db('sql/decrementRessource.sql', (1, ressource['id']))
    if ressource['number']==1:
        DB.write_db('sql/deleteRessource.sql', (ressource['id'],))

def shootB2B(target, boat):
    check = checkCanFire(target, boat)
    if check:
        result = hasTouched(boat, target)
        DB.write_db('sql/decrementActionPoint.sql', (4, boat['id']))
        if result['succeed']==1:
            damage = calculateDamage(boat, target, result)
            DB.write_db('sql/updateBoatDamage.sql', {'targetid':target['id'], 'damage':damage})
            target = DB.request_db('sql/getBoat.sql', (target['id'],))[0]
            xp=getAndIncrementXP(boat, target)
            events.addEventShootB2B(boat, target, damage, result,  xp)
            if target['boatstructure']>0:
                flash("La cible "+target['boatname']+" a été touchée  ("+str(result['value'])+"/"+str(result['precision'])+") et a pris "+str(damage)+" points de structures."+result['comment']+"<button class='btn btn-danger' onclick='shootAgainByCanon("+'"b-'+str(target['id'])+'"'+")'>Tirer encore!</button>")
            else:
                events.addEventSinkB2B(boat, target)
                boatutils.destroyBoat(target)
                flash( "La cible "+target['boatname']+" a pris "+str(damage)+" points de structures  ("+str(result['value'])+"/"+str(result['precision'])+") et a été coulée."+result['comment'])
        else:
            xp= getAndIncrementXPMissed(boat)
            if not result['crit']:
                events.addEventShootMissB2B(boat, target, result, xp)
                flash("La cible "+target['boatname']+" a été manquée ("+str(result['value'])+"/"+str(result['precision'])+")."+result['comment']+" <button class='btn btn-danger' onclick='shootAgainByCanon("+'"b-'+str(target['id'])+'"'+")'>Tirer encore!</button>")
            else:
                events.addEventShootMissCritB2B(boat, target, result, xp)
                flash("La cible "+target['boatname']+" a été manquée ("+str(result['value'])+"/"+str(result['precision'])+"). Un canon a explosé."+result['comment']+"<button class='btn btn-danger' onclick='shootAgainByCanon("+'"b-'+str(target['id'])+'"'+")'>Tirer encore!</button>")
                DB.write_db('sql/decrementCanon.sql', (boat['id'],))
        if target['fakeside']!='' and target['fakeside'] is not None:
            boatutils.removeFlag(target['id'])
            events.addEventBoatRemoveFlag(target)


def shootH2B(target, harbour):
    check = True
    event="\n"
    if check:
        result = harbourHasTouched(target)
        if result['succeed']==1:
            damage = calculateDamageHarbour(harbour, target,  result)
            DB.write_db('sql/updateBoatDamage.sql', {'targetid':target['id'], 'damage':damage})
            target = DB.request_db('sql/getBoat.sql', (target['id'],))[0]
            events.addEventShootH2B(harbour, target, result, damage)
            if target['boatstructure']==0:
                boatutils.destroyBoat(target)
                events.addEventSinkH2B(harbour, target)
        else:
            events.addEventShootMissH2B(harbour, target, result)

def shootB2H(target, boat):
    check = checkCanFire(target, boat)
    event="\n"
    if check:
        result = hasTouched(boat, target)
        DB.write_db('sql/decrementActionPoint.sql', (4, boat['id']))
        if result['succeed']==1:
            damage = calculateDamage(boat, target, result)
            event = building.updateHarbourDamage(target, damage, event)
            oldside=target['side']
            xp = getAndIncrementXP(boat, 'harbour')
            events.addEventShootB2H(boat, target, damage, result, event, xp)
            building.calculateSide(target)
            target = DB.request_db('sql/getHarbour.sql', (target['id'],))[0]
            if target['defense']>target['maxpop']*0.1 and oldside==target['side']:
                flash("Le port "+target['name']+" a été touché et a pris "+str(damage)+" points de défense ("+str(result['value'])+"/"+str(result['precision'])+")."+result['comment']+"<button class='btn btn-danger' onclick='shootAgainByCanon("+'"h-'+str(target['id'])+'"'+")'>Tirer encore!</button>. Il a riposté")
                shootH2B(boat, target)
            else:
                if oldside!=target['side']:
                    flash( "Le port "+target['name']+" a pris "+str(damage)+" points de structures et maintenant "+target['side']+"."+result['comment'])
                else:
                    building.surrenderHarbour(target)
                    flash( "Le port "+target['name']+" a pris "+str(damage)+" points de structures et se rend."+result['comment'])
                    events.addEventSurrenderHarbour(boat, target)
        else:
            xp= getAndIncrementXPMissed(boat)
            if not result['crit']:
                events.addEventShootMissB2H(boat, target, result, xp)
                flash("Le port "+target['name']+" a été manquée ("+str(result['value'])+"/"+str(result['precision'])+")."+result['comment']+"<button class='btn btn-danger' onclick='shootAgainByCanon("+'"h-'+str(target['id'])+'"'+")'>Tirer encore!</button>")
            else:
                events.addEventShootMissCritB2H(boat, target, result, xp)
                flash("Le port "+target['name']+" a été manquée ("+str(result['value'])+"/"+str(result['precision'])+"). Un canon a explosé."+result['comment']+"<button class='btn btn-danger' onclick='shootAgainByCanon("+'"h-'+str(target['id'])+'"'+")'>Tirer encore!</button>")
                DB.write_db('sql/decrementCanon.sql', (boat['id'],))
            if target['defense']>target['maxpop']*0.1:
                shootH2B(boat, target)

def checkCanFire(target, boat):
    if maputils.getDistance(boat['lat'],boat['long'],  target['lat'], target['long'])>boat['range']:
        flash("La cible "+target['name']+" est trop loin")
        return False
    if  boat['actionpoint']<4 :
        flash("vous n'avez plus assez de point d'action ("+str(boat['actionpoint'])+")")
        return False
    return True

def calculateDamage(boat, target, result):
    if result['munition']:
        damage = DD.dice(3, boat['canon'])
    else:
        damage= DD.dice(1, boat['canon'])
    if result['crit']:
        damage=critDamage(damage)
    damage = changeDamageOnDist(damage, boat['lat'], boat['long'],  target['lat'], target['long'])

    return  math.ceil(damage)

def changeDamageOnDist(damage, lat1, long1, lat2, long2):
    dist = maputils.getDistance(lat1,long1, lat2, long2)
    if dist>1:
        if dist>2:
            damage=damage*0.5
        else:
            damage = damage*0.75
    return damage

def calculateDamageHarbour(harbour, target,  result):
    damage = DD.dice(3, harbour['canon'])
    if result['crit']:
        damage=critDamage(damage)
    if maputils.getDistance(harbour['lat'],harbour['long'],  target['lat'], target['long'])==0:
        damage=critDamage(damage)
    else:
        changeDamageOnDist(damage, harbour['lat'],harbour['long'],  target['lat'], target['long'])
    return math.ceil(damage)

def critDamage(damage):
    return damage*1.5

def harbourHasTouched(target):
    precision = 90
    result = DD.didSucceed(precision, 100)
    result['precision']=precision
    return result

def hasTouched(boat, target):
    precision = getPrecision(boat, target)
    result = DD.didSucceed(precision, 100)
    if boatutils.getOrCreateBoatWithStockid(boat) and boatutils.getRessourceInHold('Munition', boat['stockid']):
        useMunition(boat)
        result['munition']=True
        result['comment']=" Vous avez utilisé un tonneau de munition."
    else:
        result['comment']=" Vous avez chargé le canon avec des fourchettes."
        result['munition']=False
    result['precision']=precision
    return result

def getPrecision(boat, target):
    if not maputils.sameCase(boat['lat'],boat['long'],  target['lat'], target['long']):
        angle = maputils.getAngle(boat['lat'],boat['long'],  target['lat'], target['long'])
        diff = calculateDiff(angle, boat['lastorientation'])
        precision = math.ceil(int(boat['precision'])-diff/4.5)
    else:
        precision = int(boat['precision'])
    return precision

def angleDist(alpha, beta):
    phi = abs(beta - alpha) % 360
    if phi > 180:
        return 360 - phi
    return phi

def calculateDiff(angle, lastorientation):
    return min(abs(angleDist(int(lastorientation)-90,angle)),abs(angleDist(int(lastorientation)+90,angle)))


def getAndIncrementXP(boat, target):
    xp=10
    if target!="harbour" and target['xp']<boat['xp']*0.9:
        xp = 2
    DB.write_db('sql/incrementXP.sql', {'boatid':boat['id'], 'xp':xp})
    return xp

def getAndIncrementXPMissed(boat):
    xp=1
    DB.write_db('sql/incrementXP.sql', {'boatid':boat['id'], 'xp':xp})
    return xp
