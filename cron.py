import os
from flask import Flask, Blueprint
from flask import render_template, send_from_directory
from flask import redirect, flash
from flask import request, make_response
import json

from . import building
from . import maputils
from . import boat
from . import pnj

cron = Blueprint('cron', __name__)



@cron.route('/cron/reloadTurnHarbours', methods=['POST'])
def reloadTurnHarbours():
    building.reloadTurnHarbours()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@cron.route('/cron/reloadTurnPNJ', methods=['POST'])
def reloadTurnPNJ():
    boat.reloadTurnPNJ()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@cron.route('/cron/popPNJ', methods=['POST'])
def popPNJs():
    pnj.popPNJs()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@cron.route('/cron/createMapPng', methods=['POST'])
def createMapPng():
    maputils.createMapPng()
    maputils.addBuildingsToMapPng()
    maputils.createSideMapPng('pirate')
    maputils.createSideMapPng('corsaire')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@cron.route('/cron/exportMapPng', methods=['POST'])
def exportMapPng():
    maputils.exportMapPng()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@cron.route('/cron/createSideMap', methods=['POST'])
def createSideMap():
    maputils.createSideMap()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@cron.route('/cron/importMapFromPng/<id>', methods=['POST'])
def importMapFromPng(id=1):
    maputils.importMapFromPng(id)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@cron.route('/cron/sunkALittle', methods=['POST'])
def sunkALittle():
    boat.sunkALittle()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
