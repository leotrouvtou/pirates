from . import DBAccess as DB
from . import maputils
from . import sail
from . import boat as boatutils
from . import events
from . import xp
from flask import flash
import math


def enterHarbour(harbourid, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    harbour = DB.request_db('sql/getHarbour.sql', (harbourid,))[0]
    check = checkCanEnter(harbour, boat)
    if check:
        DB.write_db('sql/updateBoatInBuilding.sql', ( harbour['lat'], harbour['long'], boatid))
        events.addEventBoatEnterHarbour(boat, harbour)

def enterPortal(portalid, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    portal = DB.request_db('sql/getPortal.sql', (portalid,))[0]
    check = checkCanEnter(portal, boat)
    if check==True:
        sail.teleportToHarbour(portal['harbourname'], boat)

def checkCanEnter(building, boat):
    if building['mapid']==boat['mapid']:
        dist = maputils.getDistance(building['lat'], building['long'], boat['lat'], boat['long'])
        if dist==1:
            return True
        else:
            if dist==0:
                flash('Vous êtes déjà dans '+building['name'])
                return False
    flash( building['name']+" est trop loin ( "+str(dist)+" cases )")
    return False

def getTavern(boatid):
    results ={}
    results['boat'] = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if results['boat']['inharbour']:
        results['harbour'] =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        results['boats'] = DB.request_db('sql/getBoatsInHarbour.sql', (results['harbour']['id'],))
        results['pops']=DB.request_db('sql/getPops.sql', (results['harbour']['id'],))
        results['totalpop'] = DB.request_db('sql/getTotalPops.sql', (results['harbour']['id'],))[0]['totalpop']
        results['members'] = xp.getMembers(boatid)
        return results
    else:
        flash("Vous n'êtes pas dans un port")
        return ''

def getShipyard(boatid):
    results ={}
    results['boat'] = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if results['boat']['inharbour']:
        results['harbour'] =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        results['bourse']=getHarbourBourse(results['harbour'])
        return results
    else:
        flash("Vous n'êtes pas dans un port")
        return ''
    
def getDock(boatid):
    results ={}
    results['boat'] = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if results['boat']['inharbour']:
        results['harbour'] =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        results['boatstock'] =  DB.request_db('sql/getBoatHold.sql', (boatid,))
        results['harbourstock']  =  DB.request_db('sql/getHarbourHold.sql', (results['harbour']['id'],))
        results['population'] = DB.request_db('sql/getHarbourPop.sql', (results['harbour']['id'],))
        results['totalpop'] = DB.request_db('sql/getTotalPops.sql', (results['harbour']['id'],))[0]['totalpop']
        results['bourse']=getHarbourBourse(results['harbour'])
        return results
    else:
        flash("Vous n'êtes pas dans un port")
        return ''

def sellRessources(ressourceid, amount, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    ressource = DB.request_db('sql/getRessource.sql', (ressourceid,))[0]
    if boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        if boat['stockid']==ressource['stockid']:
            if amount<=ressource['number']:
                stockid = getOrCreateHarbourStock(harbour['id'])
                price = getPrice(ressource, amount, 'sell', harbour['id'] )
                if ressource['type']!="Pièces de huit" and ressource['type']!="Population":
                    getOrCreateRessource(amount, ressource['ressourceid'], ressource['side'], harbour['stockid'])
                if ressource['type']=="Population":
                    ransomToCapilal(amount, ressource['side'])
                DB.write_db('sql/decrementRessource.sql', (amount, ressource['id']))
                if amount==ressource['number']:
                    DB.write_db('sql/deleteRessource.sql', (ressource['id'],))
                DB.write_db('sql/incrementThune.sql', {'boatid':boatid, 'price':price})
                events.addEventBoatUnLoadRessource(harbour, boat, amount, ressource['type'])
                flash("Vous avez vendu "+str(amount)+" tonneaux de "+ressource['type'])
                return True
            else:
                flash("Vous voulez vendre plus qu'il n'y en a")
                return ''

def ransomToCapilal(amount, side):
    harbours = DB.request_db('sql/getHarbourSide.sql', (side,))
    for harbour in harbours:
        if harbour['defense']<harbour['maxpop']:
            toadd = min(harbour['maxpop']-harbour['defense'], amount)
            amount = amount-toadd
            event = "({} {}s)".format(str(toadd), side)
            DB.write_db('sql/incrementPop.sql', (toadd, side, harbour['id']))
            events.addEventHarbourAddPop(harbour, event)
            if amount==0:
                return True


def buyRessources(ressourceid, amount, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        ressource = DB.request_db('sql/getRessource.sql', (ressourceid,))[0]
        if harbour['stockid']==ressource['stockid']:
            if amount<=ressource['number']:
                price = getPrice(ressource, amount, 'buy', harbour['id'])
                if boat['thune']>=price:
                    if boat['stockid']=='' or  boat['stockid'] is None:
                        boatutils.createAndAssignStock(boatid)
                        boat=DB.request_db('sql/getBoat.sql', (boatid,))[0]
                    getOrCreateRessource(amount, ressource['ressourceid'], ressource['side'], boat['stockid'])
                    DB.write_db('sql/decrementRessource.sql', (amount, ressource['id']))
                    DB.write_db('sql/decrementThune.sql', {'boatid':boatid, 'price':price})
                    events.addEventBoatLoadRessource(harbour, boat, amount, ressource['type'])
                    flash("Vous avez acheté "+str(amount)+" tonneaux de "+ressource['type'])
                    return True
                else:
                    flash("Vous n'avez pas assez de pièces de huit")
                    return ''
            else:
                flash("Vous voulez acheter plus qu'il n'y en a")
                return ''

def modifyPrice(ressourceid, sellorbuy, price, harbourId):
    harbourprice = DB.request_db('sql/getHarbourPrice.sql', (ressourceid, harbourId))
    if harbourprice==[]:
        basicprices=DB.request_db('sql/getHarbourPrice.sql', (ressourceid, 0))[0]
        DB.write_db('sql/insertPriceForHarbour.sql', (ressourceid, basicprices['sellprice'], basicprices['buyprice'], harbourId))
    if sellorbuy=='buy':
        DB.write_db('sql/updateBuyPrice.sql', (price, harbourId, ressourceid))
    else:
        DB.write_db('sql/updateSellPrice.sql', (price, harbourId, ressourceid))
    return ''


def getOrCreateRessourceType(denom):
    ressourceType = DB.request_db('sql/getRessourceType.sql', (denom, ))
    if len(ressourceType)==0:
        DB.write_db('sql/addRessourceType.sql', (denom, ))
    return DB.request_db('sql/getRessourceType.sql', (denom,))[0]

def addRessourceType(denom, sellprice, buyprice):
    DB.write_db('sql/addRessourceType.sql', (denom, ))
    ressource = DB.request_db('sql/getRessourceType.sql', (denom, ))[0]
    DB.write_db('sql/insertPriceForHarbour.sql', (ressource['id'], sellprice, buyprice, 0))

def modifyRessource(ressourceid, amount):
    if amount==0:
        DB.write_db('sql/deleteRessource.sql', (ressourceid,))
    else:
        DB.write_db('sql/modifyRessource.sql', (amount, ressourceid))
    return ''

def modifyPop(popid, amount):
    harbour =  DB.request_db('sql/getHarbourByPopId.sql', (popid,))[0]
    if amount==0:
        DB.write_db('sql/deletePop.sql', (popid,))
    else:
        DB.write_db('sql/modifyPop.sql', (amount, popid))
    calculateSide(harbour)

def getOrCreateHarbourStock(harbourid):
    harbour =  DB.request_db('sql/getHarbourById.sql', (harbourid,))[0]
    if harbour['stockid'] is None or harbour['stockid']=='':
        DB.write_db('sql/createStock.sql')
        stock=DB.request_db('sql/getLastStock.sql')[0]
        DB.write_db('sql/assignStockToHarbour.sql', (stock['id'], harbourid))
        return stock['id']
    return harbour['stockid']

def addRessource(ressourceType, side, amount, harbourId):
    if ressourceType:
        stockid = getOrCreateHarbourStock(harbourId)
        harbour =  DB.request_db('sql/getHarbourById.sql', (harbourId,))[0]
        getOrCreateRessource(amount, ressourceType, side, harbour['stockid'])
    else:
        flash("Merci de selectionner une ressource")

def addPop(side, amount, harbourid):
    harbour =  DB.request_db('sql/getHarbourById.sql', (harbourid,))[0]
    sumtot =  DB.request_db('sql/getTotalPops.sql', (harbourid,))[0]
    if int(sumtot['totalpop'] or 0)+amount<=int(harbour['maxpop']):
        getOrCreatePop(amount, side, harbour)
    else:
        flash(" la population totale doit être moins que "+str(harbour['maxpop']))


def getOrCreateRessource(amount, ressourceid, side, stockid, sourceid=''):
    if ressourceid in ['Population', 'Pièces de huit', 'Pavillon']:
        ressourceid =  getOrCreateRessourceType(ressourceid)['id']
    ressource = DB.request_db('sql/getRessourceByType.sql', (ressourceid, side, stockid))
    if len(ressource)==0:
        if sourceid!='':
            DB.write_db('sql/createRessourceWithSourceId.sql', (amount, ressourceid, side, amount*10, stockid, sourceid ))
        else:
            DB.write_db('sql/createRessource.sql', (amount, ressourceid, side, amount*10, stockid ))
    else:
        DB.write_db('sql/incrementRessource.sql', (amount,  ressource[0]['id']))

def getOrCreatePop(amount, side, harbour):
    harbourpop = DB.request_db('sql/getharbourPopBySide.sql', ( side, harbour['id']))
    if len(harbourpop)==0:
        DB.write_db('sql/createPop.sql', (amount, side, harbour['id'] ))
    else:
        DB.write_db('sql/incrementPop.sql', (amount, side, harbour['id']))
    calculateSide(harbour)

def loadPeople(popid, amount, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    pop = DB.request_db('sql/getPop.sql', (popid,))[0]
    harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
    totpop =  DB.request_db('sql/getTotalPops.sql', ( harbour['id'],))[0]
    if boat['inharbour']:
        if harbour['id']==pop['harbourid']:
            if amount<=pop['number'] and totpop['totalpop']-amount>=harbour['maxpop']*0.8:
                if pop['side']== 'neutral'or boat['side'] in pop['side']:
                    if boat['stockid']=='' or boat['stockid'] is None:
                        boatutils.createAndAssignStock(boatid)
                        boat=DB.request_db('sql/getBoat.sql', (boatid,))[0]
                    getOrCreateRessource(amount, 'Population', pop['side'], boat['stockid'])
                    DB.write_db('sql/decrementPop.sql', (amount, pop['id']))
                    flash('Vous avez embarqué '+str(amount)+' '+pop['side']+'s')
                    events.addEventBoatLoadPeople(harbour, boat, amount, pop['side'])
                    return True
                else:
                    flash("Vousne pouvez embarquer que des populations alliées.")
            else:
                flash("Vous voulez embarquer trop de gens (il doit rester 80% de la pop du port complet : "+str(harbour['maxpop']*0.8)+").")
                return ''


def unLoadPeople(ressourceid, amount, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    ressource = DB.request_db('sql/getRessource.sql', (ressourceid,))[0]
    harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
    if boat['inharbour']:
        if boat['stockid']==ressource['stockid']:
            if amount<=ressource['number']:
                price = getPrice(ressource, amount, 'sell', harbour['id'])
                if int(harbour['defense'])+amount<=int(harbour['maxpop']):
                    events.addEventBoatUnLoadPeople(harbour, boat, amount, ressource['side'])
                    getOrCreatePop(amount, ressource['side'], harbour)
                    DB.write_db('sql/decrementRessource.sql', (amount, ressource['id']))
                    if amount==ressource['number']:
                        DB.write_db('sql/deleteRessource.sql', (ressource['id'],))
                    flash('Vous avez debarqué '+str(amount)+' '+ressource['side']+'s')
                else:
                    flash(" la population totale doit être moins que "+str(harbour['maxpop']))
                return True
            else:
                flash("Vous voulez debarquer plus de gens que vous en avez.")
                return ''

def getPrice(ressource, amount, action, harbourid):
    price = DB.request_db('sql/getHarbourPrice.sql', (ressource['ressourceid'], harbourid))
    if len(price)>0:
        price=price[0]
    else:
        price = DB.request_db('sql/getHarbourPrice.sql', (ressource['ressourceid'], 0))
        if len(price)>0:
            price=price[0]
        else:
            return amount*1
    return amount*price[action+'price']


def calculateSide(harbour):
    harbourpop = DB.request_db('sql/getHarbourPop.sql', ( harbour['id'],))
    totpop =  DB.request_db('sql/getTotalPops.sql', ( harbour['id'],))[0]
    newside=harbour['side']
    if totpop['totalpop']>harbour['maxpop']/2:
        for pop in harbourpop:
            if pop['number'] > totpop['totalpop']/2 and pop['side']!=harbour['side']:
                newside=pop['side']
    else:
        newside=harbourpop[0]['side']
    if newside!=harbour['side']:
        if harbour['side']!="neutral" and harbour['capital']:
            DB.write_db('sql/updateHarbourCapital.sql', (harbour['id'],))
            DB.write_db('sql/updateCapitalSide.sql', (harbour['side'],))
        DB.write_db('sql/updateHarbourSide.sql', (newside, harbour['id']))
        events.addEventHarbourChangeSide(harbour, newside)

def createBuilding(type, request):
    name   = request.form.get("name")
    side   = request.form.get("side")
    lat   = request.form.get("lat")
    long   = request.form.get("long")
    mapid   = request.form.get("mapid")
    if name and side and lat and long and mapid:
        if type=="harbour":
            capital   = request.form.get("capital")
            maxpop   = request.form.get("maxpop")
            description   = request.form.get("description")
            neutral   = request.form.get("neutral")
            pirates   = request.form.get("pirate")
            corsaires   = request.form.get("corsaire")
            if maxpop:
                harbour =  DB.request_db('sql/getHarbourByLatLong.sql', (lat, long, mapid))
                portal =  DB.request_db('sql/getPortalByLatLong.sql', (lat, long, mapid))
                if len(harbour)==0 and len(portal)==0:
                    DB.write_db('sql/createHarbour.sql', (name,description, side, lat, long, mapid, capital, maxpop))
                    harbour =  DB.request_db('sql/getHarbourByName.sql', (name,))[0]
                    for pop in ['neutral', 'pirates', 'corsaires']:
                        amount =request.form.get(pop)
                        if amount!='':
                            DB.write_db('sql/createPop.sql', (amount, pop, harbour['id'] ))
                else:
                    flash('Il y a déjà un batiment à cet endroit.')
            else:
                flash('Vous devez remplir la pop max.')
        else:
            toharbour   = request.form.get("toharbour")
            if toharbour:
                harbour =  DB.request_db('sql/getHarbourByLatLong.sql', (lat, long, mapid))
                portal =  DB.request_db('sql/getPortalByLatLong.sql', (lat, long, mapid))
                if len(harbour)==0 and len(portal)==0:
                    DB.write_db('sql/createPortal.sql', (name, side, lat, long, mapid, toharbour))
                else:
                    flash('Il y a déjà un batiment à cet endroit.')
            else:
                flash('Le portail doit menner quelque part.')
    else:
        flash('Merci de choisir un nom et un camp')


def getBuildings():
    results ={}
    results['harbours'] = DB.request_db('sql/getHarbours.sql')
    results['portals'] = DB.request_db('sql/getPortals.sql')
    return results

def getHarbour(id):
    results ={}
    results['harbour'] =  DB.request_db('sql/getHarbourById.sql', (id,))[0]
    results['harbourpop'] = DB.request_db('sql/getHarbourPop.sql', ( id,))
    results['harbourstock']  =  DB.request_db('sql/getHarbourHold.sql', (id,))
    results['stockitems']  =  DB.request_db('sql/getStockItems.sql')
    return results


def getWeight(ressourceType, amount):
    return 1*amount



def getBourse():
    results ={}
    harbours = DB.request_db('sql/getHarbours.sql')
    results=getBasicPrices(0, results)
    for harbour in harbours:
        results = appendHarbourBourse(harbour['id'], results)
    return results


def getHarbourBourse(harbour):
    results ={}
    results=getBasicPrices(0, results)
    results = appendHarbourBourse(harbour['id'], results)
    return results

def appendHarbourBourse(harbourid, results):
    prices = DB.request_db('sql/getHarbourPrices.sql', (harbourid,))
    harbour={}
    harbour['name']=DB.request_db('sql/getHarbourById.sql', (harbourid,))[0]['name']
    pricelist=[]
    for price in prices:
        item={}
        item['type']=price['type']
        item['sellprice']=price['sellprice']
        item['buyprice']=price['buyprice']
        item['ressourceid']=price['ressourceid']
        pricelist.append(item)
    for price in results[0]['prices']:
        if "'"+str(price['type'])+"'" not in str(pricelist):
            pricelist.append(price)
    harbour['prices']=pricelist
    results[harbourid]= harbour
    return results

def getBasicPrices(harbourid, results):
    prices = DB.request_db('sql/getHarbourPrices.sql', (0,))
    harbour={}
    harbour['name']="Prix de bases"
    pricelist=[]
    for price in prices:
        item={}
        item['type']=price['type']
        item['basic']=1
        item['sellprice']=price['sellprice']
        item['buyprice']=price['buyprice']
        item['ressourceid']=price['ressourceid']
        pricelist.append(item)
    harbour['prices']=pricelist
    results[0]= harbour
    return results



def reloadTurnHarbours():
    harbours = DB.request_db('sql/getHarboursInMap.sql')
    for harbour in harbours:
        if harbour['side']!='neutral':
            maputils.updateSideMap( harbour['side'], harbour['lat']-5, harbour['lat']+5, harbour['long']-5, harbour['long']+5)
        if harbour['active']:
            harbourstock  =  DB.request_db('sql/getHarbourHold.sql', (harbour['id'],))
            harbourpop = DB.request_db('sql/getHarbourPop.sql', (harbour['id'],))
            totalpop = DB.request_db('sql/getTotalPops.sql', (harbour['id'],))[0]['totalpop']
            event=''
            for stock in harbourstock:
                event = event + increaseStock(stock, totalpop)
            if harbour['side'] is not 'neutral':
                for pop in harbourpop:
                    event = increasePop(pop, totalpop, harbour['maxpop'], harbour, event)
                    if pop != harbourpop[len(harbourpop)-1]:
                        event=event+' |'
            events.addEventNewTurnHarbour(harbour, event)
        else:
            if harbour['defense']==0 or harbour['defense']==None:
                harbourstock  =  DB.request_db('sql/getHarbourHold.sql', (harbour['id'],))
                event=''
                for stock in harbourstock:
                    event = event + decreaseStock(stock, totalpop)
                events.addEventNewTurnHarbourNoPop(harbour, event)
            else:
                boatnumber = DB.request_db('sql/getHarbourBoatNumber.sql', (harbour['id'],))[0]
                if boatnumber['number']<6:
                    harbourpop = DB.request_db('sql/getHarbourPop.sql', (harbour['id'],))
                    totalpop = DB.request_db('sql/getTotalPops.sql', (harbour['id'],))[0]['totalpop']
                    event=''
                    if harbour['side'] is not 'neutral':
                        for pop in harbourpop:
                            event = increasePop(pop, totalpop, harbour['maxpop'], harbour, event)
                            if pop != harbourpop[len(harbourpop)-1]:
                                event=event+' |'
                    events.addEventNewTurnHarbourNotActive(harbour, event)
                else:
                    events.addEventNewTurnHarbourSiegeMode(harbour)

def decreaseStock(stock, totalpop):
    ismin=''
    persents = getStockPersent(stock['type'])
    if stock['number']>0 and stock['type'] is not 'Population':
        toremove = persents['rate']*totalpop
        if stock['number']-toremove<0:
            toremove = stock['number']
            ismin='(min)'
        DB.write_db('sql/decrementRessource.sql', (round(toremove), stock['id']))
        return ' '+stock['type']+': -'+str(round(toremove))+ismin+' |'
    return ''

def increaseStock(stock, totalpop):
    ismax=''
    persents = getStockPersent(stock['type'])
    if stock['number']<=persents['max']*totalpop and stock['type'] is not 'Population':
        toadd = persents['rate']*totalpop
        if stock['number']+toadd>persents['max']*totalpop:
            toadd = totalpop*persents['max']-stock['number']
            ismax='(max)'
        DB.write_db('sql/incrementRessource.sql', (round(toadd), stock['id']))
        return ' '+stock['type']+': +'+str(round(toadd))+ismax+' |'
    return ''

def drinksOnMe(drinks, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        harbourpop = DB.request_db('sql/getharbourPopBySide.sql', ('neutral', harbour['id'],))
        amount = math.floor(drinks/10)
        if len(harbourpop)>0:
            switched = min(harbourpop[0]['number'], amount)
            if boat['thune']>=drinks:
                DB.write_db('sql/decrementThune.sql', {'boatid':boatid, 'price':drinks})
                events.addEventBoatdrinksOnMe(harbour, boat, drinks)
                getOrCreatePop(switched, boat['side'], harbour)
                DB.write_db('sql/decrementPop.sql', (switched, harbourpop[0]['id']))
                events.addEventPopChangeSide(harbour, switched, boat['side'])
                calculateSide(harbour)
                flash("Vous avez payé une tournée de "+str(drinks)+" verres")
            else:
                flash("Vous n'avez pas assez de pièces de huit")
        else:
            flash("Il n'y a personne dans la taverne !")


def getStockPersent(stocktype):
    persents = {}
    persents['rate']=0.1
    persents['max']=0.5
    return persents

def increasePop(pop, totalpop, maxpop, harbour, event):
    ismax=''
    if totalpop<maxpop:
        toadd = math.ceil(getPopPersent(pop['side'], harbour['side'])*pop['number'])
        if totalpop+toadd>maxpop:
            toadd = maxpop-totalpop
            ismax='(max)'
        DB.write_db('sql/incrementPop.sql', (round(toadd), pop['side'], harbour['id']))
        event = event+' '+pop['side']+': +'+str(round(toadd))+ismax
    return event

def getPopPersent(popside, harbourside):
    if popside==harbourside:
        return 0.05
    else:
        return 0.02

def updateHarbourDamage(harbour, damage, event):
    harbourpop = DB.request_db('sql/getHarbourPopNotNull.sql', (harbour['id'],))
    share =  calculateShare(damage, len(harbourpop))
    for pop in harbourpop:
        if pop['side']==harbour['side']:
            damage=share['fullshare']
        else:
            damage=share['littleshare']
        if pop['number']-damage<0:
            damage=pop['number']
        DB.write_db('sql/decrementPop.sql', (damage, pop['id']))
        event=event+pop['side']+": "+str(damage)+" morts, "
    return event

def calculateShare(damage, number):
    share={}
    if number==1:
        share['fullshare']=damage
    elif number==2:
        share['littleshare']=round(damage*0.25)
        share['fullshare']=damage-share['littleshare']
    else:
        share['littleshare']= round(damage*0.4/(number-1))
        share['fullshare'] = damage - (share['littleshare']*(number-1))
    return share

def surrenderHarbour(harbour):
    harbourpop = DB.request_db('sql/getHarbourPop.sql', (harbour['id'],))
    DB.write_db('sql/createPop.sql', (harbour['defense'], 'neutral', harbour['id'] ))
    for pop in harbourpop:
        DB.write_db('sql/deletePop.sql', (pop['id'],))
    calculateSide(harbour)

def getCapital(side):
    return DB.request_db('sql/getCapital.sql', (side,))
