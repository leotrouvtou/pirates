from . import DBAccess as DB
from . import sail
from . import maputils
from . import boat as boatutils
from . import DD
from . import events
from . import building
from . import canon
from. import xp
from flask import flash
import json
import math
import random

def move(pnj):
    harbour = building.getCapital("corsaire")[0]
    if pnj['mouvementpoint']>0:
        direction = sail.getPNJDirection(harbour, pnj)
        if direction:
            sail.sailTo(direction, pnj["id"])


def shoot(pnj):
    if pnj['type']=="middleZombi":
        harbours= DB.request_db('sql/getHarboursArroundMe.sql', (pnj['lat']-pnj['range'], pnj['lat']+pnj['range'], pnj['long']-pnj['range'], pnj['long']+pnj['range']))
        print(harbours)
        if len(harbours)>0:
            target=random.choice(harbours)
            canon.shootB2H(target, pnj)
        else:
            boats = DB.request_db('sql/getBoats.sql', (pnj['lat']-pnj['range'], pnj['lat']+pnj['range'], pnj['long']-pnj['range'], pnj['long']+pnj['range'], pnj['id']))
            toshoot=[]
            for boat in boats:
                if boat['side']!=pnj['side']:
                    toshoot.append(boat)
            if len(toshoot)>0:
                target=random.choice(toshoot)
                canon.shootB2B(target, pnj)
    else:
        boats = DB.request_db('sql/getBoats.sql', (pnj['lat']-pnj['range'], pnj['lat']+pnj['range'], pnj['long']-pnj['range'], pnj['long']+pnj['range'], pnj['id']))
        toshoot=[]
        for boat in boats:
            if boat['side']!=pnj['side']:
                toshoot.append(boat)
        if len(toshoot)>0:
            target=random.choice(toshoot)
            canon.shootB2B(target, pnj)
        else:
            harbours= DB.request_db('sql/getHarboursArroundMe.sql', (pnj['lat']-pnj['range'], pnj['lat']+pnj['range'], pnj['long']-pnj['range'], pnj['long']+pnj['range']))
            if len(harbours)>0:
                target=random.choice(harbours)
                canon.shootB2H(target, pnj)

def moveAndShoot(pnj):
    if pnj['mouvementpoint']>0:
        move(pnj)
    pnj = DB.request_db('sql/getBoat.sql', (pnj['id'],))[0]
    if pnj['actionpoint']>=4:
        shoot(pnj)


def popPNJs():
    pnjs = DB.request_db('sql/getPNJInMap.sql')
    toPop = max(10-len(pnjs),3)
    pnjs = DB.request_db('sql/getPNJNotInMap.sql')
    for i in range(0, toPop):
        popPNJ(random.choice(pnjs))

def popPNJ(pnj):
    case = maputils.getRandomSeaOnSide()
    members = xp.getMembers(pnj['id'])
    newBoat=boatutils.calculateBoat(randomizePNJ(), members)
    newBoat['id']=pnj['id']
    print(newBoat)
    DB.write_db('sql/changeBoat.sql', newBoat)
    DB.write_db('sql/teleportToPosition.sql', ( case['lat'], case['long'], pnj['id']))

def randomizePNJ():
    number = random.randint(1, 20)
    if number>18:
        return "bigZombi"
    elif number<6:
        return "littleZombi"
    else:
        return "middleZombi"
