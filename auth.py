import os
from flask import Blueprint
from flask import render_template, send_from_directory
from flask import redirect, url_for, flash
from flask import request, make_response, session
from flask_login import LoginManager,login_user, logout_user
from flask_login import UserMixin,login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
from . import DBAccess as DB
from . import boat


auth = Blueprint('auth', __name__)




class User(UserMixin):
    pass

def requires_roles(*roles):
    def inner_function(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if current_user.is_authenticated:
                if not session["role"] in roles:
                    flash("Vous n'avez pas les permissions pour voir cette page")
                    return redirect(url_for('auth.login'))
                return f(*args, **kwargs)
        return decorated
    return inner_function



@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/signup')
def signup():
    sides = DB.request_db('sql/sideNumber.sql')
    return render_template('signup.html', sides=sides)

@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False
    results=DB.request_db('sql/getUserByEmail.sql', (email,))
    if len(results)<1:
        flash('Merci de vérifier le pseudo et le mot de passe.')
        return redirect(url_for('auth.login'))
    else:
        user = results[0]
    if not user or not check_password_hash(user['password'], password):
        flash('Merci de vérifier le pseudo et le mot de passe.')
        return redirect(url_for('auth.login'))

    flask_user = User()
    flask_user.id = email
    login_user(flask_user, remember=remember)
    return redirect(url_for('main.map'))

@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    pseudo = request.form.get('pseudo')
    side = request.form.get('side')
    boatname = request.form.get('boatname')
    password = request.form.get('password')
    user=DB.request_db('sql/getUserByEmail.sql', (email,))
    if user: # if a user is found, we want to redirect back to signup page so user can try again
        flash('Email déjà utilisé')
        return redirect(url_for('auth.signup'))
    DB.write_db('sql/createUser.sql', (email, pseudo, generate_password_hash(password, method='sha256')))
    user=DB.request_db('sql/getUserByEmail.sql', (email,))[0]
    boat.createBoat(boatname, user['id'], side)
    return redirect(url_for('auth.login'))


# Custom static data
@auth.route('/restricted/<path:filename>')
@login_required
def custom_restricted(filename):
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    if boat['side'] in filename:
        return send_from_directory(os.path.join(auth.root_path, 'restricted'), filename)
    else:
        return redirect(url_for('auth.login'))
        
