import os
from flask import Flask, Blueprint
from flask import render_template, send_from_directory
from flask import redirect, flash, url_for
from flask import request, make_response, session
from flask_login import login_required
import json
from datetime import datetime, timedelta

from . import DBAccess as DB
from . import maputils
from . import sail
from . import canon
from . import building
from . import boat
from . import events
from . import quests
from . import ressources
from . import treasure

from .auth import requires_roles

main = Blueprint('main', __name__)
sides = ['pirate', 'corsaire', 'neutre']
timehour= 22

# Custom static data
@main.route('/statics/<path:filename>')
def custom_static(filename):
    return send_from_directory(os.path.join(main.root_path, 'statics'), filename)

def getOrCreatePageText(page, request):
    result={}
    pagetext= DB.request_db('sql/getPageText.sql', (page,))
    if pagetext == []:
        DB.write_db('sql/createPageText.sql', (page,))
        pagetext = DB.request_db('sql/getPageText.sql', (page,))
    result['content']=pagetext[0]
    result['edit']=request.args.get('edit', 'false')
    return result


@main.route('/')
def index():
    pagetext= getOrCreatePageText('index', request)
    return render_template('index.html', pagetext=pagetext['content'], edit=pagetext['edit'])

@main.route('/sailTo/<direction>', methods=['POST'])
@login_required
def sailTo(direction):
    result = sail.sailTo(direction, session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/repair', methods=['POST'])
@login_required
def repair():
    boat.repair(session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/enterBuilding/<buildingid>', methods=['POST'])
@login_required
def enterBuilding(buildingid):
    buildingtype = request.form.get('buildingtype')
    if buildingtype=="harbour":
        result = building.enterHarbour(buildingid,  session['boatid'])
    else:
        result = building.enterPortal(buildingid,  session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/getObject/<objectid>', methods=['POST'])
@login_required
def getObject(objectid):
    boat.getObject(objectid,  session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/hold')
@login_required
def getBoatHold():
    pagetext= getOrCreatePageText('hold', request)
    results=boat.getHold(session['boatid'])
    myboat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    malus = boat.calculatePMMalus(myboat)
    return render_template('hold.html', holds = results['holds'], boat=results['boat'],treasures=results['treasures'],pagetext=pagetext['content'], edit=pagetext['edit'], malus=malus)


@main.route('/dock')
@login_required
def getDock():
    results=building.getDock(session['boatid'])
    pagetext= getOrCreatePageText('dock', request)
    myboat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    malus = boat.calculatePMMalus(myboat)
    return render_template('dock.html', boatstock = results['boatstock'], harbourstock = results['harbourstock'], boat=results['boat'], harbour=results['harbour'], harbourpop=results['population'], items=results['bourse'], totalpop = results['totalpop'], pagetext=pagetext['content'], edit=pagetext['edit'], malus=malus)


@main.route('/shipyard')
@login_required
def getShipyard():
    results=building.getShipyard(session['boatid'])
    pagetext= getOrCreatePageText('shipyard', request)
    return render_template('shipyard.html', boat=results['boat'], harbour=results['harbour'],items=results['bourse'], pagetext=pagetext['content'], edit=pagetext['edit'])

@main.route('/quests')
@login_required
def getQuests():
    results=quests.getQuests(session['boatid'])
    pagetext= getOrCreatePageText('quests', request)
    return render_template('quests.html', boatquests = results['boatquests'], harbourquests = results['harbourquests'], boat=results['boat'], harbour=results['harbour'], boatquestsid = results['boatquestsid'], pagetext=pagetext['content'], edit=pagetext['edit'])

@main.route('/tavern')
@login_required
def getTavern():
    results=building.getTavern(session['boatid'])
    pagetext= getOrCreatePageText('tavern', request)
    if results=='':
        return redirect(url_for('main.map'))
    else:
        return render_template('tavern.html', pops = results['pops'], totalpop = results['totalpop'], boat=results['boat'], boats=results['boats'], harbour=results['harbour'], pagetext=pagetext['content'],members=results['members'], edit=pagetext['edit'])

@main.route('/shootByCanon/', methods=['POST'])
@main.route('/shootByCanon/<targetid>', methods=['POST'])
@login_required
def shootByCanon(targetid=''):
    if targetid!='':
        result = canon.shootByCanon(targetid, session['boatid'])
    else:
        flash("Merci de selectioner une cible")
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/giveMoney/<targetid>', methods=['POST'])
@login_required
def giveMoney(targetid):
    amount = request.form.get('amount')
    if amount!='':
        treasure.giveMoney(targetid, session['boatid'], amount)
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}



@main.route('/searchTreasure', methods=['POST'])
@login_required
def searchTreasure():
    treasure.searchTreasure( session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/takeQuest/<questid>', methods=['POST'])
@login_required
def takeQuest(questid):
    result = quests.takeQuest(questid, session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/validateQuest/<questid>', methods=['POST'])
@login_required
def validateQuest(questid):
    result = quests.validateQuest(questid, session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/inspectWithGoogle/<coord>', methods=['POST'])
@login_required
def inspectWithGoogle(coord):
    result = maputils.inspectWithGoogle(coord, session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/sellRessources/<ressourceid>', methods=['POST'])
@login_required
def sellRessources(ressourceid):
    amount = request.form.get('amount')
    if amount!='':
        result = building.sellRessources(ressourceid, int(amount),  session['boatid'])
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/buyRessources/<ressourceid>', methods=['POST'])
@login_required
def buyRessources(ressourceid):
    amount = request.form.get('amount')
    if amount!='':
        result = building.buyRessources(ressourceid, int(amount), session['boatid'])
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/drinksOnMe', methods=['POST'])
@login_required
def drinksOnMe():
    amount = request.form.get('amount')
    if amount!='':
        result = building.drinksOnMe(int(amount), session['boatid'])
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/putRessourceIntoSea/<ressourceid>', methods=['POST'])
@login_required
def putRessourceIntoSea(ressourceid):
    amount = request.form.get('amount')
    if amount!='':
        result = boat.putRessourceIntoSea(ressourceid, int(amount), session['boatid'])
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/putRessourcesIntoTreasure', methods=['POST'])
@login_required
def putRessourcesIntoTreasure():
    ressources = request.form.get('ressources')
    if ressources!='[]':
        result = treasure.buriTreasure(session['boatid'], json.loads(ressources))
    else:
        flash('Merci de selectionner des ressources')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/loadPeople/<popid>', methods=['POST'])
@login_required
def loadPeople(popid):
    amount = request.form.get('amount')
    if amount!='':
        result = building.loadPeople(popid, int(amount), session['boatid'])
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/unLoadPeople/<ressourceid>', methods=['POST'])
@login_required
def unLoadPeople(ressourceid):
    amount = request.form.get('amount')
    if amount!='':
        result = building.unLoadPeople(ressourceid, int(amount), session['boatid'])
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@main.route('/changeFlag/<ressourceid>', methods=['POST'])
@login_required
def changeFlag(ressourceid):
    boat.changeFlag(ressourceid, session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/removeFlag', methods=['POST'])
@login_required
def removeFlag():
    boat.removeFlag(session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}



@main.route('/map')
@login_required
def map():
    carte = maputils.getMapInfo(session['boatid'])
    targets = canon.getTargets(session['boatid'])
    myboat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    malus = boat.calculatePMMalus(myboat)
    if myboat['fakeside']=='' or myboat['fakeside'] is None:
        fakeside=''
    else:
        fakeside = json.loads(myboat['fakeside'])
    now = datetime.now()
    now = now.strftime('%d/%m')+", il est "+now.strftime('%H:%M:%S')
    nextturn= datetime.strptime(myboat['lastturn'], '%Y-%m-%d %H:%M:%S')+ timedelta(hours=timehour)
    nextturn=nextturn.strftime('%d/%m')+" à "+nextturn.strftime('%H:%M:%S')
    return render_template('map.html', xmin = carte['xmin'], xmax=carte['xmax'], ymin=carte['ymin'], ymax=carte['ymax'], carte=carte['carte'], targets=targets, boat=myboat, nextturn=nextturn, fakeside=fakeside,now=now, malus=malus)

@main.route('/sidemap')
@login_required
def sidemap():
    pagetext= getOrCreatePageText('sidemap', request)
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    legend = maputils.getLegend()
    return render_template('sidemap.html', pagetext=pagetext['content'],boat=boat, edit=pagetext['edit'], legend=legend)


@main.route('/persomap')
@main.route('/persomap/<coord>')
@login_required
def persomap(coord=''):
    carte = maputils.getPersoMapInfo(coord, request)
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    return render_template('persomap.html', xmin = carte['xmin'], xmax=carte['xmax'], ymin=carte['ymin'], ymax=carte['ymax'], carte=carte['carte'], boat=boat)


@main.route('/help')
def help():
    pagetext= getOrCreatePageText('help', request)
    boat = getBoatIfConnected()
    return render_template('help.html', pagetext=pagetext['content'], boat= boat, edit=pagetext['edit'])

@main.route('/events')
@main.route('/events/<refid>')
@login_required
def getEvents(refid=''):
    myevents = events.getEvents(refid)
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('events', request)
    boats = DB.request_db('sql/getAllBoats.sql')
    harbours = DB.request_db('sql/getAllHarbours.sql')
    return render_template('events.html', boat= boat, pagetext=pagetext['content'], harbours=harbours,boats=boats,edit=pagetext['edit'], ev=myevents)


def getBoatIfConnected():
    boat = DB.request_db('sql/getBoat.sql', (session.get('boatid'),))
    if len(boat)>0:
        boat= boat[0]
    else:
        boat={}
    return boat


@main.route('/messages')
@login_required
def messages():
    pagetext= getOrCreatePageText('messages', request)
    boat = getBoatIfConnected()
    messages = DB.request_db('sql/getMyMessages.sql', (session['boatid'],))
    return render_template('messages.html', messages=messages, pagetext=pagetext['content'], boat= boat, edit=pagetext['edit'])

@main.route('/sended')
@login_required
def sended():
    pagetext= getOrCreatePageText('messages', request)
    boat = getBoatIfConnected()
    messages = DB.request_db('sql/getMySendedMessages.sql', (session['boatid'],))
    return render_template('sended.html', messages=messages, pagetext=pagetext['content'], boat= boat, edit=pagetext['edit'])

@main.route('/createMessage')
@login_required
def createMessage():
    boat = getBoatIfConnected()
    boats = DB.request_db('sql/getAllBoats.sql')
    username = DB.request_db('sql/getUserName.sql', (boat['id'],))[0]
    now = datetime.now()
    now = "le "+now.strftime('%d/%m')+", à "+now.strftime('%H:%M:%S')
    return render_template('createmessage.html', boat= boat, boats=boats, now=now, username=username)

@main.route('/reply/<id>')
@login_required
def replyMessage(id):
    boat = getBoatIfConnected()
    boats = DB.request_db('sql/getAllBoats.sql')
    message = DB.request_db('sql/getMessage.sql', (id,))[0]
    username = DB.request_db('sql/getUserName.sql', (boat['id'],))[0]
    now = datetime.now()
    now = "le "+now.strftime('%d/%m')+", à "+now.strftime('%H:%M:%S')
    if ',' in str(message['toids']):
        toids =  message['toids'].split(',')
        if str(boat['id']) in toids:
            toids.remove(str(boat['id']))
            toids.append(str(message['fromid']))
    else:
        if message['toids']=='pirates':
            toids = 'pirates'
        elif message['toids']=='corsaires':
            toids = 'corsaires'
        else:
            toids=[]
            if message['fromid'] == boat['id']:
                toids.append(str(message['toid']))
            else:
                toids.append(str(message['fromid']))
    message['toids']=str(toids)
    return render_template('createmessage.html', boat= boat, boats=boats, message=message, now=now, username=username)


@main.route('/message/<id>')
@login_required
def message(id):
    boat = getBoatIfConnected()
    message = DB.request_db('sql/getMessage.sql', (id,))[0]
    tonames = DB.request_db('sql/getBoatNames.sql', (message['toids'],))[0]
    if session['boatid'] in [message['toid'], message['fromid']]:
        if message['toid']==session['boatid']:
            DB.write_db('sql/makeMessageReaded.sql', (id,))
        return render_template('message.html', boat= boat, message=message, tonames=tonames)
    else:
        flash("Ce message n'est pas pour vous")
        return redirect(url_for('main.messages'))

@main.route('/sendMessage', methods=['POST'])
@login_required
def sendMessage():
    boat = getBoatIfConnected()
    subject = request.form.get('subject')
    toids = request.form.get('toids')
    content = request.form.get('content')
    if subject=='':
        flash('Merci de renseigner un sujet')
        return json.dumps({'success':False}), 200, {'ContentType':'application/json'}
    if toids=='':
        flash('Merci de renseigner un destinataire')
        return json.dumps({'success':False}), 200, {'ContentType':'application/json'}
    if toids=='pirates':
        toList = []
        pirates =  DB.request_db('sql/getIdBySide.sql', ('pirate',))
        for pirate in pirates:
            toList.append(pirate['id'])
        if boat['id'] in toList:
            toList.remove(boat['id'])
    elif toids=='corsaires':
        toList = []
        corsaires =  DB.request_db('sql/getIdBySide.sql', ('corsaire',))
        for corsaire in corsaires:
            toList.append(corsaire['id'])
        if boat['id'] in toList:
            toList.remove(boat['id'])
    else:
        toList = toids.split(',')
    for id in toList:
        if toids in ['pirates','corsaires']:
            DB.write_db('sql/createMessage.sql', (subject, content, id, toids, session['boatid']))
        else:
            DB.write_db('sql/createMessage.sql', (subject, content, id, toids, session['boatid']))
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/makeRound', methods=['POST'])
@login_required
def makeRound():
    angle = request.form.get('angle')
    if angle=='':
        flash('Merci de renseigner une direction')
    else:
       sail.makeRound(angle, session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/addCanon/<ressourceid>', methods=['POST'])
@login_required
def AddCanon(ressourceid):
    boat.addCanon(session['boatid'], ressourceid)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/farmRessources/<coord>', methods=['POST'])
@login_required
def farmRessources(coord):
    ressourceName = request.form.get('ressourceName')
    if ressourceName=='':
        flash('Merci de renseigner ce que vous cherchez')
    else:
        ressources.farmRessources(session['boatid'],ressourceName, coord)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/changeBoat/<boatType>', methods=['POST'])
@login_required
def changeBoat(boatType):
    boat.changeBoat(session.get('boatid'),boatType)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/error')
def hello():
    print("in hello")
    x = None
    x[5]
    return "Hello World!"
