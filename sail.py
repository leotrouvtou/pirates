from . import DBAccess as DB
from . import events
from flask import flash
import random


def sailTo(direction, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    newcoord = getNewPosition(direction, boat)
    case = DB.request_db('sql/getCase.sql', (newcoord['lat'],newcoord['long'], newcoord['mapid']))[0]
    newcoord['pa']=mouvementPoints(case, boat)
    message = checkDirection(case, newcoord, boat)
    if message=='':
        boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
        DB.write_db('sql/updateBoatPosition.sql', ( newcoord['lat'], newcoord['long'],newcoord['pa'], newcoord['deg'], boatid))
        events.addEventBoatMove(boat, newcoord['lat'], newcoord['long'])
    else:
        flash(message)

def makeRound(angle, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if angle in ["no","n","ne","e","se","s","so","o"]:
        deg = getAngleByDirection(angle)
        case = DB.request_db('sql/getCase.sql', (boat['lat'],boat['long'], boat['mapid']))[0]
        pm=mouvementPoints(case, boat)
        if  boat['mouvementpoint']<pm :
            flash("Vous n'avez plus assez de point de mouvement ("+str(boat['mouvementpoint'])+")")
        else:
            boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
            DB.write_db('sql/updateBoatDirection.sql', (deg, boatid))
            DB.write_db('sql/decrementMouvementPoint.sql', (pm, boat['id']))
            events.addEventBoatMakeRound(boat, deg)
    else:
        flash("Merci de renseigner un angle correct")

def teleportToHarbour(harbourname, boat):
    harbour = DB.request_db('sql/getHarbourByName.sql', (harbourname,))[0]
    DB.write_db('sql/teleportToHarbour.sql', ( harbour['lat'], harbour['long'], harbour['mapid'],  boat['id']))
    events.addEventBoatTP(boat, harbour['lat'], harbour['long'], harbour['mapid'])
    return True


def checkDirection(case, newcoord, boat):
    if is_sea(case):
        if case['boatid']:
            return "La case est déjà occupée"
        if case['harbourid']:
            return 'Pour entrer dans le port, cliquez dessus puis sur "entrer"'
        if  boat['mouvementpoint']<newcoord['pa'] :
            return "Vous n'avez plus assez de point de mouvement ("+str(boat['mouvementpoint'])+")"
        return ''
    else:
        return "Un bateau ne va pas sur terre"

def is_sea(case):
    if 'creek' in case['land'] or 'corail' in case['land'] or 'sea' in case['land'] or case['land'] in  ['lava']:
        return True
    return False
def getNewPosition(direction, boat):
    newcoord={}
    newcoord['lat']=boat['lat']
    newcoord['long']=boat['long']
    newcoord['mapid']=boat['mapid']
    if "n" in direction:
        newcoord['long'] -= 1
    if "s" in direction:
        newcoord['long'] += 1
    if "e" in direction:
        newcoord['lat'] += 1
    if "o" in direction:
        newcoord['lat'] -= 1;
    newcoord['deg']=getAngleByDirection(direction)
    return newcoord

def getAngleByDirection(direction):
    if direction == "no":
        deg=-45
    if direction == "n":
        deg=0
    if direction == "ne":
        deg=45
    if direction == "e":
        deg=90
    if direction == "se":
        deg=135
    if direction == "s":
        deg=180
    if direction == "so":
        deg=-135
    if direction == "o":
        deg=-90
    return deg


def mouvementPoints(case, boat):
    if 'weed' in case['land']:
        return 2
    if 'corail' in case['land']:
        return 3
    return 1

def getDirection(newcoord, boat):
    direction=''
    if newcoord['long']<boat['long']:
        direction='n'
    else:
        if newcoord['long']>boat['long']:
            direction='s'
    if newcoord['lat']<boat['lat']:
        direction+='o'
    else:
        if newcoord['lat']>boat['lat']:
            direction+='e'
    return direction


def getPNJDirection(harbour, boat):
    casescloser = DB.request_db('sql/getCaseWithDistanceCloser.sql', {'blat':boat['lat'], 'blong':boat['long'],'hlat':harbour['lat'], 'hlong':harbour['long'] })
    cases = DB.request_db('sql/getCaseWithDistance.sql', {'blat':boat['lat'], 'blong':boat['long'],'hlat':harbour['lat'], 'hlong':harbour['long'] })
    if len(casescloser)>0:
        return getDirection(random.choice(casescloser), boat)
    elif len(cases)>0:
        return getDirection(random.choice(cases), boat)
    else:
        return False
