from . import DBAccess as DB
from . import sail
from . import building
from . import boat as boatutils
from flask import session, g
from bs4 import BeautifulSoup
from PIL import Image
import json
import time
import math
from flask import current_app, flash
import os
import random


mapmax=50
def calculateCoord(boatid, request=False, mapsize=''):
    coord={}
    latlong = False
    if(request==False and mapsize==''):
        boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
        lat = boat['lat']
        long = boat['long']
        coord['xmin'] = max(int(lat)-int(boat['perception']), 0)
        coord['xmax'] = min(int(lat)+int(boat['perception']), mapmax)
        coord['ymin'] = max(int(long)-int(boat['perception']), 0)
        coord['ymax'] = min(int(long)+int(boat['perception']), mapmax)
        coord['mapid']=boat['mapid']
        coord['lat'] = boat['lat']
        coord['long'] = boat['long']
        coord['mapid']=boat['mapid']
        coord['perception']=boat['perception']
        coord['mapmax']=mapmax
        return coord
    else:
        if (boatid!='' and 'x' in str(boatid)):
            position = boatid.split('x')
            if (len(position)==2):
                lat=position[0]
                long=position[1]
                latlong = True
                coord['mapid']=1
        else:
            if (boatid == ''):
                boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))
            if len(boat)>0:
                lat = boat[0]['lat']
                long = boat[0]['long']
                coord['mapid']=boat[0]['mapid']
                latlong = True
    if latlong and request and len(request.args)==0:
        coord['xmin'] = int(lat)-int(mapsize[0])
        coord['xmax'] = int(lat)+int(mapsize[0])
        coord['ymin'] = int(long)-int(mapsize[1])
        coord['ymax'] = int(long)+int(mapsize[1])
    else:
        coord['xmin'] = int(request.args.get('xmin',25-int(mapsize[0])))
        coord['xmax'] = int(request.args.get('xmax', 25+int(mapsize[0])))
        coord['ymin'] = int(request.args.get('ymin', 25-int(mapsize[1])))
        coord['ymax'] = int(request.args.get('ymax', 25+int(mapsize[1])))
        coord['mapid'] = int(request.args.get('mapid', 1))
    if  coord['xmin']<0:
        coord['xmin']=0
        coord['xmax']=int(mapsize[0])*2+1
    if  coord['ymin']<0:
        coord['ymin']=0
        coord['ymax']=int(mapsize[1])*2+1
    if  coord['xmax']>mapmax:
        coord['xmax']=mapmax
        coord['xmin']=mapmax -int(mapsize[0])*2
    if  coord['ymax']>mapmax:
        coord['ymax']=mapmax
        coord['ymin']=mapmax-int(mapsize[1])*2
    return coord


def getSimpleMapInfo(coord, request):
    mapsize = session['mapsize'].split('x')
    carte={}
    coord = calculateCoord(coord, request, mapsize)
    xmin = coord['xmin']
    xmax = coord['xmax']
    ymin = coord['ymin']
    ymax = coord['ymax']
    results = DB.request_db('sql/getMap.sql', (xmin, xmax, ymin, ymax, coord['mapid']))
    map=[[{'content':'<th></th>'} for j in range(0, 202)] for i in range(0,202)]
    for result in results:
        x=result["lat"]
        y=result["long"]
        xy=str(x)+'x'+str(y)
        landcase = getLandCaseForManage(xy)
        landcase = changeCaseBackground(landcase, result['land'])
        if result["harbourid"] :
            landcase.th.insert(0, getImageCase("harbour"))
            map[x][y]['content']='<th id="'+xy+'" class="landable" onclick="manageAddLand('+"'"+xy+"'"+')" background="/statics/images/'+result['land']+'.png"><img class="imgcase" src="/statics/images/harbour.png"></th>'
        else:
            if result["portalid"] and result['active'] :
                landcase.th.insert(0, getImageCase("portal"))
        map[x][y]['content']=str(landcase)

    carte['carte']=createMap(xmin, xmax, ymin, ymax, map)
    carte['xmin']=xmin
    carte['xmax']=xmax
    carte['ymin']=ymin
    carte['ymax']=ymax
    return carte

def getPersoMapInfo(coord, request):
    mapsize = session['mapsize'].split('x')
    carte={}
    coord = calculateCoord(coord, request, mapsize)
    xmin = coord['xmin']
    xmax = coord['xmax']
    ymin = coord['ymin']
    ymax = coord['ymax']
    persomap = getPersoMap(session['boatid'])
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    results = DB.request_db('sql/getMapWithlastSeen.sql', (boat['side'], xmin, xmax, ymin, ymax, coord['mapid']))
    map=[[{'content':'<th style="background-color:lightgray"></th>'} for j in range(0, 202)] for i in range(0,202)]
    g.request_start_time = time.time()

    for result in results:
         x=result["lat"]
         y=result["long"]
         xy=str(x)+'x'+str(y)
         if xy in persomap:
             landcase = getLandCase()
             landcase = changeCaseBackground(landcase, result['land'], result['lastseeninhours'])
             if result["harbourid"] :
                 landcase.th.insert(0, getImageCase("harbour"))
                 landcase.th.insert(1, getTooltipBuilding(result['harbourname'], result['harbourside'], result["harbourid"], False, 'harbour'))
             else:
                 if result["boatid"]==session['boatid'] or result["boatid"] and result['lastseeninhours']<12 and 'creek' not in result['land']:
                     if result['boatfakeside']=='' or result['boatfakeside'] is None :
                         landcase.th.insert(0, getImageCase(result['boattype'], result['lastorientation'], result['boatside']))
                         landcase.th.insert(2, getTooltipBoat(result['boatname'], result['boatside'], result['boatid']))
                     else:
                         fakeside = json.loads(result['boatfakeside'])
                         landcase.th.insert(0, getImageCase(result['boattype'],result['lastorientation'], fakeside['side']))
                         landcase.th.insert(2, getTooltipBoat(fakeside['boatname'], fakeside['side'], fakeside['id']))

                 else:
                     if result["portalid"] and result['active'] and (result['portalside']=='neutral' or boat['side'] in result['portalside']):
                         landcase.th.insert(0, getImageCase("portal"))
                         landcase.th.insert(1, getTooltipBuilding(result['portalname'], result['portalside'], result["portalid"], False, 'portal'))

             map[x][y]['content']=str(landcase)
    print("%.5fs" % (time.time() - g.request_start_time))
    carte['carte']=createMap(xmin, xmax, ymin, ymax, map)
    carte['xmin']=xmin
    carte['xmax']=xmax
    carte['ymin']=ymin
    carte['ymax']=ymax
    return carte

def getMapInfo(boatid):
    carte={}
    coord = calculateCoord(boatid)
    xmin = coord['xmin']
    xmax = coord['xmax']
    ymin = coord['ymin']
    ymax = coord['ymax']
    results = DB.request_db('sql/getMapWithDistance.sql', {'lat':coord['lat'], 'long':coord['long'], 'mapid':coord['mapid'], 'perception':coord['perception'], 'mapmax':coord['mapmax'] })
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    coordlist=[]
    map=[[{'content':''} for j in range(0, 202)] for i in range(0,202)]
    for result in results:
        x=result["lat"]
        y=result["long"]
        casecoord=str(x)+'x'+str(y)
        coordlist.append(casecoord)
        landcase = getLandCase()
        landcase = changeCaseBackground(landcase, result['land'])
        if result["harbourid"] :
            landcase.th.insert(0, getImageCase("harbour"))
            landcase.th.insert(1, getTooltipBuilding(result['harbourname'], result['harbourside'], result["harbourid"], (result['harbourside']=='neutral' or boat['side'] in result['harbourside']) and result['distance']==1, 'harbour', result['distance'], casecoord))
        else:
            if result["boatid"]:
                if 'creek' not in result['land'] or result['distance']<3:
                    if result['boatfakeside']=='' or result['boatfakeside'] is None :
                        landcase.th.insert(0, getImageCase(result['boattype'], result['lastorientation'], result['boatside']))
                        if 'creek' in result['land'] and result['distance']==0:
                            landcase.th.insert(2, getTooltipBoatCreek(result['boatname'], result['boatside'], result['boatid'], result['distance'], casecoord))
                        else:
                            landcase.th.insert(2, getTooltipBoat(result['boatname'], result['boatside'], result['boatid'], result['distance'], casecoord))
                    else:
                        fakeside = json.loads(result['boatfakeside'])
                        landcase.th.insert(0, getImageCase(result['boattype'], result['lastorientation'], fakeside['side']))
                        if 'creek' in result['land'] and result['distance']==0:
                            landcase.th.insert(2, getTooltipBoatCreek(fakeside['boatname'], fakeside['side'], fakeside['id'], result['distance'], casecoord, result['boatid']))
                        else:
                            landcase.th.insert(2, getTooltipBoat(fakeside['boatname'], fakeside['side'], fakeside['id'], result['distance'], casecoord, result['boatid']))
                else:
                    landcase.th.insert(1, getTooltipCreek(casecoord))
            else:
                if result["portalid"] and result['active'] and ((result['portalside']=='neutral' and coord['mapid']==1) or boat['side'] in result['portalside']):
                    landcase.th.insert(0, getImageCase("portal"))
                    landcase.th.insert(1, getTooltipBuilding(result['portalname'], result['portalside'], result["portalid"], result['distance']==1, 'portal'))
                else:
                    if result["objectid"]:
                        landcase.th.insert(0, getImageCase("baril"))
                        landcase.th.insert(1, getTooltipBaril(result['objectname'],  result["objectid"], result['distance']))
                    else:
                        if 'creek' in result['land']:
                            landcase.th.insert(1, getTooltipCreek(casecoord))
                        if result['distance']==1:
                            newcoord={}
                            newcoord['lat']=x
                            newcoord['long']=y
                            newcoord['mapid']=coord['mapid']
                            newcoord['pa']=sail.mouvementPoints(result, boat)
                            if sail.checkDirection(result, newcoord, boat)=='':
                                landcase.th.insert(1, getTooltipMoveLand(result, newcoord, boat))
        map[x][y]['content']=str(landcase)

    carte['carte']=createMap(xmin, xmax, ymin, ymax, map)
    carte['xmin']=xmin
    carte['xmax']=xmax
    carte['ymin']=ymin
    carte['ymax']=ymax
    if coord['mapid']==1:
        updatePersoMap(boatid, coordlist)
        updateSideMap( boat['side'], xmin, xmax, ymin, ymax)
    return carte


def getLandCase():
    return BeautifulSoup('<th class="landcase"></th>', 'html.parser')

def getLandCaseForManage(coord):
    return BeautifulSoup('<th id="'+coord+'" class="landable"  onclick="manageAddLand('+"'"+coord+"'"+')" background="/statics/images/dummy.png"></th>', 'html.parser')

def getImageCase(image, deg='', side=''):
    if deg=='':
        if image=="harbour":
            return BeautifulSoup('<img class="imgcase harbour" src="/statics/images/'+image+'.png">', 'html.parser')
        else:
            return BeautifulSoup('<img class="imgcase" src="/statics/images/'+image+'.png">', 'html.parser')
    else:
        return BeautifulSoup('<span class="boathalo '+side+'halo"></span><img  src="/statics/images/'+image+'.png" class="boat" style="transform: rotate('+str(deg)+'deg);">', 'html.parser')

def getTooltipBuilding(name, side, id, hasaccess, buildingtype, distance=12, coord=''):
    soup = BeautifulSoup('<span class="tooltip notland"><a href="/events/h'+str(id)+'">'+name+' </a><img class="tooltipbutton" src="/statics/images/'+side+'.png"></span>', 'html.parser')
    br='<br/>'
    if hasaccess:
        soup.span.insert(2, BeautifulSoup(br+'<button class="btn btn-success" onclick="enterBuilding('+str(id)+", '"+buildingtype+"'"+');">Entrer</button>', 'html.parser'))
        br=''
    if distance<4:
        soup.span.append(BeautifulSoup(br+'<button class="btn btn-danger" onclick="shootAgainByCanon('+"'"+'h-'+str(id)+"'"+');">Attaquer !</button>', 'html.parser'))
    if coord!='':
        soup.span.append(BeautifulSoup('<button class="btn btn-primary" onclick="inspectWithGoogle('+"'"+coord+"'"+');">Inspecter à la lunette</button>', 'html.parser'))
    return soup

def getTooltipBoat(name, side, id, distance=12, coord='', realid=''):
    soup =  BeautifulSoup('<span class="tooltip notland"><a href="/events/b'+str(id)+'">'+name+'</a> <img class="tooltipbutton" src="/statics/images/'+side+'.png"></img></span>', 'html.parser')
    if distance<4 and distance !=0:
        if realid=='':
            soup.span.append(BeautifulSoup('<br/><button class="btn btn-danger" onclick="shootAgainByCanon('+"'"+'b-'+str(id)+"'"+');">Attaquer !</button>', 'html.parser'))
            if distance<2 and distance !=0:
                soup.span.append(BeautifulSoup('<br/><button class="btn btn-primary" onclick="giveMoney('+"'"+str(id)+"'"+');">Donner de l\'argent !</button>', 'html.parser'))
        else:
            soup.span.append(BeautifulSoup('<br/><button class="btn btn-danger" onclick="shootAgainByCanon('+"'"+'b-'+str(realid)+"'"+');">Attaquer !</button>', 'html.parser'))
            if distance<2 and distance !=0:
                soup.span.append(BeautifulSoup('<br/><button class="btn btn-primary" onclick="giveMoney('+"'"+str(realid)+"'"+');">Donner de l\'argent !</button>', 'html.parser'))

    if coord!='' and distance !=0:
        soup.span.append(BeautifulSoup('<button class="btn btn-primary" onclick="inspectWithGoogle('+"'"+coord+"'"+');">Inspecter à la lunette</button>', 'html.parser'))
    return soup

def getTooltipBoatCreek(name, side, id, distance=12, coord='', realid=''):
    soup =  BeautifulSoup('<span class="tooltip notland"><a href="/events/b'+str(id)+'">'+name+'</a> <img class="tooltipbutton" src="/statics/images/'+side+'.png"></img></span>', 'html.parser')
    soup.span.append(BeautifulSoup('<button class="btn btn-primary" onclick="farmRessources(\''+coord+'\', \'Bois\');">Chercher bois</button>', 'html.parser'))
    soup.span.append(BeautifulSoup('<button class="btn btn-primary" onclick="farmRessources(\''+coord+'\', \'Nourriture/Eau\');">Chercher Nourriture/eau</button>', 'html.parser'))
    return soup

def getTooltipCreek(coord=''):
    soup =  BeautifulSoup('<span class="tooltip"></span>', 'html.parser')
    soup.span.append(BeautifulSoup('<button class="btn btn-primary" onclick="inspectWithGoogle('+"'"+coord+"'"+');">Inspecter à la lunette</button>', 'html.parser'))
    return soup


def getTooltipMoveLand(case, newcoord, boat):
    MP=sail.mouvementPoints(case, boat)
    s=''
    if MP>1:
        s='s'
    direction=sail.getDirection(newcoord, boat)
    return BeautifulSoup('<span class="tooltip">'+str(MP)+' point'+s+' de manœuvre'+s+'<button onclick="sailTo('+"'"+direction+"'"+');">Se déplacer</button></span>', 'html.parser')




def getTooltipBaril(name, id, distance):
    soup = BeautifulSoup('<span class="tooltip notland">'+name+'</span>', 'html.parser')
    if distance==1:
        soup.span.append(BeautifulSoup('<br/><button onclick="getObject('+str(id)+');">Ramasser</button>', 'html.parser'))
    return soup
def changeCaseBackground(case, land, lastseen=0):
    rot=0
    if '-' in land:
        splited = land.split('-')
        land = splited[0]
        rot= getRotation(splited[1])
    if land =='foret':
        case.th.insert(0,  BeautifulSoup('<img class="foret" src="/statics/images/foret.png/">', 'html.parser'))
        case.th.insert(1,  BeautifulSoup('<img class="imgland"  src='+'/statics/images/sand.png/>', 'html.parser'))
    else:
        if land =='montain':
            case.th.insert(0,  BeautifulSoup('<img class="montain" src="/statics/images/montain.png/">', 'html.parser'))
            case.th.insert(1,  BeautifulSoup('<img class="imgland"  src='+'/statics/images/sand.png/>', 'html.parser'))
        else:
            case.th.insert(0,  BeautifulSoup('<img class="imgland"  style="transform: rotate('+str(rot)+'deg);" src='+'/statics/images/'+land+'.png/>', 'html.parser'))
    if lastseen and lastseen>12:
        case.th.insert(0,  BeautifulSoup('<span class ="oldseen"></span>', 'html.parser'))
    return case

def getRotation(rot):
    if rot=='e':
        deg=90
    else:
        if rot=='s':
            deg=180
        else:
            if rot=='o':
                deg=-90
    return deg

def createMap(xmin, xmax, ymin, ymax, map):
    html='<tr><td background="/statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    for x in range(xmin, xmax+1):
        html=html+'<th background="/statics/images/background.jpg"><div class="xy">'+str(x)+'</div></th>'
    html=html+'<td  background="/statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    for y in range(ymin, ymax+1):
        html=html+'</tr><tr align="center"><th background="/statics/images/background.jpg">'+str(y)+'</th>'
        for x in range(xmin, xmax+1):
            html=html+map[x][y]['content']
        html=html+'<th  background="/statics/images/background.jpg"><div class="xy">'+str(y)+'</div></th>'
        html=html+'</tr>'
    html=html+'<td  background="/statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    for x in range(xmin, xmax+1):
        html=html+'<th background="/statics/images/background.jpg">'+str(x)+'</th>'
    html=html+'<td  background="/statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    return html


def getDistance(lat1, long1, lat2, long2):
    return max(abs(lat2-lat1), abs(long2-long1))

def getAngle(lat1, long1, lat2, long2):
    if (lat2-lat1)!=0:
        angle = math.atan((long2-long1)/(lat2-lat1))
        if  lat2>lat1:
            angle=angle+math.radians(90)
        elif  lat2<lat1:
            angle=math.radians(-90)+angle
    else:
        if (long2-long1)>0:
            angle=math.radians(180)
        else:
            angle=0
    return math.degrees(angle)

def sameCase(lat1, long1, lat2, long2):
    if lat1==lat2 and long1==long2:
        return True
    return False


def updatePersoMap(boatid, newCoordlist):
    persomap = DB.request_db('sql/getPersoMap.sql', (boatid,))
    if len(persomap)==0:
        DB.write_db('sql/createPersoMap.sql', (boatid,))
        persomap = DB.request_db('sql/getPersoMap.sql', (boatid,))
    persoMapList = persomap[0]['coordlist'][1:-1].replace("\'", "").split(', ')
    oldlen=len(persoMapList)
    for coord in newCoordlist:
        if coord in persoMapList:
            continue
        else:
            persoMapList.append(coord)
    if len(persoMapList)!=oldlen:
        DB.write_db('sql/updatePersoMap.sql', (str(persoMapList),boatid))



def updateSideMap(xmin, xmax, ymin, ymax, side):
    DB.write_db('sql/updateSideMap.sql', (xmin, xmax, ymin, ymax, side))

def createSideMap():
    for side in ['pirate', 'corsaire']:
        for lat in range(0, mapmax):
            for long in range(0, mapmax):
                DB.write_db('sql/createSideMap.sql', (lat, long, side))
            print(side, lat)

def getPersoMap(boatid):
    persomap = DB.request_db('sql/getPersoMap.sql', (boatid,))
    return persomap[0]['coordlist'][1:-1].replace("\'", "").split(', ')



def importMapFromPng(id):
    im = Image.open('/home/leotrouvtou/carte'+str(id)+'.png')
    imsize = im.size
    pix = im.load()
    f = open('data/colormap.json')
    colors = json.load(f)
    for y in range(0,imsize[0], 1) :
        for x in range(0,imsize[1], 1) :
                color=pix[x,y]
                DB.write_db('sql/createInitialMap.sql', ((x), (y), id, colors[str(color)]))
        print(y)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}        

def createMapPng():
    img = Image.new('RGB', (mapmax*5, mapmax*5), color = (121, 121, 121))
    pix = img.load()
    mapcases = DB.request_db('sql/getFullMap.sql')
    for case in mapcases:
        color = (20, 0, 255)
        if 'sea' in case['land']:
            if 'weed' in case['land']:
                color=(68, 232, 176)
            elif case['land']!='deepsea':
                color=(0, 179, 255)
        elif 'sable' in case['land']:
            color=(255, 255, 0)
        elif 'sand' in case['land']:
            color=(255, 255, 0)
        elif 'foret' in case['land']:
            color=(76, 175, 80)
        elif 'montain' in case['land']:
            color=(134, 118, 89)
        elif 'lava' in case['land']:
            color=(229, 48, 48)
        elif 'creek'  in case['land']:
            color=(255, 255, 0)
        elif 'corail'  in case['land']:
            color=(232, 177, 68)
        pix =colorize(pix, case['lat'], case['long'], (color))
    img.save('globalMap.png')

def getLegend():
    f = open('data/legend.json')
    return json.load(f)

def exportMapPng():
    img = Image.new('RGB', (mapmax, mapmax), color = (121, 121, 121))
    pix = img.load()
    mapcases = DB.request_db('sql/getFullMap.sql')
    f = open('data/mapcolor.json')
    colors = json.load(f)
    for case in mapcases:
        pix[case['lat'], case['long']]= tuple(map(int, colors[case['land']].split(', ')))
    img.save('exportedMap.png')

def addBuildingsToMapPng():
    buildings=building.getBuildings()
    im = Image.open('globalMap.png')
    imsize = im.size
    pix = im.load()
    for harbour in buildings['harbours']:
        if harbour['mapid']==1:
            if harbour['side']=='pirate':
                pix =colorize(pix, harbour['lat'],harbour['long'], (0,0,0))
            elif harbour['side']=='corsaire':
                pix =colorize(pix, harbour['lat'],harbour['long'], (229, 48, 48))
            else:
                pix =colorize(pix, harbour['lat'],harbour['long'], (255, 255, 255))
    im.save('globalMapWithHarbour.png')

def getBoatslistWithColor():
    boats=boatutils.getBoats()
    results={}
    for boat in boats:
        coord=str(boat['lat'])+'x'+str(boat['long'])
        if not boat['inharbour'] and 'creek'not in current_app.mapcache[coord]:
            boatcolor={}
            coord=str(boat['lat'])+'x'+str(boat['long'])
            if boat['fakeside']!='' and boat['fakeside'] is not None:
                fakeside = json.loads(boat['fakeside'])
                boat['side']=fakeside['side']
            if boat['side']=='pirate':
                color = (75, 75, 75)
            elif boat['side']=='corsaire':
                color =  (216, 75, 75)
            else:
                color=pix = (183, 183, 183)
            results[coord]=color
    return results

def createSideMapPng(side):
    hideSideMapPng(side)
    boats = getBoatslistWithColor()
    im = Image.open('globalMapWithHarbour'+side+'.png')
    imsize = im.size
    pix = im.load()
    cases=DB.request_db('sql/getLastSeen.sql', (side,))
    for case in cases:
        if case['lastseeninhours']<12:
            boat = boats.get(case['coord'])
            if boat:
                pix =colorize(pix, case['lat'],case['long'], (boat))
        else:
            pix = fogify(pix, case['lat'],case['long'])
    im.save('restricted/'+side+'sidemap.png')

def hideSideMapPng(side):
    cases=DB.request_db('sql/getNotVisitedCases.sql', (side,))
    im = Image.open('globalMapWithHarbour.png')
    im.putalpha(255)
    imsize = im.size
    pix = im.load()
    for case in cases:
        pix =colorize(pix, case['lat'],case['long'], (121, 121, 121))
    im.save('globalMapWithHarbour'+side+'.png')


def fogify(image, lat, long):
    color = image[5*lat,5*long]
    colorlist=list(color)
    colorlist[3]=128
    newcolor =tuple(colorlist)
    for i in range(0, 4):
        for j in range(0, 4):
            image[5*lat+i, 5*long+j]=newcolor
    return image

def colorize(image, lat, long, color):
    for i in range(0, 4):
        for j in range(0, 4):
            image[5*lat+i, 5*long+j]=color
    return image



def createColorMap():
    f = open('data/mapcolor.json')
    colors = json.load(f)
    ret = {};
    for key in colors:
        ret[colors[key]] = key;
    with open('data/colormap.json', 'w') as outfile:
        outfile.write(str(ret))

def inspectWithGoogle(coord, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    lat = coord.split('x')[0]
    long = coord.split('x')[1]
    if getDistance(boat['lat'], boat['long'], int(lat), int(long))<=boat['perception']:
        if boat['actionpoint']>=3:
            DB.write_db('sql/decrementActionPoint.sql', (3, boatid))
            case = DB.request_db('sql/getCase.sql', (lat, long, boat['mapid']))[0]
            if case['harbourid']:
                harbour =  DB.request_db('sql/getHarbour.sql', (case['harbourid'],))[0]
                boats = DB.request_db('sql/getBoatsInHarbour.sql', (case['harbourid'],))
                pops=DB.request_db('sql/getPops.sql', (case['harbourid'],))
                result = "<h2>Observations : Port {} ({})</h2>".format(harbour['name'], harbour['side'])
                if len(pops)>0:
                    result = result+ "<h3>Population dans le port : </h3>"
                    for pop in pops:
                        result = result + "{} {}".format(pop['number'], pop['side'])
                        if pop['number']>1:
                            result = result +'s'
                        if pop!=pops[len(pops)-1]:
                            result = result +', '
                        else:
                            result = result +'</br>'
                if len(boats)>0:
                    result = result+ "<h3>Bateau dans le port : </h3>"
                    for curboat in boats:
                        result = result + "{} ({})".format(curboat['name'],curboat['side'])+')  <button class="btn btn-danger" onclick="shootAgainByCanon('+"'"+'b-'+str(curboat['id'])+"'"+');">Attaquer !</button><br>'
                else:
                    result = result+ "<br/>Aucun navire dans le port"
            elif case['boatid']:
                curboat =  DB.request_db('sql/getBoatCase.sql', (case['boatid'],))[0]
                result = "<h2>Observations : Navire {} ({}) en {}</h2>".format(curboat['boatname'], curboat['side'], coord)
                if curboat['fakeside']=='' or curboat['fakeside'] is None:
                    fakeside=''
                else:
                    fakeside = json.loads(curboat['fakeside'])
                    result = result+"<br/>Navire navigant sous les pavillons de {} ({})".format(fakeside['boatname'], fakeside['side'])
                percent = math.floor(100*curboat['boatstructure']/curboat['boatstructuremax'])
                state="nickel"
                if percent<60 and percent>30:
                    state="abimé"
                elif percent<=30 and percent>10:
                    state="très abimé"
                elif percent<=10:
                    state="presque détruit"
                result = result+"<br/>Navire {}".format(state)
            else:
                result = "<h2>Observations :</h2> Personne dans la crique en "+coord
            flash(result)
        else:
            flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")
    else:
        flash("La lunette ne voit pas aussi loin")


def getRandomSeaOnSide():
    pos={"lat":0,"long":0}
    
    place = random.choice(['n','s','e','o'])
    casemax =mapmax-1
    if place =='n':
        while 1:
            pos["lat"]=random.randint(0, casemax)
            case = DB.request_db('sql/getCase.sql', (pos['lat'],pos['long'], 1))[0]
            if sail.is_sea(case) and not case['boatid'] and not case['harbourid']:
                return case
    if place =='s':
        while 1:
            pos['long']=casemax
            pos["lat"]=random.randint(0, casemax)
            case = DB.request_db('sql/getCase.sql', (pos['lat'],pos['long'], 1))[0]
            if sail.is_sea(case) and not case['boatid'] and not case['harbourid']:
                return case
    if place =='o':
        while 1:
            pos['long']=random.randint(0, casemax)
            case = DB.request_db('sql/getCase.sql', (pos['lat'],pos['long'], 1))[0]
            if sail.is_sea(case) and not case['boatid'] and not case['harbourid']:
                return case
    if place =='e':
        while 1:
            pos['long']=random.randint(0, casemax)
            pos["lat"]=casemax
            case = DB.request_db('sql/getCase.sql', (pos['lat'],pos['long'], 1))[0]
            if sail.is_sea(case) and not case['boatid'] and not case['harbourid']:
                return case
