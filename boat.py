from . import DBAccess as DB
from . import sail
from . import maputils
from . import pnj as pnjutils
from . import DD
from . import events
from . import building
from . import canon
from. import xp

from datetime import datetime, timedelta
from flask import flash
import json
import math
from flask import current_app

def createBoat(boatname, userid, side):
    harbour=DB.request_db('sql/getCapital.sql', (side,))[0]
    DB.write_db('sql/createBoat.sql', (boatname, harbour['lat'], harbour['long'], side))
    boat=DB.request_db('sql/getBoatByName.sql', (boatname,))[0]
    DB.write_db('sql/updateUserBoatId.sql', (boat['id'], userid))

def getHold(boatid):
    results ={}
    results['boat'] = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    results['holds']=DB.request_db('sql/getBoatHold.sql', ( boatid,))
    results['treasures']=DB.request_db('sql/myTreasureList.sql', ( boatid,))
#    results['totalweight'] = DB.request_db('sql/getBoatTotalHold.sql', (boatid,))[0]['totalweight']
    return results

def destroyBoat(boat):
    stockid=boat['stockid']
    thunelost = max(round(0.1*boat['thune']), 20)
    if (stockid=='' or stockid is None):
        createAndAssignStock(boat['id'])
        boat = DB.request_db('sql/getBoat.sql', (boat['id'],))[0]
    DB.write_db('sql/createNewFloatingObject.sql', (boat['lat'], boat['long'], boat['stockid'], "Restes de "+boat['boatname'], boat['mapid']))
    DB.write_db('sql/decrementThune.sql', {'boatid':boat['id'], 'price':min(thunelost, boat['thune'])})
    content = getRessourcesList(stockid)
    building.getOrCreateRessource(thunelost, 'Pièces de huit', '', boat['stockid'])
    building.getOrCreateRessource(1, 'Pavillon', boat['boatname'], boat['stockid'], boat['id'] )
    content = content +", {} pièces de huit et son pavillon".format(thunelost)
    events.addEventBoatLooseRessources(boat, content)
    DB.write_db('sql/removeStockFromBoat.sql', ( boat['id'], ))
    sail.teleportToHarbour('Pandémonium', boat)
    DB.write_db('sql/resetBoatDamage.sql', (boat['id'],))

def hasFlag(sourceid, boatid):
    flags = DB.request_db('sql/getFlagFromBoat.sql', (boatid, sourceid))
    if len(flags)>0:
        return True
    return False

def changeFlag(sourceid, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if boat['actionpoint']>=5:
        if hasFlag(sourceid, boatid):
            fakeboat = DB.request_db('sql/getBoat.sql', (sourceid,))[0]
            fakeside={}
            fakeside['side']=fakeboat['side']
            fakeside['boatname']=fakeboat['boatname']
            fakeside['id']=fakeboat['id']
            DB.write_db('sql/decrementActionPoint.sql', (5, boatid))
            DB.write_db('sql/changeFakeSide.sql', (json.dumps(fakeside), boatid))
            flash("Vous venez d'installer le drapeau de "+fakeboat['boatname'])
    else:
        flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")


def removeFlag(boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if boat['actionpoint']>=2:
        DB.write_db('sql/changeFakeSide.sql', ('', boatid))
        DB.write_db('sql/decrementActionPoint.sql', (2, boatid))
        flash("Vous venez de remettre votre drapeau")
    else:
        flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")

def getRessourcesList(stockid):
    content =''
    ressources =  DB.request_db('sql/getRessources.sql', (stockid,))
    for ressource in ressources:
        if ressource['side']!='':
            if ressource['type'] not in ['Population', 'Pavillon', 'Pièces de huit']:
                content = content+"{} tonneaux de {} ({})".format(str(ressource['number']), ressource['type'], ressource['side'])
            else:
                content = content+"{} {} ({})".format(str(ressource['number']), ressource['type'], ressource['side'])
        else:
            if ressource['type'] not in ['Population', 'Pavillon', 'Pièces de huit']:
                content = content+"{} tonneaux de {}".format(str(ressource['number']), ressource['type']
                )
            else:
                content = content+"{} {}".format(str(ressource['number']), ressource['type'])
        if ressource != ressources[len(ressources)-1]:
            content = content+', '
    return content

def putRessourceIntoSea(ressourceid, amount, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    ressource = DB.request_db('sql/getRessource.sql', (ressourceid,))[0]
    if boat['inharbour']:
        flash("Merci de ne pas polluer le port")
        return ''
    else:
        if boat['stockid']==ressource['stockid']:
            if amount<=ressource['number']:
                stockid = createFloatingObject(boat, ressource['type'])
                DB.write_db('sql/decrementRessource.sql', (amount, ressource['id']))
                if amount==ressource['number']:
                    DB.write_db('sql/deleteRessource.sql', (ressource['id'],))
                building.getOrCreateRessource(amount, ressource['ressourceid'], ressource['side'], stockid, ressource['sourceid'])
                events.addEventBoatPutIntoSeaRessource(boat, amount, ressource['type'])
                return True
            else:
                flash("Vous voulez jeter plus qu'il n'y en a")
                return ''

def createFloatingObject(boat, ressourceType):
    stock=createStock()
    DB.write_db('sql/createNewFloatingObject.sql', (boat['lat'], boat['long'], stock['id'], "Tonneaux de "+ressourceType, boat['mapid']))
    return stock['id']

def getObject(objectid, boatid):
    boat=DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if boat['actionpoint']>=2:
        object=DB.request_db('sql/getObjectById.sql', (objectid,))[0]
        if maputils.getDistance(boat['lat'], boat['long'], object['lat'], object['long'])==1:
            boat=getOrCreateBoatWithStockid(boat)
            DB.write_db('sql/decrementActionPoint.sql', (2, boatid))
            content = assignObjectContent(boat['stockid'], object['stockid'])
            events.addEventBoatGetFloatingObject(boat, object['name'], content)
            flash('"'+object['name']+'" ont été ramassés')
            DB.write_db('sql/deleteFloatingObject.sql', ( object['id'],))
        else:
            flash('"'+object['name']+'" est trop loin')
    else:
        flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")
    return True


def assignObjectContent(boatstockid, objectstockid):
    ressources =  DB.request_db('sql/getRessources.sql', (objectstockid,))
    for ressource in ressources:
        building.getOrCreateRessource(ressource['number'], ressource['ressourceid'], ressource['side'], boatstockid, ressource['sourceid'])
    return  getRessourcesList(objectstockid)

def createStock():
    DB.write_db('sql/createStock.sql')
    stock=DB.request_db('sql/getLastStock.sql')[0]
    return stock

def createAndAssignStock(boatid):
    stock=createStock()
    DB.write_db('sql/assignStockToBoat.sql', (stock['id'], boatid))

def repair(boatid):
    boat=DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if  boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
    if boat['inharbour'] and harbour['active'] :
        paNeeded = 2
        hasWood = 8
    else:
        paNeeded = 4
        hasWood = hasWoodToRepair(boat)
    if boat['actionpoint']>=paNeeded:
        if boat['boatstructure']<boat['boatstructuremax']:
            torepair=DD.dice(hasWood, boat['repair'])
            newPS= min(boat['boatstructure']+torepair, boat['boatstructuremax'])
            DB.write_db('sql/modifyPSBoat.sql', (newPS, boatid))
            DB.write_db('sql/decrementActionPoint.sql', (paNeeded, boatid))
            if hasWood==4:
                torepair = str(torepair)+" (vous avez utilisé une ressource bois)"
            elif hasWood==8:
                torepair = str(torepair)+" (Chantier Naval)"
            flash("Vous avez réparé le bateau de "+str(torepair)+" points de structure ("+str(newPS)+"/"+str(boat['boatstructuremax'])+")")
            events.addEventRepair(boat, torepair, newPS)
        else:
            flash("Le bateau est déjà tout neuf !")
    else:
       flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")


def getOrCreateBoatWithStockid(boat):
    if not hasStockid(boat):
        createAndAssignStock(boat['id'])
        boat = DB.request_db('sql/getBoat.sql', (boat['id'],))[0]
    return boat


def hasStockid(boat):
    if (boat['stockid']=='' or boat['stockid'] is None):
        return False
    return True

def hasWoodToRepair(boat):
    boat = getOrCreateBoatWithStockid(boat)
    ressource = DB.request_db('sql/getRessourceByName.sql', ('Bois', boat['stockid']))
    if len(ressource)>0 and ressource[0]['number']>0:
        DB.write_db('sql/decrementRessource.sql', (1, ressource[0]['id']))
        if 1==ressource[0]['number']:
            DB.write_db('sql/deleteRessource.sql', (ressource[0]['id'],))
        return 4
    else:
        return 1

def getRessourceInHold(name, stockid):
    ressource = DB.request_db('sql/getRessourceByName.sql', (name, stockid))
    if len(ressource)>0 and ressource[0]['number']>0:
        return ressource[0]
    return False


def getBoats():
    boats=DB.request_db('sql/getBoatsFullMap.sql')
    return boats

def newTurn(boatid, pnj=False):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    message = "Nouveau tour ! "
    PmMalus = calculatePMMalus(boat)
    if boat['inharbour']:
        malusPa=0
    else:
        malusPa=calculatePaMalus(boat)
    if pnj:
        DB.write_db('sql/reloadTurn.sql', (malusPa, PmMalus['malus'],'pirate', pnj, boatid))
    else:
        DB.write_db('sql/reloadTurn.sql', (malusPa, PmMalus['malus'],'corsaire', pnj, boatid))
    maluses=PmMalus['message']
    if malusPa!=0:
        maluses=maluses+" L'équipage a faim et soif et travaille moins."
    else:
        if not boat['inharbour']:
            maluses=maluses+" Vous avez utilisé un tonneau de Nourriture/Eau."
        else:
            maluses=maluses+" Dans un port vous ne consommez pas de ressources."
    events.addEventBoatNewTurn(boat, maluses)
    if boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        if harbour['side'] not in [ 'neutral', boat['side']]:
            message = "Nouveau tour ! Le port est ennemi et vous a tiré dessus !"
            canon.shootH2B(boat, harbour)
    flash(message+maluses)

def calculatePMMalus(boat):
    result={}
    malusPmS=calculatePmMalusStructure(boat)
    malusPmE=calculatePmMalusEnc(boat)
    malusPm=min(boat['mouvementpointmax']-1, malusPmS+malusPmE)
    maluses=''
    if malusPmS!=0:
        maluses=maluses+' Le bateau est endomagé et avance moins, '
    if malusPmE!=0:
        maluses=maluses+' Le bateau est trop chargé et avance moins, '
    if malusPm!=0:
        maluses=maluses+'(-'+str(malusPm)+').'
    result['malus']=malusPm
    result['message']=maluses
    return result

def calculatePmMalusStructure(boat):
    percent = math.floor(100*boat['boatstructure']/boat['boatstructuremax'])
    malus=0
    if percent<60 and percent>30:
        malus=2
    elif percent<=30 and percent>10:
        malus=4
    elif percent<=10:
        malus=6
    return malus

def calculatePmMalusEnc(boat):
    if boat['stockid']=='' or boat['stockid'] is None or boat['totalweight']<boat['maxweight']:
        return 0
    else:
        if boat['totalweight']>2*boat['maxweight']:
            return boat['mouvementpointmax']-1
        else:
            return math.ceil((boat['totalweight']-boat['maxweight'])/boat['maxweight']*math.ceil(boat['mouvementpointmax']/2))

def calculatePaMalus(boat):
    boat= getOrCreateBoatWithStockid(boat)
    if boat['inharbour']:
        malus=0
    else:
        ressource = DB.request_db('sql/getRessourceByName.sql', ('Nourriture/Eau', boat['stockid']))
        if len(ressource)>0 and ressource[0]['number']>0:
            malus=0
            DB.write_db('sql/decrementRessource.sql', (1, ressource[0]['id']))
            if 1==ressource[0]['number']:
                DB.write_db('sql/deleteRessource.sql', (ressource[0]['id'],))
        else:
            malus=2
    return malus

def addCanon(boatid, ressourceid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    ressource = DB.request_db('sql/getRessource.sql', (ressourceid,))[0]
    if boat['actionpoint']>=3:
        if boat['stockid']==ressource['stockid'] and boat['canon']<boat['canonmax']:
            if ressource['type']=='Canon':
                DB.write_db('sql/decrementRessource.sql', (1, ressource['id']))
                DB.write_db('sql/incrementCanon.sql', (boatid,))
                DB.write_db('sql/decrementActionPoint.sql', (3, boatid))
                if 1==ressource['number']:
                    DB.write_db('sql/deleteRessource.sql', (ressource['id'],))
                return True
            else:
                flash("Vous ne pouvez installer que des canons")
        else:
            flash("Vous n'avez plus de place pour un nouveau canon")
    else:
        flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")


def changeBoat(boatid, boatType):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    members = xp.getMembers(boat['id'])
    if boat['inharbour']:
        if  boat['actionpoint']<boat['actionpointmax'] or boat['mouvementpoint']<boat['mouvementpointmax']:
            flash("Il faut avoir tous ses points d'action et tous ses points de manoeuvre pour changer de navire.")
        elif boat['boatstructure']<boat['boatstructuremax']:
            flash("Vous devez rendre un navire en parfait état.")
        elif boat['canon']<boat['canonmax']:
            flash("Vous devez rendre un navire avec tous ses canons.")
        else:
            newBoat=calculateBoat(boatType, members)
            newBoat['id']=boatid
            DB.write_db('sql/removePaPm.sql', (boatid,))
            DB.write_db('sql/changeBoat.sql', newBoat)
    else:
        flash("Vous devez être dans un port")

def calculateBoat(boatType, members):
    boatList = {"fregate":{"structure":200, "mouvement":15, "canon":18,"range": 3, "maxweight":50},
                "brick":{"structure":75, "mouvement":30, "canon":6,"range": 2, "maxweight":25},
                 "flute":{"structure":300, "mouvement":10, "canon":12,"range": 3, "maxweight":200},
                 "bigZombi":{"structure":300, "mouvement":10, "canon":24,"range": 3, "maxweight":200},
                 "middleZombi":{"structure":200, "mouvement":15, "canon":18,"range": 3, "maxweight":200},
                "littleZombi":{"structure":75, "mouvement":30, "canon":12,"range": 3, "maxweight":200},
                }
    newBoat=boatList[boatType]
    newBoat['type']=boatType
    newBoat["structure"]=newBoat["structure"]+members['charpentierlvl']*math.ceil(newBoat["structure"]/8)
    newBoat["mouvement"]=newBoat["mouvement"]+members['navigateurlvl']
    newBoat["range"]=newBoat["range"]+members['vigielvl']
    return newBoat


def sunkALittle():
    objects = DB.request_db('sql/getObjects.sql')
    for obj in objects:
        removed=0
        ressources =  DB.request_db('sql/getRessources.sql', (obj['stockid'],))
        for ressource in ressources:
            toRemove = math.ceil(ressource['number']*0.1)
            DB.write_db('sql/decrementRessource.sql', (math.ceil(ressource['number']*0.1), ressource['id']))
            if toRemove==ressource['number']:
                DB.write_db('sql/deleteRessource.sql', (ressource['id'],))
                removed = removed+1
        if removed == len(ressources):
            DB.write_db('sql/deleteFloatingObject.sql', (obj['id'],))


def reloadTurnPNJ():
    pnjs = DB.request_db('sql/getPNJInMap.sql')
    for pnj in pnjs:
        difference=datetime.now() - datetime.strptime(pnj['lastturn'], '%Y-%m-%d %H:%M:%S')
        if (difference.days*24+difference.seconds/3600)>current_app.config['timehour']:
            newTurn(pnj['id'], True)
        pnjutils.moveAndShoot(pnj)
