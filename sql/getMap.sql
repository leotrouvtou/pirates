SELECT
map.lat,
map.long,
land,
boat.id as boatid,
boatname,
boat.side as boatside,
harbour.id as harbourid,
boat.type as boattype,
harbour.name as harbourname,
harbour.side as harbourside,
portal.id as portalid,
portal.name as portalname,
portal.side as portalside,
portal.active as active,
floatingobject.id as objectid,
floatingobject.name as objectname,
lastorientation
from map
left join boat on map.lat = boat.lat and map.long=boat.long and map.mapid=boat.mapid
left join harbour on map.lat = harbour.lat and map.long=harbour.long and map.mapid=harbour.mapid
left join portal on map.lat = portal.lat and map.long=portal.long and map.mapid=portal.mapid
left join floatingobject on map.lat = floatingobject.lat and map.long=floatingobject.long and map.mapid=floatingobject.mapid
where map.lat>=? and map.lat<=? and map.long>=? and map.long<=? and map.mapid= ? order by map.long asc, map.lat asc;
