WITH cte AS (
SELECT  id,
        lat,
        long,
        mapid,
        ROW_NUMBER() OVER (
            PARTITION BY
                lat, long, mapid
                ORDER BY
                id desc
                ) row_num
                FROM
        map
        )
delete from map where id in (select id from cte where row_num > 1);
select * from cte where row_num>1;
