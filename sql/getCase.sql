select
map.lat as lat,
map.long as long,
land,
boat.id as boatid,
harbour.id as harbourid
from map
left join boat on map.lat = boat.lat and map.long=boat.long and map.mapid=boat.mapid
left join harbour on map.lat = harbour.lat and map.long=harbour.long and map.mapid=harbour.mapid
where map.lat=? and map.long=? and map.mapid=?;
