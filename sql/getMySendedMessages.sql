-- select message.id as id, toids, fromid, created, subject, boat.boatname as expname from message join boat on boat.id=fromid where fromid=? group by content order by created desc;
select
group_concat(distinct boatname||' ('||side||')') as names,
mess.id as id,
toids,
fromid,
created,
subject
from message as mess
join boat
on mess.fromid=?
where boat.id in (select distinct toid from message where toids=mess.toids) group by mess.content order by created desc;
