alter table boat add column lat integer default 1 not null;
alter table boat add column long integer default 1 not null;
update boat set lat=boatlat, long=boatlong;


alter table boat drop column boatlat;
alter table boat drop column boatlong;
