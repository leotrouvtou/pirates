drop table if exists map;
CREATE TABLE map
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    mapid integer not null,
    land text,
    lat integer not null,
    long integer not null,
    Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);


drop table if exists boat;
CREATE TABLE boat
(
    id integer PRIMARY KEY autoincrement  NOT NULL ,
    mapid integer default 1,
    boatname text not null UNIQUE,
    lat integer not null,
    long integer not null,
    side text not null,
    type text default 'fregate',
    fakeside text,
    lasttack text,
    lastorientation text,
    boatstructure integer default 200,
    boatstructuremax integer default 200,
    actionpoint integer default 10,
    actionpointmax integer default 10,
    mouvementpoint integer default 15,
    mouvementpointmax integer default 15,
    canon integer default 6,
    canonmax integer default 6,
    perception integer default 5,
    range integer default 3,
    precision integer default 60,
    repair integer default 20,
    inharbour boolean default false,
    stockid integer,
    thune integer default 0,
    xpi integer default 0,
    xp integer default 0,
    level integer default 0,
    hasmessage boolean default 0,
    lastturn DATETIME DEFAULT CURRENT_TIMESTAMP
);

drop table if exists member;
CREATE TABLE member
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    boatid integer,
    canonierlvl integer default 0,
    vigielvl integer default 0,
    maitrelvl integer default 0,
    voilierlvl integer default 0,
    navigateurlvl integer default 0,
    charpentierlvl integer default 0
);

drop table if exists harbour;
CREATE TABLE harbour
(
    id integer PRIMARY KEY autoincrement  NOT NULL ,
    mapid integer not null,
    name text not null,
    description text,
    capital boolean,
    lat integer not null,
    long integer not null,
    side text default "neutral",
    action integer,
    defense integer,
    maxpop integer,
    stockid integer,
    active integer default 1
);

drop table if exists portal;
CREATE TABLE portal
(
    id integer PRIMARY KEY autoincrement  NOT NULL ,
    mapid integer not null,
    name text not null,
    lat integer not null,
    long integer not null,
    side text default "neutral",
    toharbour integer
);

drop table if exists user;
CREATE TABLE user
(
 id integer PRIMARY KEY autoincrement  NOT NULL ,
 email VARCHAR(100) UNIQUE,
 pseudo VARCHAR(100),
 password VARCHAR(100),
 boatid integer,
 role VARCHAR(100),
 is_active boolean,
 is_authenticated boolean,
 mapsize text default '17x15',
 Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);

drop table if exists stock;
CREATE TABLE stock
(
  id integer PRIMARY KEY autoincrement  NOT NULL,
  totalweight integer
);


drop table if exists floatingobject;
CREATE TABLE floatingobject
(
  id integer PRIMARY KEY autoincrement  NOT NULL,
  stockid integer,
  lat integer,
  long integer,
  name text,
  mapid integer,
  picture text default 'baril'
);

drop table if exists treasure;
CREATE TABLE treasure
(
  id integer PRIMARY KEY autoincrement  NOT NULL,
  stockid integer,
  lat integer,
  long integer,
  name text,
  mapid integer,
  boatid integer
);

drop table if exists ressources;
CREATE TABLE ressources
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    number  integer,
    type text,
    side  text default "neutral",
    weight integer,
    stockid integer
);


drop table if exists population;
CREATE TABLE population
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    harbourid integer,
    number  integer,
    side  text default "neutral"
);


insert into harbour (name, description, capital, lat, long, mapid, side, maxpop) values ("Meele Island", 'Capitale des pirates',1,44,10,1,'pirate', 1000);

insert into harbour (name, description, capital, lat, long, mapid, side, maxpop) values ("Cayoune", 'Capitale des corsaires',1,86,82,1,'corsaire', 1000);

insert into harbour (name, description, capital, lat, long, mapid, side, maxpop) values ("Pandémonium", 'Capitale des enfers',0,10,10,2,'neutre', 1000);

drop table if exists events;
CREATE TABLE events
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    eventtype text,
    referenceid integer,
    Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
    personal boolean ,
    comment text
);


drop table if exists quest;
CREATE TABLE quest
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    questtype text,
    harbourid integer,
    side text,
    description text,
    validation text,
    award text
);


drop table if exists pagetext;
CREATE TABLE pagetext
(
    page text,
    content text
);

drop table if exists boatquestlink;
CREATE TABLE boatquestlink
(
    questid integer,
    boatid integer,
    complete boolean default false
);


drop table if exists persomap;
CREATE TABLE persomap
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    coordlist text,
    boatid integer
);


drop table if exists price;
CREATE TABLE price
(
    harbourid integer,
    type text,
    sellprice integer,
    buyprice integer
);

drop table if exists lastseen;
CREATE TABLE lastseen
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    lat integer,
    long integer,
    side text,
    Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);


drop table if exists victorypoint;
CREATE TABLE victorypoint
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    side text,
    points integer,
    Comment text,
    Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX lastseen_coord_side_idx ON lastseen (lat, long, side);

drop table if exists message;
CREATE TABLE message
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    fromid integer,
    toid integer,
    toids integer,
    subject text,
    content text,
    readed boolean default 0,
    created DATETIME DEFAULT CURRENT_TIMESTAMP
);



CREATE TRIGGER IF NOT EXISTS update_active_harbour_modify
   AFTER update
   ON harbour
BEGIN
update harbour set active = ((NEW.defense > NEW.maxpop*0.8) AND (select count(*) from boat where boat.lat>NEW.lat-6 and boat.lat<NEW.lat+6 and boat.long>NEW.long-6 and boat.long<NEW.long+6 and boat.mapid=NEW.mapid and boat.side!=NEW.side)<6) where harbour.id=NEW.id;
update portal set active = ((NEW.defense > NEW.maxpop*0.8) AND (select count(*) from boat where boat.lat>NEW.lat-6 and boat.lat<NEW.lat+6 and boat.long>NEW.long-6 and boat.long<NEW.long+6 and boat.mapid=NEW.mapid and boat.side!=NEW.side)<6) where portal.toharbour=NEW.id;

END;
CREATE TRIGGER IF NOT EXISTS update_active_harbour_on_boat_mouvement
   AFTER update
   ON boat
BEGIN
update harbour set name=name;

end;

update ressourcetype set weight=1;


update ressourcetype set weight=0 where name="Pavillon";
update ressourcetype set weight=0 where name="Pièces de huit";

drop TRIGGER update_total_weight_stockid;
CREATE TRIGGER IF NOT EXISTS update_total_weight_stockid
AFTER update
   ON ressources
BEGIN
update stock set totalweight=(select COALESCE(sum(number*ressourcetype.weight),0) from ressources  join ressourcetype on ressourceid = ressourcetype.id  where stockid=NEW.stockid) where stock.id=new.stockid;
end;

drop TRIGGER create_total_weight_stockid;
CREATE TRIGGER IF NOT EXISTS create_total_weight_stockid
AFTER insert
   ON ressources
BEGIN
update stock set totalweight=(select COALESCE(sum(number*ressourcetype.weight),0) from ressources  join ressourcetype on ressourceid = ressourcetype.id where stockid=NEW.stockid) where stock.id=new.stockid;
end;

CREATE TRIGGER IF NOT EXISTS delete_total_weight_stockid
AFTER delete
   ON ressources
BEGIN

update ressources set number=number;

end;
CREATE TRIGGER IF NOT EXISTS update_hasmessage_insert
   AFTER INSERT
   ON message
BEGIN
update boat set hasmessage = 1 where boat.id=NEW.toid;
END;

CREATE TRIGGER IF NOT EXISTS update_hasmessage_update
   AFTER UPDATE
   ON message
BEGIN
update boat set hasmessage = (select count(*) from message where toid=boat.id and readed=0 limit 1) where boat.id=NEW.toid;
END;

CREATE TRIGGER IF NOT EXISTS update_defense_insert
   AFTER INSERT
   ON population
BEGIN
update harbour set defense = (select sum(number) from population where harbourid=NEW.harbourid) where harbour.id=NEW.harbourid;
END;

CREATE TRIGGER IF NOT EXISTS update_defense_update
   AFTER UPDATE
   ON population
BEGIN
update harbour set defense = (select sum(number) from population where harbourid=NEW.harbourid) where harbour.id=NEW.harbourid;
END;

CREATE TRIGGER IF NOT EXISTS update_defense_delete
   AFTER DELETE
   ON population
BEGIN
update harbour set defense = (select sum(number) from population where harbour.id=harbourid);
END;

CREATE TRIGGER IF NOT EXISTS update_harbour_side_to_portal_update
   AFTER UPDATE
   ON harbour
BEGIN
update portal set side = NEW.side where toharbour=NEW.id;
END;

update harbour set defense = (select sum(number) from population where harbour.id=harbourid);


update boat set xp=(select count(*)*4 from events where instr(comment, 'a attaqué')>0 and personal=0 and eventtype='boat' and events.referenceid=boat.id group by referenceid);

update boat set xp=xp+(select count(*)*1 from events where instr(comment, 'a été attaqué')>0 and personal=0 and eventtype='boat' and events.referenceid=boat.id group by referenceid);

alter table boat add COLUMN canon int default 18;
alter table boat add COLUMN canonmax int default 18;
alter table harbour add COLUMN canonmax int default 18;
alter table harbour add COLUMN canon int default 18;
update boat set canon=18, canonmax=18;
update harbour set canon=18, canonmax=18;

alter table boat add COLUMN pnj boolean default O;

update boat set pnj=1, side="pirate", mapid=2, inharbour=0 where lastturn < date('now','start of month','-5 month');

