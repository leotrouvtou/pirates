SELECT
map.lat,
map.long,
land,
boat.id as boatid,
boatname,
boat.side as boatside,
boat.type as boattype,
boat.fakeside as boatfakeside,
harbour.id as harbourid,
harbour.name as harbourname,
harbour.side as harbourside,
portal.id as portalid,
portal.name as portalname,
portal.side as portalside,
portal.active as active,
lastorientation,
Cast ((julianday('now') - julianday(lastseen.Timestamp)) * 24 As Integer) as lastseeninhours
from map
left join boat on map.lat = boat.lat and map.long=boat.long and map.mapid=boat.mapid
left join lastseen on map.lat = lastseen.lat and map.long = lastseen.long and map.mapid=1 and lastseen.side=?
left join harbour on map.lat = harbour.lat and map.long=harbour.long and map.mapid=harbour.mapid
left join portal on map.lat = portal.lat and map.long=portal.long and map.mapid=portal.mapid
where map.lat>=? and map.lat<=? and map.long>=? and map.long<=? and map.mapid= ? order by map.long asc, map.lat asc;
