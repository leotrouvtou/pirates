UPDATE member SET
 canonierlvl = canonierlvl + (CASE WHEN :member = 'canonier' THEN 1 ELSE 0 END),
 vigielvl = vigielvl + (CASE WHEN :member = 'vigie' THEN 1 ELSE 0 END),
 maitrelvl = maitrelvl + (CASE WHEN :member = 'maitre' THEN 1 ELSE 0 END),
 navigateurlvl =  navigateurlvl + (CASE WHEN :member = 'navigateur' THEN 1 ELSE 0 END),
 voilierlvl = voilierlvl + (CASE WHEN :member = 'voilier' THEN 1 ELSE 0 END),
 charpentierlvl = charpentierlvl + (CASE WHEN :member = 'charpentier' THEN 1 ELSE 0 END)
WHERE boatid = :boatid;
