update boat set
precision=precision + (CASE WHEN :member = 'canonier' THEN 5 ELSE 0 END),
perception=perception + (CASE WHEN :member = 'vigie' THEN 1 ELSE 0 END),
actionpointmax=actionpointmax + (CASE WHEN :member = 'maitre' THEN 1 ELSE 0 END),
mouvementpointmax=mouvementpointmax + (CASE WHEN :member = 'navigateur' THEN 1 ELSE 0 END),
repair=repair + (CASE WHEN :member = 'voilier' THEN 2 ELSE 0 END),
boatstructuremax=boatstructuremax + (CASE WHEN :member = 'charpentier' THEN 25 ELSE 0 END)
where id=:boatid;
