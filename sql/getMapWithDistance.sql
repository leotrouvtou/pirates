SELECT
map.lat,
map.long,
land,
boat.id as boatid,
boatname,
boat.side as boatside,
boat.type as boattype,
boat.fakeside as boatfakeside,
harbour.id as harbourid,
harbour.name as harbourname,
harbour.side as harbourside,
portal.active as active,
portal.id as portalid,
portal.name as portalname,
portal.side as portalside,
floatingobject.id as objectid,
floatingobject.name as objectname,
lastorientation,
max(abs(map.lat-:lat), abs(map.long-:long)) as distance
from map
left join boat on map.lat = boat.lat and map.long=boat.long and map.mapid=boat.mapid
left join harbour on map.lat = harbour.lat and map.long=harbour.long and map.mapid=harbour.mapid
left join portal on map.lat = portal.lat and map.long=portal.long and map.mapid=portal.mapid
left join floatingobject on map.lat = floatingobject.lat and map.long=floatingobject.long and map.mapid=floatingobject.mapid
where map.lat>=max(:lat-:perception, 0) and map.lat<=min(:lat+:perception, :mapmax) and map.long>=max(:long-:perception, 0) and map.long<=min(:long+:perception, :mapmax) and map.mapid=:mapid order by map.long asc, map.lat asc;
