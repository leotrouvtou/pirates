alter table ressources add column ressourceid integer;

alter table price add column ressourceid integer;

drop table if exists ressourcetype;
CREATE TABLE ressourcetype
(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    name text unique,
    weight integer
);

insert into ressourcetype (name)  select distinct type from ressources;
update ressources set ressourceid ='';
UPDATE ressources
SET ressourceid = (SELECT id
                  FROM ressourcetype
                  WHERE name = ressources.type);
UPDATE price
SET ressourceid = (SELECT id
                  FROM ressourcetype
                  WHERE name = price.type);

update ressources set type='goods' where ressourceid !='';
alter table price drop column type;

