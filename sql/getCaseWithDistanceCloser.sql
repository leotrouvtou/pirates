select
map.lat as lat,
map.long as long,
boat.id as boatid,
harbour.id as harbourid,
land,
map.lat||'x'||map.long as coord,
max(abs(map.lat-:hlat), abs(map.long-:hlong)) as dist,
max(abs(:blat-:hlat), abs(:blong-:hlong)) as actualdist
from map
left outer join boat on map.lat = boat.lat and map.long=boat.long and map.mapid=boat.mapid
left outer join harbour on map.lat = harbour.lat and map.long=harbour.long and map.mapid=harbour.mapid
where map.lat>=:blat-1
and  map.long>=:blong-1
and  map.lat<=:blat+1
and  map.long<=:blong+1
and  coord!=:blat||'x'||:blong
and boatid is NULL
and harbourid is NULL
and dist<actualdist
and (land like '%sea%' or land like '%creek%' or land like '%corail%')
and map.mapid=1
order by dist asc;
