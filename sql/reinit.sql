delete from map where mapid=1;
update harbour set lat=16, long=85 where id=4;
update harbour set lat=11, long=49 where id=5;
update harbour set lat=55, long=12 where id=1;
update harbour set lat=83, long=7 where id=6;
update harbour set lat=88, long=51 where id=7;
update harbour set lat=23, long=24 where id=2;

update harbour set lat=51, long=87 where id=8;
update harbour set lat=42, long=46 where id=9;

update harbour set lat=80, long=80, mapid=2;
update harbour set lat=23, long=24, mapid=1 where id=2;
update harbour set lat=10, long=10 where id=3;



update boat set lat=10, long=10, mapid=2, inharbour=1 where side="pirate";
update boat set lat=23, long=24,  mapid=1, inharbour=1 where side="corsaire";

update boat set inharbour=0, pnj=1, side="pirate", mapid=2, inharbour=0 where lastturn < date('now','start of month','-5 month');

update boat set side="corsaire",lat=23, long=24,  mapid=1, inharbour=1  where lastturn > date('now','start of month','-5 month') and side='pirate';

delete from persomap;
delete from boatquestlink;
# delete from victorypoint;
delete from quest;
update lastseen set Timestamp='2022-05-22 20:22:15';

update boat set boatstructure=boatstructuremax, canon=canonmax, actionpoint=actionpointmax, mouvementpoint=mouvementpointmax;
