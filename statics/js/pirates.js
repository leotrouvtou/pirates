function sailTo(direction){
    $.ajax({
      type: "POST",
      url: "/sailTo/"+direction,
      dataType: "json",
      traditional: true,
      success: function(data){
        console.log(data)
        loc = '/map'
        window.location = loc
      }
    });
}

function reloadTurn(boatid){
    $.ajax({
      type: "POST",
      url: "/reloadTurn/"+boatid,
      dataType: "json",
      traditional: true,
      success: function(data){
        console.log(data)
          loc = '/map'
          window.location = loc
      }
    });
}

function recruit(member){
    $.ajax({
      type: "POST",
      url: "/recruit/"+member,
      dataType: "json",
      traditional: true,
      success: function(data){
        console.log(data)
          loc = '/tavern'
          window.location = loc
      }
    });
}

function getObject(objectid){
    $.ajax({
      type: "POST",
      url: "/getObject/"+objectid,
      dataType: "json",
      traditional: true,
      success: function(data){
        console.log(data)
        if (data["success"]===false){
          $("#message").html(data["message"]);
          // show
          $("#message").show();
          // hide after 5 seconds
          var t = setTimeout("$(\"#message\").hide() ;",5000);
        } else {
          loc = '/map'
          window.location = loc
        }
      }
    });
}
function repair(){
    $.ajax({
      type: "POST",
      url: "/repair",
      dataType: "json",
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function sendMessage(){
  var subject   = document.getElementById("subject").value;
  var toids = document.getElementById("toids").value;
  var content   = document.getElementById("summernote").value;
  var toidside = document.getElementById("side").value;
  if (content!='' && (toidside!='' || toids!='') && subject!='') {
    if (toidside==''){
    var toids = [];
    for (var option of document.getElementById('toids').options)
    {
      if (option.selected) {
        toids.push(option.value);
      }
    }}
    else {
      toids=toidside
    }
    $.ajax({
      type: "POST",
      url: "/sendMessage",
      dataType: "json",
      traditional: true,
      data: {content: content, toids: toids.toString(), subject:subject},
      success: function(data){
        var loc = '/messages'
        window.location = loc
      }
    });
  } else {
    alert("Merci de remplir tous les champs");
  }
}


function saveTextPage(page){
  var content   = document.getElementById("summernote").value;
    $.ajax({
      type: "POST",
      url: "/saveTextPage/"+page,
      dataType: "json",
      traditional: true,
      data: {content: content},
      success: function(data){
        var loc = window.location.href
        window.location = loc.split('?')[0];
      }
    });
}
function takeQuest(questid){
    $.ajax({
      type: "POST",
      url: "/takeQuest/"+questid,
      dataType: "json",
      traditional: true,
      success: function(data){
        loc = '/quests'
        window.location = loc
      }
    });
}


function validateQuest(questid){
    $.ajax({
      type: "POST",
      url: "/validateQuest/"+questid,
      dataType: "json",
      traditional: true,
      success: function(data){
        loc = '/quests'
        window.location = loc
      }
    });
}

function sellRessources(ressourceid){
   amount = prompt('Combien on vend?');
    $.ajax({
      type: "POST",
      url: "/sellRessources/"+ressourceid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        loc = '/dock'
        window.location = loc
      }
    });
}

function searchTreasure(){
    $.ajax({
      type: "POST",
      url: "/searchTreasure",
      dataType: "json",
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function giveMoney(targetid){
   amount = prompt('Combien on donne?');
    $.ajax({
      type: "POST",
      url: "/giveMoney/"+targetid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        window.location.reload(true)
      }
    });
}
function buyRessources(ressourceid){
   amount = prompt('Combien on prend?');
    $.ajax({
      type: "POST",
      url: "/buyRessources/"+ressourceid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        loc = '/dock'
        window.location = loc
      }
    });
}

function putRessourceIntoSea(ressourceid){
   amount = prompt('Combien on jette?');
    $.ajax({
      type: "POST",
      url: "/putRessourceIntoSea/"+ressourceid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        loc = '/hold'
        window.location = loc
      }
    });
}
function putRessourcesIntoTreasure(){
  selects = document.querySelectorAll("select")
  ressources=[]
  for (var i = 0; i < selects.length; i++) {
    element=selects[i]
    if (element.value!=0){
      ressource = {'id': element.id, 'number': element.value}
      ressources.push(ressource)
    }
  }
    $.ajax({
      type: "POST",
      url: "/putRessourcesIntoTreasure",
      dataType: "json",
      traditional: true,
      data: {ressources: JSON.stringify(ressources)},
      success: function(data){
        loc = '/hold'
        window.location = loc
      }
    });
}

function addCanon(ressourceid){
    $.ajax({
      type: "POST",
      url: "/addCanon/"+ressourceid,
      dataType: "json",
      traditional: true,
      success: function(data){
        loc = '/hold'
        window.location = loc
      }
    });
}

function changeBoat(boatType){
    $.ajax({
      type: "POST",
      url: "/changeBoat/"+boatType,
      dataType: "json",
      traditional: true,
      success: function(data){
        loc = '/map'
        window.location = loc
      }
    });
}
function farmRessources(coord, ressourceName){
    $.ajax({
      type: "POST",
      url: "/farmRessources/"+coord,
      dataType: "json",
      data: {ressourceName:ressourceName },
      traditional: true,
      success: function(data){
        loc = '/map'
        window.location = loc
      }
    });
}

function deleteRessource(ressourceid){
    $.ajax({
      type: "POST",
      url: "/anim/modifyRessource/"+ressourceid,
      dataType: "json",
      data: {amount:0 },
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function addRessourceType(){
  var denom   = document.getElementById("name").value;
  var sellprice   = document.getElementById("sellprice").value;
  var buyprice   = document.getElementById("buyprice").value;
    $.ajax({
      type: "POST",
      url: "/anim/addRessourceType",
      dataType: "json",
      data: {denom: denom, sellprice:sellprice, buyprice:buyprice},
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}


function addVictoryPoints(){
  var points   = document.getElementById("points").value;
  var description   = document.getElementById("description").value;
  var side   = document.getElementById("side").value;
    $.ajax({
      type: "POST",
      url: "/anim/addVictoryPoints",
      dataType: "json",
      data: {points: points, description:description, side:side},
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function addRessource(harbourid){
  var amount   = document.getElementById("ressourceamount").value;
  var side   = document.getElementById("ressourceside").value;
  var ressourcetype   = document.getElementById("ressourcetype").value;
    $.ajax({
      type: "POST",
      url: "/anim/addRessource/"+harbourid,
      dataType: "json",
      data: {amount: amount, side:side, ressourcetype:ressourcetype},
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function changeFlag(ressourceid){
    $.ajax({
      type: "POST",
      url: "/changeFlag/"+ressourceid,
      dataType: "json",
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function removeFlag(){
    $.ajax({
      type: "POST",
      url: "/removeFlag",
      dataType: "json",
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function inspectWithGoogle(coord){
    $.ajax({
      type: "POST",
      url: "/inspectWithGoogle/"+coord,
      dataType: "json",
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function modifyRessource(ressourceid){
  amount = prompt('Combien on met?');
    $.ajax({
      type: "POST",
      url: "/anim/modifyRessource/"+ressourceid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        window.location.reload(true)
      }
    });
}
function makeRound(){
  angle = prompt('Dans queqlle direction? \n no n ne \n o □ e \n so s se');
    $.ajax({
      type: "POST",
      url: "/makeRound",
      dataType: "json",
      traditional: true,
      data: {angle: angle},
      success: function(data){
        window.location.reload(true)
      }
    });
}
function drinksOnMe(){
  amount = prompt('Combien on met?');
    $.ajax({
      type: "POST",
      url: "/drinksOnMe",
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        window.location.reload(true)
      }
    });
}
function modifyRessourceName(ressourceid){
  name = prompt('Quel nom?');
    $.ajax({
      type: "POST",
      url: "/anim/modifyRessourceName/"+ressourceid,
      dataType: "json",
      traditional: true,
      data: {name: name},
      success: function(data){
        window.location.reload(true)
      }
    });
}


function modifyPrice(id, harbourid, sellorbuy){
  price = prompt('Combien on met?');
    $.ajax({
      type: "POST",
      url: "/anim/modifyPrice/"+harbourid,
      dataType: "json",
      traditional: true,
      data: {price: price, ressourceid: id, sellorbuy:sellorbuy},
      success: function(data){
        window.location.reload(true)
      }
    });
}

function deletePop(popid){
    $.ajax({
      type: "POST",
      url: "/anim/modifyPop/"+popid,
      dataType: "json",
      data: {amount: 0},
      traditional: true,
      success: function(data){
        window.location.reload(true)
      }
    });
}

function modifyPop(popid){
  amount = prompt('Combien on met?');
    $.ajax({
      type: "POST",
      url: "/anim/modifyPop/"+popid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        window.location.reload(true)
      }
    });
}
function addPop(harbourid){
  var amount   = document.getElementById("popamount").value;
  var side   = document.getElementById("popside").value;
    $.ajax({
      type: "POST",
      url: "/anim/addPop/"+harbourid,
      dataType: "json",
      traditional: true,
      data: {amount: amount, side:side},
      success: function(data){
        window.location.reload(true)
      }
    });
}
function loadPeople(popid){
   amount = prompt('Combien on embarque?');
    $.ajax({
      type: "POST",
      url: "/loadPeople/"+popid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        loc = '/dock'
        window.location = loc
      }
    });
}
function unLoadPeople(popid){
   amount = prompt('Combien on débarque?');
    $.ajax({
      type: "POST",
      url: "/unLoadPeople/"+popid,
      dataType: "json",
      traditional: true,
      data: {amount: amount},
      success: function(data){
        loc = '/dock'
        window.location = loc
      }
    });
}

function enterBuilding(buildingid, buildingtype){
    $.ajax({
      type: "POST",
      url: "/enterBuilding/"+buildingid,
      dataType: "json",
      traditional: true,
      data: {buildingtype: buildingtype},
      success: function(data){
        console.log(data)
        loc = '/map'
        window.location = loc
      }
    });
}


function shootByCanon(){
  target = document.getElementById('target').value
  $.ajax({
    type: "POST",
    url: "/shootByCanon/"+target,
    dataType: "json",
    traditional: true,
    success: function(data){
      console.log(data)
      loc = '/map'
      window.location = loc
    }
  });
}

function shootAgainByCanon(id){
  document.getElementById('target').value = id
  shootByCanon()
}

function addLand(lat, long, image, mapid){
  if (image=='harbour'){
    addHarbour(lat, long, mapid)
  } else if (image=='portal'){
    addPortal(lat, long, mapid)
  } else {
    $.ajax({
      type: "POST",
      url: "/addLand",
      dataType: "json",
      traditional: true,
      data: {image: image,lat: lat, long: long, mapid: mapid},
      success: function(data){
        console.log(data)
      }
    });
  }
}

function saveBuilding(buildingtype){
  var name   = document.getElementById("name").value;
  var side   = document.getElementById("side").value;
  var lat   = document.getElementById("lat").value;
  var long   = document.getElementById("long").value;
  var mapid   = document.getElementById("mapid").value;
  if (buildingtype=="harbour"){
    var capital   = document.getElementById("capital").checked;
    var description   = document.getElementById("description").value;
    var maxpop   = document.getElementById("maxpop").value;
    var neutral   = document.getElementById("neutral").value;
    var pirates   = document.getElementById("pirates").value;
    var corsaires   = document.getElementById("corsaires").value;
    datatosend = {name:name, side:side, lat: lat, long:long, mapid:mapid, capital:capital, maxpop:maxpop, neutral:neutral, pirates:pirates,corsaires:corsaires}
  } else {
    var toharbour   = document.getElementById("toharbour").value;
    datatosend = {name:name, side:side, lat: lat, long:long, mapid:mapid, toharbour:toharbour}
  }
  $.ajax({
    type: "POST",
    url: "/createBuilding/"+buildingtype,
    dataType: "json",
    traditional: true,
    data: datatosend,
    success: function(data){
      console.log(data)
      window.location.reload(true)
    }
  });
}


function addHarbour(lat, long, mapid){
  document.getElementById("modal").innerHTML ="<button class=closemodal onclick=closeModal() role=button>X</button><h2>Ajout d'un Port</h2>Vous êtes dans la page de création d'un port en "+lat+"x"+long+".<br/>Veuillez remplir les informations suivantes : <br/>Nom : <input class='input is-large' type='nom' id='name' placeholder='Nom' autofocus=''> <br/>Nom : <input class='input is-large' type='description' id='description' placeholder='Description' autofocus=''> <br/> Camp :<select id='side' class='input' name='side' ><option value=''>--Choisir le camp--</option><option value='neutral'>Neutral</option><option value='pirate'>Pirate</option><option value='corsaire'>Corsaire</option></select>  <br/>Capitale : <input class='input is-large' type='checkbox' id='capital' placeholder='Pop Max' autofocus=''><br/> Maximum de pop : <input class='input is-large'id='maxpop' placeholder='Pop Max' autofocus=''><br/>Nombre de neutres : <input class='input is-large' id='neutral'  placeholder='Nombre de neutre' autofocus=''><br/>Nombre de pirates : <input class='input is-large' id='pirates' name='name' placeholder='Nombre de pirates' autofocus=''><br/>Nombre de corsaires : <input class='input is-large' id='corsaires' name='name' placeholder='Nombre de corsaires' autofocus=''><input id='lat' style='display:none;' value='"+lat+"'/><input id='long' style='display:none;' value='"+long+"'/><input id='mapid' type='text' style='display:none;' value='"+mapid+"'/><p buttonmodal class=buttonmodal><button class='btn btn-primary' onclick=saveBuilding('harbour')>Sauver</button><button class='right btn btn-outline-secondary' onclick=closeModal()>Annuler</button>"
  openModal()
};
function addPortal(lat, long, mapid){
  document.getElementById("modal").innerHTML ="<button class=closemodal onclick=closeModal() role=button>X</button><h2>Ajout d'un Portail</h2><p>Vous êtes dans la page de création d'un Portail en "+lat+"x"+long+".</p>Veuillez remplir les informations suivantes : </p>Nom : <input class='input is-large' type='nom' id='name' placeholder='Nom' autofocus=''> <br/>Camp :<select id='side' class='input' name='side' ><option value=''>--Choisir le camp--</option><option value='neutral'>Neutral</option><option value='pirate'>Pirate</option><option value='corsaire'>Corsaire</option></select>  <br/>Port de destination : <input class='input 'id='toharbour' placeholder='Id du port' autofocus=''><input id='lat' style='display:none;' value='"+lat+"'/><input id='long' style='display:none;' value='"+long+"'/><input id='mapid' type='text' style='display:none;' value='"+mapid+"'/><p buttonmodal class=buttonmodal><button class='btn btn-primary' onclick=saveBuilding('portal')>Sauver</button><button class='right btn btn-outline-secondary' onclick=closeModal()>Annuler</button>"
  openModal()
};

function openModal(){
  $(".maskmodal").addClass("active");
};

function closeModal(){
  $(".maskmodal").removeClass("active");
}
