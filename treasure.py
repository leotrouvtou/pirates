from . import DBAccess as DB
from . import events
from . import maputils
from . import boat as boatutils
from . import events
from . import sail
from. import building
from flask import flash

def giveMoney(targetid, boatid, amount):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    target = DB.request_db('sql/getBoat.sql', (targetid,))[0]
    if maputils.getDistance(boat['lat'], boat['long'], target['lat'], target['long'])<2 and boat['mapid']==target['mapid']:
        if boat['actionpoint']>=2:
            if boat['thune']>=int(amount):
                DB.write_db('sql/decrementThune.sql', {'boatid':boatid, 'price':amount})
                DB.write_db('sql/incrementThune.sql', {'boatid':targetid, 'price':amount})
                DB.write_db('sql/decrementActionPoint.sql', (2, boatid))
                events.addEventBoatGiveMoney(boat, target, amount)
                flash("Vous avez donné "+amount+" pièces de huit à "+target['name'])
            else:
                flash("Vous n'avez pas assez de pièces de huit")
                return ''
        else:
            flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")
    else:
        flash("Ce bateau est trop loin")
        return ''


def buriTreasure(boatid, ressources):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    case = DB.request_db('sql/getCase.sql', (boat['lat'],boat['long'], boat['mapid']))[0]
    if sail.is_sea(case):
        if not case['harbourid']:
            if boat['actionpoint']>=5:
                content = ""
                for ressource in ressources:
                    if ressource['id']=='thune':
                        if ressource['number']>boat['thune']:
                            flash("Vous voulez enterrer plus que vous avez de pièce de huit")
                            return 0
                        else:
                            content = content +str(ressource['number'])+" Pièces de huit "
                    else:
                        boatressource = DB.request_db('sql/getRessource.sql', (ressource['id'],))[0]
                        if boat['stockid']==boatressource['stockid']:
                            if int(ressource['number'])>boatressource['number']:
                                flash("Vous voulez enterrer plus qu'il n'y en a")
                                return 0
                stock=boatutils.createStock()
                for ressource in ressources:
                    boatressource = DB.request_db('sql/getRessource.sql', (ressource['id'],))[0]
                    DB.write_db('sql/decrementRessource.sql', (ressource['number'], ressource['id']))
                    if boatressource['number']==int(ressource['number']):
                        DB.write_db('sql/deleteRessource.sql', (boatressource['id'],))
                    building.getOrCreateRessource(ressource['number'], boatressource['ressourceid'], boatressource['side'], stock['id'], boatressource['sourceid'])
                DB.write_db('sql/decrementActionPoint.sql', (5, boatid))
                DB.write_db('sql/createTreasure.sql', (boat['lat'], boat['long'], "Trésor de "+boat['boatname'], boat['mapid'], stock['id'],boat['id']))

                content = boatutils.getRessourcesList(stock['id'])
                flash("Vous avez caché un trésor ("+content+")")
            else:
                flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")
        else:
            flash("Un trésor ne peut pas être dans un port")
    else:
        flash("Un trésor doit être dans l'eau")


def createTreasure(lat, long, name, ressources, boatid):
    stock=boatutils.createStock()
    for ressource in ressources:
        building.getOrCreateRessource(ressource['number'], ressource['ressourceid'], "neutral", stock['id'], '')
    DB.write_db('sql/createTreasure.sql', (lat, long, name, 1, stock['id'], boatid))

def isTreasure(lat,long,mapid):
    return DB.request_db('sql/getTreasure.sql', (lat, long, mapid))

def searchTreasure(boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    if boat['actionpoint']>=5:
        treasure = isTreasure(boat['lat'], boat['long'], boat['mapid'])
        if treasure!=[]:
            content = boatutils.assignObjectContent(boat['stockid'], treasure[0]['stockid'])
            events.addEventBoatGetTreasure(boat, treasure[0]['name'], content)
            flash('Vous venez de trouver le trésor "'+treasure[0]['name']+'" avec '+content)
            DB.write_db('sql/deleteTreasure.sql', ( treasure[0]['id'],))
            DB.write_db('sql/decrementActionPoint.sql', (5, boatid))
        else:
            flash("Il n'y a pas de trésor ici")
            DB.write_db('sql/decrementActionPoint.sql', (5, boatid))
    else:
        flash("Vous n'avez pas assez de point d'action("+str(boat['actionpoint'])+")")
    return ''


def createGift():
    boats = DB.request_db('sql/getAllBoatsAllMaps.sql')
    for boat in boats:
        land = DB.request_db('sql/getRandomSea.sql')[0]
        while DB.request_db('sql/getTreasure.sql', (land['lat'], land['long'], land['mapid']))!=[]:
            land = DB.request_db('sql/getRandomSea.sql')[0]
        ressources = [{"number":200, "ressourceid":8}, {"number":50, "ressourceid":6}]
        createTreasure(land['lat'], land['long'], "Trésor de noël "+boat['boatname'], ressources, boat['id'])
