from flask import session
from . import DBAccess as DB

def addEvent(eventtype, referenceid, isPersonal, comment):
    DB.write_db('sql/addEvent.sql', (eventtype, referenceid, isPersonal, comment))

def getEvents(referenceid):
    result={}
    if referenceid=='':
        referenceid=session['boatid']
        result['element'] = DB.request_db('sql/getBoat.sql', (referenceid,))[0]
        personal=True
        eventtype='boat'
    else:
        if 'b' in referenceid:
            referenceid=referenceid.replace("b", "")
            result['element'] = DB.request_db('sql/getBoat.sql', (referenceid,))[0]
            personal=False
            eventtype='boat'
        else:
            if 'h' in referenceid:
                referenceid=referenceid.replace("h", "")
                harbour = DB.request_db('sql/getHarbourById.sql', (referenceid,))
                boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
                if len(harbour)>0 and harbour[0]['side']==boat['side']:
                    personal=True
                    result['pops']=DB.request_db('sql/getPops.sql', (referenceid,))
                    result['totalpop'] = DB.request_db('sql/getTotalPops.sql', (referenceid,))[0]['totalpop']
                else:
                    personal=False
                eventtype='harbour'
                result['element'] = harbour[0]

    result['events'] = DB.request_db('sql/getEvents.sql', (eventtype, referenceid, personal))
    result['eventtype'] = eventtype
    result['personal'] = personal
    return result

def addBoatEvent( referenceid, isPersonal, comment):
    addEvent('boat', referenceid, isPersonal, comment)

def addHarbourEvent( referenceid, isPersonal, comment):
    addEvent('harbour', referenceid, isPersonal, comment)

def boatNameAndLink(boat):
    return "<a class='{}link' href='/events/b{}'>{} ({})</a>".format(boat['side'],boat['id'], boat['boatname'], boat['side']) 

def harbourNameAndLink(harbour):
    return "<a class='{}link' href='/events/h{}'>{} ({})</a>".format(harbour['side'],harbour['id'], harbour['name'], harbour['side']) 
def addEventShootB2B(boat, target, damage, result, xp):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a attaqué "+boatNameAndLink(target)+" ("+str(result['value'])+"/"+str(result['precision'])+") et a fait "+str(damage)+" de dégâts. Gain renommée: "+str(xp))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a attaqué "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été attaqué par "+boatNameAndLink(boat)+" qui a fait "+str(damage)+" de dégâts")
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été attaqué par "+boatNameAndLink(boat))


def addEventShootMissB2B(boat, target, result, xp):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a manqué "+boatNameAndLink(target)+" "+str(result['value'])+"/"+str(result['precision'])+". Gain renommée: "+str(xp))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a manqué "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été manqué par "+boatNameAndLink(boat))
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été manqué par "+boatNameAndLink(boat))

def addEventShootMissCritB2B(boat, target, result, xp):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a manqué "+boatNameAndLink(target)+" "+str(result['value'])+"/"+str(result['precision'])+". Un canon a explosé. Gain renommée: "+str(xp))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a manqué "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été manqué par "+boatNameAndLink(boat))
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été manqué par "+boatNameAndLink(boat))

def addEventSinkB2B(boat, target):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a coulé "+boatNameAndLink(target))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a coulé "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été coulé par "+boatNameAndLink(boat))
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été coulé par "+boatNameAndLink(boat))

def addEventBoatMove(boat, lat, long):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" s'est déplacé en "+str(lat)+"x"+str(long))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" s'est déplacé.")

def addEventBoatMakeRound(boat, angle):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a tourné sur place "+str(angle)+" deg.")
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a tourné sur place.")

def addEventBoatNewTurn(boat, maluses):
    addBoatEvent(boat['id'], True, "Vous avez déclanché un nouveau tour."+maluses)

def addEventBoatFarmRessources(boat, amount, ressourceName):
    addBoatEvent(boat['id'], True, "Vous avez remplis "+str(amount)+" tonneaux de "+ressourceName)

def addEventBoatGetFloatingObject(boat, objectname, content):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a ramassés "+objectname+" ("+content+")")
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a ramassés des débris.")

def addEventBoatGetTreasure(boat, objectname, content):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a ramassés un trésor: "+objectname+" ("+content+")")
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a ramassés un trésor.")

def addEventBoatLooseRessources(boat, content):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a perdu "+content)

def addEventBoatEnterHarbour(boat, harbour):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" est entré dans le port "+harbourNameAndLink(harbour))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" est entré dans un port")

def addEventBoatTP(boat, lat, long, mapid):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" s'est téléporté en "+str(lat)+"x"+str(long)+" sur la carte "+str(mapid))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" s'est téléporté.")

def addEventRepair(boat, torepair, newPS):
    addBoatEvent(boat['id'], True, "Vous avez réparé le bateau de "+str(torepair)+" points de structure ("+str(newPS)+"/"+str(boat['boatstructuremax'])+")")
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a été réparé.")

def addEventNewTurnHarbour(harbour, event):
    addHarbourEvent(harbour['id'], True, "Nouveau tour : les ressources et la population ont augmenté. "+event)
    addHarbourEvent(harbour['id'], False, "Nouveau tour : les ressources et la population ont augmenté.")
    
def addEventNewTurnHarbourNoPop(harbour, event):
    addHarbourEvent(harbour['id'], True, "Nouveau tour : les ressources ont baissé. "+event)
    addHarbourEvent(harbour['id'], False, "Nouveau tour : les ressources ont baissé.")

def addEventNewTurnHarbourNotActive(harbour, event):
    addHarbourEvent(harbour['id'], True, "Nouveau tour : la population a augmenté. "+event)
    addHarbourEvent(harbour['id'], False, "Nouveau tour : la population a augmenté.")

def addEventNewTurnHarbourSiegeMode(harbour):
    addHarbourEvent(harbour['id'], True, "Nouveau tour : le port est assiégé, toutes les ressources sont utilisées pour la defense.")
    addHarbourEvent(harbour['id'], False, "Nouveau tour : le port est assiégé, toutes les ressources sont utilisées pour la defense.")


def addEventBoatGiveMoney(boat, target, amount):
    addBoatEvent(boat['id'], True, "Vous avez donné "+str(amount)+" pièces de huit à "+target['name'])
    addBoatEvent(target['id'], True, boat['name']+" vous a donné "+str(amount)+" pièces de huit")

def addEventHarbourAddPop(harbour, event):
    addHarbourEvent(harbour['id'], True, "Rançon payée: la population ont augmenté "+event)
    addHarbourEvent(harbour['id'], False, "Rançon payée: la population ont augmenté")

def addEventBoatLoadRessource(harbour, boat, amount, type):
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a embarqué des marchandises")
    addBoatEvent(boat['id'], True, "Vous avez embarqué "+str(amount)+"  caisses de "+type)
    addHarbourEvent(harbour['id'], False, "Un Navire a embarqué des marchandises")
    addHarbourEvent(harbour['id'], True, boatNameAndLink(boat)+" a embarqué "+str(amount)+" caisses de  "+type)

def addEventBoatUnLoadRessource(harbour, boat, amount, type):
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a débarqué des marchandises")
    addBoatEvent(boat['id'], True, "Vous avez débarqué "+str(amount)+"  caisses de "+type)
    addHarbourEvent(harbour['id'], False, "Un Navire a débarqué des marchandises")
    addHarbourEvent(harbour['id'], True, boatNameAndLink(boat)+" a débarqué "+str(amount)+" caisses de  "+type)

def addEventBoatdrinksOnMe(harbour, boat, amount):
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a payé une tournée")
    addBoatEvent(boat['id'], True, "Vous avez payé une tournée de "+str(amount)+" verres")
    addHarbourEvent(harbour['id'], False, "Un Navire a payé une tournée")
    addHarbourEvent(harbour['id'], True, boatNameAndLink(boat)+" a payé une tournée de "+str(amount)+" verres")

def addEventPopChangeSide(harbour, amount, side):
    addHarbourEvent(harbour['id'], False, "Des colons ont changés d'allégeance")
    if amount>1:
        addHarbourEvent(harbour['id'], True, str(amount)+" colons ont changés d'allégeance ("+side+")")
    else:
        addHarbourEvent(harbour['id'], True, str(amount)+" colon a changé d'allégeance ("+side+")")

def addEventBoatPutIntoSeaRessource(boat, amount, type):
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a jeté des marchandises à la mer")
    addBoatEvent(boat['id'], True, "Vous avez jeté à la mer "+str(amount)+"  tonneaux de "+type)

def addEventBoatLoadPeople(harbour, boat, amount, side):
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a embarqué des colons")
    addBoatEvent(boat['id'], True, "Vous avez embarqué "+str(amount)+"  "+side+"s")
    addHarbourEvent(harbour['id'], False, "Un Navire a embarqué des colons")
    addHarbourEvent(harbour['id'], True, boatNameAndLink(boat)+" a embarqué "+str(amount)+"  "+side+"s")

def addEventBoatUnLoadPeople(harbour, boat, amount, side):
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a débarqué des colons")
    addBoatEvent(boat['id'], True, "Vous avez débarqué "+str(amount)+"  "+side+"s")
    addHarbourEvent(harbour['id'], False, "Un Navire a débarqué des colons")
    addHarbourEvent(harbour['id'], True, boatNameAndLink(boat)+" a débarqué "+str(amount)+"  "+side+"s")

def addEventHarbourChangeSide(harbour, side):
    addHarbourEvent(harbour['id'], False, "Le port "+harbourNameAndLink(harbour)+" est maintenant géré par les "+side+"s")
    addHarbourEvent(harbour['id'], True, "Le port "+harbourNameAndLink(harbour)+" est maintenant géré par les "+side+"s")


def addEventShootB2H(boat, target, damage, result, event, xp):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a attaqué le port "+target['name']+" ("+str(result['value'])+"/"+str(result['precision'])+") et a fait "+str(damage)+" de dégâts : "+event+" gain renommée: "+str(xp))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a attaqué le port "+target['name'])
    addHarbourEvent(target['id'], True, "Le port "+target['name']+" a été attaqué par le "+boatNameAndLink(boat)+" qui a fait "+str(damage)+" de dégâts : "+event)
    addHarbourEvent(target['id'], False, "Le port "+target['name']+" a été attaqué par le "+boatNameAndLink(boat))


def addEventShootMissB2H(boat, target, result, xp):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a manqué le port "+target['name']+" ("+str(result['value'])+"/"+str(result['precision'])+") gain renommée: "+str(xp))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a manqué le port "+target['name'])
    addHarbourEvent(target['id'], True, "Le port "+target['name']+" a été manqué par le "+boatNameAndLink(boat))
    addHarbourEvent(target['id'], False, "Le port "+target['name']+" a été manqué par le "+boatNameAndLink(boat))

def addEventShootMissCritB2H(boat, target, result, xp):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a manqué le port "+target['name']+" ("+str(result['value'])+"/"+str(result['precision'])+"). Un canon a explosé. gain renommée: "+str(xp))
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a manqué le port "+target['name'])
    addHarbourEvent(target['id'], True, "Le port "+target['name']+" a été manqué par le "+boatNameAndLink(boat))
    addHarbourEvent(target['id'], False, "Le port "+target['name']+" a été manqué par le "+boatNameAndLink(boat))
    
def addEventSurrenderHarbour(boat, target):
    addBoatEvent(boat['id'], True, boatNameAndLink(boat)+" a soumis le port "+target['name'])
    addBoatEvent(boat['id'], False, boatNameAndLink(boat)+" a soumis le port "+target['name'])
    addHarbourEvent(target['id'], True, "Le port "+target['name']+" a été soumis par "+boatNameAndLink(boat))
    addHarbourEvent(target['id'], False, "Le port "+target['name']+" a été soumis par "+boatNameAndLink(boat))


def addEventShootH2B(harbour, target, result, damage):
    addHarbourEvent(harbour['id'], True, "Le port "+harbourNameAndLink(harbour)+" a attaqué "+boatNameAndLink(target)+" et a fait "+str(damage)+" de dégâts ("+str(result['value'])+"/"+str(result['precision'])+")")
    addHarbourEvent(harbour['id'], False, "Le port "+harbourNameAndLink(harbour)+" a attaqué "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été attaqué par le port "+harbourNameAndLink(harbour)+" qui a fait "+str(damage)+" de dégâts ("+str(result['value'])+"/"+str(result['precision'])+")")
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été attaqué par "+harbourNameAndLink(harbour))


def addEventShootMissH2B(harbour, target, result):
    addHarbourEvent(harbour['id'], True, "Le port "+harbourNameAndLink(harbour)+" a manqué "+boatNameAndLink(target)+"("+str(result['value'])+"/"+str(result['precision'])+")")
    addHarbourEvent(harbour['id'], False, "Le port "+harbourNameAndLink(harbour)+" a manqué "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été manqué par le port "+harbourNameAndLink(harbour)+"("+str(result['value'])+"/"+str(result['precision'])+")")
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été manqué par "+harbourNameAndLink(harbour))

def addEventSinkH2B(harbour, target):
    addHarbourEvent(harbour['id'], True, "Le port "+harbourNameAndLink(harbour)+" a coulé "+boatNameAndLink(target))
    addHarbourEvent(harbour['id'], False, "Le port "+harbourNameAndLink(harbour)+" a coulé "+boatNameAndLink(target))
    addBoatEvent(target['id'], True, boatNameAndLink(target)+" a été coulé par "+harbourNameAndLink(harbour))
    addBoatEvent(target['id'], False, boatNameAndLink(target)+" a été coulé par "+harbourNameAndLink(harbour))

def addEventBoatTakeQuest(boat, quest):
    addBoatEvent(boat['id'], True, 'Vous avez pris la quête "'+quest['description']+'"')

def addEventBoatRemoveFlag(boat):
    addBoatEvent(boat['id'], True, 'Le faux pavillon a été enlevé par un coup de canon')

def addEventBoatValidateQuest(boat, quest):
    addBoatEvent(boat['id'], True, 'Vous avez rendu la quête "'+quest['description']+'"')
