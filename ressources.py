from . import map
from . import DD
from . import building
from . import DBAccess as DB
from . import boat as boatutils
from . import events
from flask import flash

def farmRessources(boatid,ressourceName, coord):
    boat=DB.request_db('sql/getBoat.sql', (boatid,))[0]
    boat = boatutils.getOrCreateBoatWithStockid(boat)
    latlong = coord.split('x')
    if map.getDistance(int(latlong[0]), int(latlong[1]), boat['lat'], boat['long'])!=0:
        flash("C'est trop loin")
        return False
    if 'creek' not in DB.request_db('sql/getCase.sql', (boat['lat'],boat['long'], boat['mapid']))[0]['land']:
        flash("Il faut être dans une crique")
        return False
    if  boat['actionpoint']<5 :
        flash("vous n'avez plus assez de point d'action ("+str(boat['actionpoint'])+")")
        return False
    ressourceid = DB.request_db('sql/getRessourceType.sql', (ressourceName, ))[0]['id']
    amount = 5+DD.dice(1,5)
    building.getOrCreateRessource(amount, ressourceid, 'neutral', boat['stockid'])
    DB.write_db('sql/decrementActionPoint.sql', (5, boat['id']))
    flash("Vous avez remplis "+str(amount)+" tonneaux de "+ressourceName)
    events.addEventBoatFarmRessources(boat, amount, ressourceName)
