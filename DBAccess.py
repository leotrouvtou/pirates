import sqlite3 as sql
from flask import current_app

def write_db(fd, params=None, variable=None):
    con = sql.connect(current_app.config['db'])
    con.text_factory = str
    con.row_factory = sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            if params:
                cur.execute(fd.read(), params)
            else:
                cur.execute(fd.read())
            con.commit()
        except Exception as e:
            con.rollback()
            raise
    con.close()

def write_db_with_transation(fd, params=None, variable=None):
    con = sql.connect(current_app.config['db'])
    con.text_factory = str
    con.row_factory = sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params, multi=True)
            con.commit()
        except Exception as e:
            con.rollback()
            raise
    con.close()

def request_db(fd, params=()):
    con = sql.connect(current_app.config['db'])
    con.text_factory = str
    con.row_factory = lambda c, r: dict([(col[0], r[idx]) for idx, col in enumerate(c.description)])
    #sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params)
            return cur.fetchall()
        except Exception as e:
            con.rollback()
            raise
    con.close()
