from flask import Flask, Blueprint
from flask_login import login_required
from flask import render_template, flash

from flask import request, make_response, session
import json
from . import DBAccess as DB
from .auth import requires_roles
from .main import getOrCreatePageText, getBoatIfConnected

xp = Blueprint('xp', __name__)

@xp.route('/recruit/<memberToRecruit>', methods=['POST'])
@login_required
def recruit(memberToRecruit):
    boat = getBoatIfConnected()
    members = getMembers(boat['id'])
    xp = nextLevelAP(memberToRecruit, members)
    if boat['inharbour']:
        if  boat['xpi']<xp:
            flash("Vous n'avez pas assez de renommée ( il faut "+str(xp)+").")
        else:
            recruitMember(memberToRecruit, boat)
            DB.write_db('sql/decrementXPI.sql', (xp, boat['id']))
    else:
        flash("Vous devez être dans un port")
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

def getMembers(boatid):
    members = DB.request_db('sql/getMembers.sql', (boatid,))
    if len(members)==0:
        DB.write_db('sql/createMembers.sql', (boatid,))
        members = DB.request_db('sql/getMembers.sql', (boatid,))
    return members[0]

def recruitMember(member, boat):
    DB.write_db('sql/recruitMember.sql', {'member':member, 'boatid':boat['id']})
    DB.write_db('sql/updateBoatMember.sql', {'member':member, 'boatid':boat['id']})

def nextLevelAP(memberToRecruit, members):
    memberlvl=members[memberToRecruit+'lvl']
    if memberlvl==0:
        return 100
    elif memberlvl==1:
        return 200
    else:
        return 400
