import random

def dice(number, face):
    result = 0
    for i in range(0, number):
        result = result + random.randint(0, face) +1
    return result

def didSucceed(score, scoremax):
    result={}
    result['value'] = random.randint(1, scoremax)
    result['succeed']= (result['value']<=score)
    result['crit']=(result['value']<=score/20 or result['value']>=(100 - score/20))
    return result
