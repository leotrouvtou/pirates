# Introduction

## Bienvenue sur le jeu :  [Pirates](https://fr.wikipedia.org/wiki/Piraterie) !

Vous allez prendre part à la guerre pour la maîtrise des caraïbes en tour par tour.  
Vous pouvez choisir votre camp  **les Pirates sanguinaires** ou **les valeureux Corsaires** : qui allez vous faire gagner?

Le jeux se joue dans des décors magnifiques et vous serez aux commandes d'un voilier que vous pourrez faire évoluer.

Quoi qu'il arrive, souvenez vous que "le code des pirates est plus un guide général qu'un véritable règlement"

# Installation

## Install git, python, ...

```console
$ sudo apt-get install python-pip python-virtualenv sqlite3 libsqlite3-dev
```

1. Create a virtualenv

2. Clone repo and Install requirements

```console
$ pip install -r requirements.txt
```

## create local_settings.cfg

```
nano local_settings.cfg 
```
with
```
# application secret
SECRET_KEY = 'secret'
# flask profiler login
LOGIN = 'login'
# flask profile pwd
PWD = 'pwd'
# Rollbar api key
ROLLBAR ='apikey'

```

## create nginx conf

```nginx
server {
        server_name  domain;
        client_max_body_size 10M;

location / {
          client_max_body_size 10M;
          proxy_redirect      off;
          proxy_set_header    Host                    $host;
          proxy_set_header    X-Real-IP               $remote_addr;
          proxy_set_header    X-Forwarded-For         $proxy_add_x_forwarded_for;
          proxy_set_header    X-Forwarded-Protocol    $scheme;
          proxy_pass    http://127.0.0.1:5014;
        }
location ^~ /cron {
        return 404;
    }
}
```


## Create the systemd service :

Create `/lib/systemd/system/pirates.service`

```ini
[Unit]
Description=Pirates
After=network.target
[Service]
WorkingDirectory=/home/user
ExecStart=path/to/pirates/venv/bin/gunicorn 'pirates:create_app()' --workers 3 --threads 3 --bind 127.0.0.1:5014 --chdir /path/to/pirates/folder --timeout 90
User=user
Restart=always
[Install]
WantedBy=multi-user.target
Alias=pirates.service
```


## Create the cron :

```cron
*/10 * * * * curl -X POST http://127.0.0.1:5014/cron/createMapPng
5 4 * * * curl -X POST http://0.0.0.0:5014/cron/reloadTurnHarbours
```


# Test everything is OK

in virtualenv do

```
coverage run -m pytest

```

# First run

You can use the tests.db to have a working instance



```
cp tests.db flaskr.db
export FLASK_APP=~/src/pirates
flask run
```


or create new map by creating a new DB :


```
sqlite flaskr.db<sql/init.sql
export FLASK_APP=~/src/pirates
flask run
curl -X POST http://127.0.0.1:5014/cron/importMapFromPng
curl -X POST http://127.0.0.1:5014/cron/importMapFromPng/2
```
And then creating you own account.
