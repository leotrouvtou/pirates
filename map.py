from . import DBAccess as DB

def calculateCoord(boatid):
    coord={}
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    lat = boat['lat']
    long = boat['long']
    coord['xmin'] = max(int(lat)-int(boat['perception']), 0)
    coord['xmax'] = min(int(lat)+int(boat['perception']), 200)
    coord['ymin'] = max(int(long)-int(boat['perception']), 0)
    coord['ymax'] = min(int(long)+int(boat['perception']), 200)
    coord['mapid']=boat['mapid']
    return coord


def getMapInfo(boatid):
    carte={}
    coord = calculateCoord(boatid)
    xmin = coord['xmin']
    xmax = coord['xmax']
    ymin = coord['ymin']
    ymax = coord['ymax']
    results = DB.request_db('sql/getMap.sql', (xmin, xmax, ymin, ymax, coord['mapid']))
    map=[[{'content':''} for j in range(0, 202)] for i in range(0,202)]
    for result in results:
         x=result["lat"]
         y=result["long"]
         if result["harbourid"]:
             map[x][y]['content']='<th onclick="enterHarbour('+str(result["harbourid"])+');" background="/statics/images/'+result['land']+'.jpg"></th>'
         else:
             if result["boatid"]:
                 map[x][y]['content']='<th background="/statics/images/'+result['land']+'.gif"><img src="/statics/images/boat.png" class="boat" style="rotate:'+result['lastorientation']+'deg;"></th>'
             else:
                 map[x][y]['content']='<th background="/statics/images/'+result['land']+'.gif"></th>'
    carte['carte']=createMap(xmin, xmax, ymin, ymax, map)
    carte['xmin']=xmin
    carte['xmax']=xmax
    carte['ymin']=ymin
    carte['ymax']=ymax
    return carte

def createMap(xmin, xmax, ymin, ymax, map):
    html='<tr><td background="statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    for x in range(xmin, xmax+1):
        html=html+'<th background="statics/images/background.jpg"><div class="xy">'+str(x)+'</div></th>'
    html=html+'<td  background="statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    for y in range(ymin, ymax+1):
        html=html+'</tr><tr align="center"><th background="statics/images/background.jpg">'+str(y)+'</th>'
        for x in range(xmin, xmax+1):
            html=html+map[x][y]['content']
        html=html+'<th  background="statics/images/background.jpg"><div class="xy">'+str(x)+'</div></th>'
        html=html+'</tr>'
    html=html+'<td  background="statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    for x in range(xmin, xmax+1):
        html=html+'<th background="statics/images/background.jpg">'+str(x)+'</th>'
    html=html+'<td  background="statics/images/background.jpg" align="center"><div class="xy">y \ x</div></td>'
    return html


def getDistance(lat1, long1, lat2, long2):
    return max(abs(lat2-lat1), abs(long2-long1))
