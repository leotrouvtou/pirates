from flask import Flask, Blueprint, session
from flask import redirect, url_for, flash
from flask_login import LoginManager
from flask_login import UserMixin, current_user

from . import DBAccess as DB
from datetime import datetime, timedelta
from flask_profiler import Profiler
import rollbar
import rollbar.contrib.flask
from . import boat
from flask import got_request_exception
import os

SECRET_KEY = 'developpement'

def create_app():
    profiler = Profiler()
    app = Flask(__name__)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)
    app.config.from_pyfile('local_settings.cfg')
    app.secret_key = app.config['SECRET_KEY']
    app.config['timehour']=22
    if os.environ.get('FLASK_DEBUG'):
        app.config['env']='developpement'
    else:
        app.config['env']='production'
    app.config["flask_profiler"] = {
        "enabled": True,
        "storage": {
        "engine": "sqlite"
    },
        "basicAuth":{
        "enabled": True,
        "username": app.config['LOGIN'],
        "password": app.config['PWD']
    },
        "ignore": [
	"^/statics/.*"
    ]
    }
    app.config['db']='flaskr.db'
    app.app_context()
    profiler.init_app(app)



    # blueprint for auth routes in our app
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    # blueprint for non-auth parts of app
    from .stats import stats as stats_blueprint
    app.register_blueprint(stats_blueprint)

    # blueprint for non-auth parts of app
    from .cron import cron as cron_blueprint
    app.register_blueprint(cron_blueprint)

    # blueprint for non-auth parts of app
    from .anim import anim as anim_blueprint
    app.register_blueprint(anim_blueprint)

    # blueprint for non-auth parts of app
    from .xp import xp as xp_blueprint
    app.register_blueprint(xp_blueprint)
    
    @app.before_first_request
    def init_rollbar():
        """init rollbar module"""
        rollbar.init(
            # access token for the demo app: https://rollbar.com/demo
            app.config['ROLLBAR'],
            # environment name
            app.config["env"],
            # server root directory, makes tracebacks prettier
            root=os.path.dirname(os.path.realpath(__file__)),
            # flask already sets up logging
            allow_logging_basic_config=False)

        # send exceptions from `app` to rollbar, using flask's signal system.
        got_request_exception.connect(rollbar.contrib.flask.report_exception, app)


    def createMapCache():
        with app.app_context():
            mapcases = DB.request_db('sql/getFullMap.sql')
            results={}
            for case in mapcases:
                results[str(case['lat'])+'x'+str(case['long'])]=case['land']
            app.mapcache= results

    createMapCache()

    class User(UserMixin):
        pass

    @login_manager.user_loader
    def load_user(user_id):
        results=DB.request_db('sql/getUserByEmail.sql', (user_id,))
        if len(results)<1:
            return redirect(url_for('auth.login'))
        else:
            user = results[0]
        flask_user = User()
        flask_user.id = user['email']
        session['role'] = user['role']
        session['boatid'] = user['boatid']
        session['mapsize'] = user['mapsize']
        session['hasmessage'] = user['hasmessage']
        difference=datetime.now() - datetime.strptime(user['lastturn'], '%Y-%m-%d %H:%M:%S')
        if (difference.days*24+difference.seconds/3600)>app.config['timehour']:
            boat.newTurn(session['boatid'])
        return flask_user
    return app
