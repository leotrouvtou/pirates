import os
from flask import Flask, Blueprint
from flask import render_template, send_from_directory
from flask import redirect, flash
from flask import request, make_response, session
from flask_login import login_required
import json

from . import DBAccess as DB
from .auth import requires_roles
from .main import getOrCreatePageText
from . import building
from . import maputils
from . import quests
from . import treasure
from . import boat as boatutils

anim = Blueprint('anim', __name__)

@anim.route('/reloadTurn/<boatid>', methods=['POST'])
@login_required
@requires_roles('admin')
def reloadTurn(boatid):
    boatutils.newTurn(session['boatid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@anim.route('/saveTextPage/<page>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def saveTextPage(page):
    content = request.form.get('content')
    if content !='':
        DB.write_db('sql/savePageText.sql', (content, page))
    else:
        flash("Vous tentez de sauver une page vide: ce doit etre une erreur")
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}





@anim.route('/anim/buildings')
@login_required
@requires_roles('admin', 'animateur')
def getBuildings():
    results=building.getBuildings()
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    pagetext= getOrCreatePageText('buildings', request)
    return render_template('buildings.html', boat=boat,harbours = results['harbours'], portals = results['portals'], pagetext=pagetext['content'], edit=pagetext['edit'])


@anim.route('/anim/captains')
@anim.route('/anim/captains/<orderBy>')
@login_required
@requires_roles('admin', 'animateur')
def captainsList(orderBy='pseudo'):
    captains=DB.request_db('sql/getAllUsers.sql', {'order':orderBy})
    pagetext= getOrCreatePageText('captains', request)
    return render_template('captains.html', orderBy=orderBy, captains=captains, pagetext=pagetext['content'], edit=pagetext['edit'])


@anim.route('/anim/victory')
@login_required
@requires_roles('admin', 'animateur')
def victoryList():
    victorys=DB.request_db('sql/getVictoryList.sql')
    victorySum=DB.request_db('sql/getVictorySum.sql')
    pagetext= getOrCreatePageText('victory', request)
    return render_template('victory.html', victorys=victorys,victorySum=victorySum, pagetext=pagetext['content'], edit=pagetext['edit'])

@anim.route('/anim/harbour/<id>')
@login_required
@requires_roles('admin', 'animateur')
def getHarbour(id):
    results=building.getHarbour(id)
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    pagetext= getOrCreatePageText('harbour', request)
    return render_template('harbour.html', boat= boat, harbourstock = results['harbourstock'], harbour=results['harbour'], harbourpop=results['harbourpop'], items=results['stockitems'], pagetext=pagetext['content'], edit=pagetext['edit'])


@anim.route('/anim/bourse')
@login_required
@requires_roles('admin', 'animateur')
def getBourse():
    results=building.getBourse()
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    pagetext= getOrCreatePageText('bourse', request)
    return render_template('bourse.html', boat= boat, items=results, pagetext=pagetext['content'], edit=pagetext['edit'])


@anim.route('/anim/modifyRessource/<ressourceid>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def modifyRessource(ressourceid):
    amount = request.form.get('amount')
    if amount!='':
        result = building.modifyRessource(ressourceid, int(amount))
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/modifyPop/<popid>', methods=['POST'])
@requires_roles('admin', 'animateur')
def modifyPop(popid):
    amount = request.form.get('amount')
    if amount!='':
        result = building.modifyPop(popid, int(amount))
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/addRessource/<harbourId>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def addRessource(harbourId):
    amount = request.form.get('amount')
    side = request.form.get('side')
    ressourceType = request.form.get('ressourcetype')
    if amount!='':
        result = building.addRessource(ressourceType, side, int(amount), harbourId)
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@anim.route('/anim/addVictoryPoints', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def addVictoryPoints():
    points = request.form.get('points')
    side = request.form.get('side')
    description = request.form.get('description')
    if points!='' and side!='' and description!='':
        DB.write_db('sql/incrementVictoryPoints.sql', (side, points, description))
    else:
        flash('Merci de remplir le formulaire')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/modifyPrice/<harbourId>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def modifyPrice(harbourId):
    price = request.form.get('price')
    sellorbuy = request.form.get('sellorbuy')
    ressourceid = request.form.get('ressourceid')
    if price!='':
        result = building.modifyPrice(ressourceid, sellorbuy, int(price), harbourId)
    else:
        flash('Merci de choisir un prix')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/modifyRessourceName/<ressourceid>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def modifyRessourceName(ressourceid):
    name = request.form.get('name')
    if name!='':
       DB.write_db('sql/updateRessourceName.sql', (name,  ressourceid))
    else:
        flash('Merci de choisir une denomination')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@anim.route('/anim/addRessourceType', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def addRessourceType():
    denom = request.form.get('denom')
    sellprice = request.form.get('sellprice')
    buyprice = request.form.get('buyprice')
    if denom!='':
        result = building.addRessourceType(denom, sellprice, buyprice)
    else:
        flash('Merci de choisir une dénomination')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/addPop/<harbourId>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def addPop(harbourId):
    amount = request.form.get('amount')
    side = request.form.get('side')
    if amount!='':
        result = building.addPop(side, int(amount), harbourId)
    else:
        flash('Merci de choisir un montant')
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}



@anim.route('/createBuilding/<type>', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def createBuilding(type):
    building.createBuilding(type, request)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@anim.route('/anim/manageCarte')
@anim.route('/anim/manageCarte/<coord>')
@login_required
@requires_roles('admin', 'animateur')
def manageCarte(coord=''):
    carte = maputils.getSimpleMapInfo(coord, request)
    boat = DB.request_db('sql/getBoat.sql', (session['boatid'],))[0]
    return render_template('managecarte.html', xmin = carte['xmin'], xmax=carte['xmax'], ymin=carte['ymin'], ymax=carte['ymax'], carte=carte['carte'], boat=boat)



@anim.route('/addLand', methods=['POST'])
@login_required
@requires_roles('admin', 'animateur')
def addLand():
    lat = request.form.get('lat')
    long = request.form.get('long')
    image = request.form.get('image')
    mapid = request.form.get('mapid')
    DB.write_db('sql/updateLand.sql', (image, lat, long, mapid))
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/addWoodAndFood')
@login_required
@requires_roles('admin')
def addWoodAndFood():
    boats = DB.request_db('sql/getAllBoatsStockID.sql')
    woodressourceid = DB.request_db('sql/getRessourceType.sql', ('Bois', ))
    foodressourceid = DB.request_db('sql/getRessourceType.sql', ('Nourriture/Eau', ))
    rhumressourceid = DB.request_db('sql/getRessourceType.sql', ('Rhum', ))
    canonressourceid = DB.request_db('sql/getRessourceType.sql', ('Canon', ))
    armoressourceid = DB.request_db('sql/getRessourceType.sql', ('Munition', ))
    for boat in boats:
        if (boat['stockid']=='' or boat['stockid'] is None):
            boatutils.createAndAssignStock(boat['id'])
            boat = DB.request_db('sql/getBoat.sql', (boat['id'],))[0]
        # building.getOrCreateRessource(10, foodressourceid[0]['id'], 'neutral', boat['stockid'])
        # building.getOrCreateRessource(10, woodressourceid[0]['id'], 'neutral', boat['stockid'])
        # building.getOrCreateRessource(1, canonressourceid[0]['id'], 'neutral', boat['stockid'])
        # building.getOrCreateRessource(1, rhumressourceid[0]['id'], 'neutral', boat['stockid'])
        #building.getOrCreateRessource(10, armoressourceid[0]['id'], 'neutral', boat['stockid'])
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


@anim.route('/anim/createDiscoveryQuests')
@login_required
@requires_roles('admin')
def createDiscoveryQuests():
    quests.createDiscoveryQuests()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@anim.route('/anim/createGift')
@login_required
@requires_roles('admin')
def createGift():
    treasure.createGift()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
