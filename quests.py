from . import DBAccess as DB
from . import maputils
from . import events
from flask import flash


def getQuests(boatid):
    results ={}
    results['boat'] = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    results['boatquests'] =  DB.request_db('sql/getBoatQuests.sql', (boatid,))
    results['boatquestsid'] = []
    if len(results['boatquests'])>0:
        for quest in results['boatquests']:
            results['boatquestsid'].append(quest['validation'])
    if results['boat']['inharbour']:
        results['harbour'] =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        results['harbourquests']  =  DB.request_db('sql/getHarbourQuests.sql', (results['harbour']['id'],))
    else:
        results['harbour'] = []
        results['harbourquests']  = []
    return results


def takeQuest(questid, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    quest = DB.request_db('sql/getQuest.sql', (questid,))[0]
    if boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        if harbour['id']==quest['harbourid']:
            boatquests =  DB.request_db('sql/getBoatQuests.sql', (boatid,))
            if not questAllreadyOwned(quest['id'], boatquests):
                DB.write_db('sql/addQuestToBoat.sql', (questid, boatid ))
                events.addEventBoatTakeQuest(boat, quest)
                return True
            else:
                flash("Vous avez déjà cette quête")
    else:
        flash("Vous n'êtes pas dans un port")
    return ''

def validateQuest(questid, boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    quest = DB.request_db('sql/getQuest.sql', (questid,))[0]
    if boat['inharbour']:
        harbour =  DB.request_db('sql/getHarbourByBoat.sql', (boatid,))[0]
        boatquests =  DB.request_db('sql/getBoatQuests.sql', (boatid,))
        if questAllreadyOwned(quest['id'], boatquests):
            if iscompleteQuest(quest, boatid):
                DB.write_db('sql/removeQuestFromBoat.sql', (questid, boatid))
                DB.write_db('sql/incrementThune.sql', {'boatid':boatid, 'price':quest['award']})
                events.addEventBoatValidateQuest(boat, quest)
                return True
            else:
                flash("La quête n'est pas remplie")
        else:
            flash("Vous n'avez pas cette quête")
    else:
        flash("Vous n'êtes pas dans un port")
    return ''

def questAllreadyOwned(questid, boatquests):
    for quest in boatquests:
        if questid == quest['questid']:
            return True
    return False


def iscompleteQuest(quest, boatid):
    if quest['questtype']=='discover':
        persomap = maputils.getPersoMap(boatid)
        if quest['validation'] in persomap:
            return True
    return False

def getreasurequest(boatid):
    boat = DB.request_db('sql/getBoat.sql', (boatid,))[0]
    quest = DB.request_db('sql/getOpenTreasureQuest.sql', (boatid,))
    if boat['inharbour']:
        if quest ==[]:
            DB.write_db('sql/createTreasureQuest.sql', ( harbourwithquest['id'], "Il faut découvrir le port de "+harbour['name'], xy, 200))
        else:
            flash("Vous ne pouvez avoir qu'une quête de trésor à la fois")
            
    else:
        flash("Vous n'êtes pas dans un port")
        


def createDiscoveryQuests():
    harbours = DB.request_db('sql/getHarboursInMap.sql')
    for harbour in harbours:
        xy = str(harbour['lat'])+'x'+str(harbour['long'])
        for harbourwithquest in harbours:
            if harbourwithquest!= harbour:
                quest = DB.request_db('sql/getDiscoveryQuestByValidation.sql', (harbourwithquest['id'],xy))
                if len(quest)==0:
                    DB.write_db('sql/createDiscoveryQuest.sql', ( harbourwithquest['id'], "Il faut découvrir le port de "+harbour['name'], xy, 200))
