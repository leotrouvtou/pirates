from flask import Flask, Blueprint
from flask_login import login_required
from flask import render_template

from flask import request, make_response, session

from . import DBAccess as DB
from .auth import requires_roles
from .main import getOrCreatePageText, getBoatIfConnected

stats = Blueprint('stats', __name__)
sides = ['pirate', 'corsaire', 'neutre']



@stats.route('/stats/lastsank')
def getStatsLastSank():
    events = DB.request_db('sql/getLastSankBoat.sql')
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('lastsank', request)
    return render_template('stats.html', title = 'Derniers Coulés', boat= boat, active='lastsank', events= events, pagetext=pagetext['content'], edit=pagetext['edit'])


@stats.route('/stats/lastsignup')
def getStatsLastSignup():
    events = DB.request_db('sql/getLastSignup.sql')
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('signup', request)
    return render_template('statsUnique.html', title = 'Derniers inscrits', boat= boat, active='lastsignup', events= events, pagetext=pagetext['content'], edit=pagetext['edit'])


@stats.route('/stats/bestsanker')
def getStatsBestSanker():
    events = DB.request_db('sql/getBestSanker.sql')
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('bestsanker', request)
    return render_template('statsboats.html', title = 'Meilleurs chasseurs', boat= boat, active='bestsanker', events= events, pagetext=pagetext['content'], edit=pagetext['edit'])



@stats.route('/stats/bestsanked')
def getStatsBestSanked():
    events = DB.request_db('sql/getBestSanked.sql')
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('bestsanked', request)
    return render_template('statsboats.html', title = 'Ils ont bu la tasse', boat= boat, active='bestsanked', events= events, pagetext=pagetext['content'], edit=pagetext['edit'])


@stats.route('/stats/bestXP')
def getStatsBestXp():
    events = DB.request_db('sql/getBestXP.sql')
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('bestXP', request)
    return render_template('statsboats.html', title = 'Ça mitraille sec', boat= boat, active='bestXP', events= events, pagetext=pagetext['content'], edit=pagetext['edit'])


@stats.route('/stats/morerich')
def getStatsMoreRick():
    events = DB.request_db('sql/getMoreRich.sql')
    boat = getBoatIfConnected()
    pagetext= getOrCreatePageText('bestsanked', request)
    return render_template('statsboats.html', title = 'Les plus riches', boat= boat, active='morerich', events= events, pagetext=pagetext['content'], edit=pagetext['edit'])


@stats.route('/stats/victory')
def getStatsVictory():
    victorys=DB.request_db('sql/getVictoryList.sql')
    victorySum=DB.request_db('sql/getVictorySum.sql')
    pagetext= getOrCreatePageText('victorystats', request)
    return render_template('statsvictory.html', victorys=victorys, victorySum=victorySum, pagetext=pagetext['content'], edit=pagetext['edit'])
