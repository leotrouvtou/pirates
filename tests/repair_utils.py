def repair_boat(client):
    response = client.post("/repair")
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_repair(client, texttocheck):
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data
