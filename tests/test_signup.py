import pytest
from flask import session

def test_signup(client):
    response = client.get('/signup')
    assert response.status_code == 200

def test_signup_allready_used(client):
    response = client.post('/signup', data=dict(email='user@gmail.com', pseuso='toto', boatname='wonder of the sea', side='pirate',  password='test'), follow_redirects=True)
    # Check that the second request was to the login page.
    assert response.status_code == 200
    assert response.request.path == "/signup"
    assert b"Email d\xc3\xa9j\xc3\xa0 utilis\xc3\xa9." in response.data

def test_signup_create_user(client, auth):
    response = client.post('/signup', data=dict(email='test@gmail.com', pseuso='toto', boatname='wonder of the sea', side='pirate',  password='test'), follow_redirects=True)
    assert response.status_code == 200
    assert response.request.path == "/login"
    response = auth.login('test@gmail.com', 'test')
    assert response.status_code == 200
    assert response.request.path == "/map"
    assert b"wonder of the sea" in response.data
    assert b'<div class="xy">44</div>' in response.data
    assert b'<th background="/statics/images/background.jpg">10</th>' in response.data

    
