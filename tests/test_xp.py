import pytest
from flask import session
from .xp_utils import recruit_member, check_vigie, check_canon, check_maitre, check_voilier, check_navigateur, check_charpentier, check_failled, check_failled_not_harbour

import unittest.mock as mock

class mock_dice_succeed(object):
    @classmethod
    def randint(self, number, face):
        return 10

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_recruit_canon_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "canonier")
    check_canon(client)

def test_recruit_vigie_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "vigie")
    check_vigie(client)

def test_recruit_maitre_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "maitre")
    check_maitre(client)

def test_recruit_voilier_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "voilier")
    check_voilier(client)
    
def test_recruit_navigateur_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "navigateur")
    check_navigateur(client)
    
def test_recruit_charpentier_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "charpentier")
    check_charpentier(client)
   
def test_recruit_failed_no_XP(client, auth):
    auth.login('user@gmail.com', 'test')
    recruit_member(client, "charpentier")
    check_charpentier(client)
    recruit_member(client, "charpentier")
    check_failled(client, b'Vous n\'avez pas assez de renomm\xc3\xa9e ( il faut 200).')
    
def test_recruit_failed_not_in_harbour(client, auth):
    auth.login('admin@gmail.com', 'test')
    recruit_member(client, "charpentier")
    check_failled_not_harbour(client, b' Vous devez \xc3\xaatre dans un port\n ')
