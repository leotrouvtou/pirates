import pytest
from flask import session

def test_index(client):
    response = client.get('/')
    assert b"Se connecter" in response.data
    assert response.status_code == 200

def test_login(client):
    response = client.get('/login')
    assert response.status_code == 200

def test_signup(client):
    response = client.get('/signup')
    assert response.status_code == 200

def test_stats_last_signup(client):
    response = client.get('/stats/lastsignup')
    assert response.status_code == 200

def test_stats_last_sank(client):
    response = client.get('/stats/lastsank')
    assert response.status_code == 200

def test_stats_best_sanker(client):
    response = client.get('/stats/bestsanker')
    assert response.status_code == 200

def test_stats_best_sanker(client):
    response = client.get('/stats/bestsanker')
    assert response.status_code == 200

def test_stats_best_sanked(client):
    response = client.get('/stats/bestsanked')
    assert response.status_code == 200

def test_stats_more_rich(client):
    response = client.get('/stats/morerich')
    assert response.status_code == 200

def test_stats_best_XP(client):
    response = client.get('/stats/bestXP')
    assert response.status_code == 200


def test_help(client):
    response = client.get('/help')
    assert response.status_code == 200
    
def test_map_page__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    with client:
        response = client.get('/map')
        assert response.status_code == 200

def test_persomap__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/persomap')
    assert response.request.path == "/persomap"
    assert response.status_code == 200

def test_sidemap__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/sidemap')
    assert response.request.path == "/sidemap"
    assert response.status_code == 200

def test_messages__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/messages')
    assert response.request.path == "/messages"
    assert response.status_code == 200
    
def test_createmessage__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/createMessage')
    assert response.request.path == "/createMessage"
    assert response.status_code == 200
    
def test_sended__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/sended')
    assert response.request.path == "/sended"
    assert response.status_code == 200
    
def test_hold__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/hold')
    assert response.request.path == "/hold"
    assert response.status_code == 200

def test_quests__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/quests')
    assert response.request.path == "/quests"
    assert response.status_code == 200

def test_events__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login()
    response = client.get('/events')
    assert response.request.path == "/events"
    assert response.status_code == 200


def test_buildings__logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login('admin@gmail.com', 'test')
    response = client.get('/anim/buildings')
    assert response.request.path == "/anim/buildings"
    assert response.status_code == 200

def test_admin_harbour_logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login('admin@gmail.com', 'test')
    response = client.get('/anim/harbour/1')
    assert response.request.path == "/anim/harbour/1"
    assert response.status_code == 200

def test_admin_bourse_logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login('admin@gmail.com', 'test')
    response = client.get('/anim/bourse')
    assert response.request.path == "/anim/bourse"
    assert response.status_code == 200


def test_admin_manage_carte_logged_in(client, auth):
    assert client.get('/login').status_code == 200
    response = auth.login('admin@gmail.com', 'test')
    response = client.get('/anim/manageCarte')
    assert response.request.path == "/anim/manageCarte"
    assert response.status_code == 200
