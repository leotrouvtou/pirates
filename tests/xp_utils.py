from .canon_utils import shoot_boat
from .. import DBAccess as DB
import unittest.mock as mock


class mock_dice_succeed(object):
    @classmethod
    def randint(self, number, face):
        return 4

def recruit_member(client, member):
    response = client.post("/recruit/"+member)
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_vigie(client):
    response = client.get("/map")
    assert response.status_code == 200
    assert b'38' in response.data
    assert b'4' in response.data
    assert b'<td class="centered">20/250' in response.data
    check_tavern(client, b'<td>Vigie</td>\n                  <td>La vigie permet de voir plus loin en mer</td>\n                  <td>1</td>')

@mock.patch('random.randint', mock_dice_succeed.randint )
def check_canon(client):
    shoot_boat(client, 1)
    response = client.post("/shootByCanon/b-1")
    response = client.get("/map")
    assert response.status_code == 200
    print(response.data)
    assert b'La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (4/75) et a pris 6 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.' in response.data
    assert b'<td class="centered">24/254' in response.data
    check_tavern(client, b'<td>Canonnier</td>\n                  <td>Le canonnier permet d\'\xc3\xaatre plus pr\xc3\xa9cis lors\n                  d\'une bord\xc3\xa9e</td>\n                  <td>1</td>')

def check_maitre(client):
    response = client.get("/map")
    assert response.status_code == 200
    assert b'<td class="centered">10/11</td>' in response.data
    assert b'<td class="centered">20/250' in response.data
    check_tavern(client, b'<td>Maitre d\'\xc3\xa9quipage</td>\n                  <td>Le maitre d\'\xc3\xa9quipage permet d\'\xc3\xaatre plus efficace\n                  lors des man\xc5\x93uvres</td>\n                  <td>1</td>')

def check_tavern(client, texttocheck):
    response = client.get("/tavern")
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data

def check_voilier(client):
    response = client.get("/map")
    assert response.status_code == 200
    assert b'<td class="centered">20/250' in response.data
    check_tavern(client, b'<td>Voilier</td>\n                  <td>Un voilier permet de mieux r\xc3\xa9parer le navire</td>\n                  <td>1</td>')

def check_navigateur(client):
    response = client.get("/map")
    assert response.status_code == 200
    assert b'<td class="centered">13/16</td>' in response.data
    assert b'<td class="centered">20/250' in response.data
    check_tavern(client, b'<td>Navigateur</td>\n                  <td>Un navigateur d\'\xc3\xaatre plus efficace lors des\n                  man\xc5\x93uvres de d\xc3\xa9placement</td>\n                  <td>1</td>')

def check_charpentier(client):
    response = client.get("/map")
    assert response.status_code == 200
    assert b'<b>Points de structures :</b> 70/225' in response.data
    assert b'<td class="centered">20/250' in response.data
    check_tavern(client, b'<td>Charpentier</td>\n                  <td>Un charpentier permet de renforcer le navire\n                  pour qu\'il puisse prendre plus de boulet sans couler</td>\n                  <td>1</td>')

def check_failled_not_harbour(client, texttocheck):
    response = client.get("/tavern")
    assert response.status_code == 302
    assert b'You should be redirected automatically to target URL: <a href="/map">/map</a>. If not click the link.' in response.data
    response = client.get("/map")
    print(response.data)
    assert texttocheck in response.data
    
def check_failled(client, texttocheck):
    response = client.get("/tavern")
    assert response.status_code == 200
    assert texttocheck in response.data
