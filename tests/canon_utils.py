from .buy_utils import buy_ressources, check_here


def buyArmo(client, number):
    buy_ressources(client, 7, number)
    check_here(client, b"Vous avez achet\xc3\xa9 "+bytes(str(number), encoding='utf8')+b" tonneaux de Munition")

def shoot_boat(client, id):
    buyArmo(client, 1)
    response = client.post("/shootByCanon/b-"+str(id))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def shoot_boat_no_armo(client, id):
    response = client.post("/shootByCanon/b-"+str(id))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_shoot(client, texttocheck):
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data

def check_restes(client, texttocheck):
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data

def shoot_harbour(client, id):
    buyArmo(client, 1)
    response = client.post("/shootByCanon/h-"+str(id))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_canon(client, texttocheck):
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data
