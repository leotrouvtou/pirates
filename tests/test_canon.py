import pytest
from flask import session
from .canon_utils import shoot_boat, check_shoot, check_restes, shoot_harbour, check_canon, buyArmo, shoot_boat_no_armo
from .buy_utils import check_here
import unittest.mock as mock
from ..canon import  calculateDamage

class mock_dice_succeed(object):
    @classmethod
    def randint(self, number, face):
        return 4

class mock_dice_sink(object):
    @classmethod
    def randint(self, number, face):
        return 69
    def calculateDamage(boat, target, result):
        return 225

class mock_dice_failed(object):
    @classmethod
    def randint(self,number, face):
        return 71

class mock_dice_crit(object):
    @classmethod
    def randint(self,number, face):
        return 1

class mock_dice_failed_crit(object):
    @classmethod
    def randint(self,number, face):
        return 97

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_shoot_boat_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_boat(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (4/70) et a pris 6 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.")

@mock.patch('random.randint', mock_dice_failed.randint )
def test_shoot_boat_failed(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_boat(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 manqu\xc3\xa9e (71/70). Vous avez utilis\xc3\xa9 un tonneau de munition.")

@mock.patch('random.randint', mock_dice_crit.randint )
def test_shoot_boat_crit(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_boat(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (1/70) et a pris 3 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.")

def test_shoot_boat_too_far(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_boat(client, 2)
    check_shoot(client, b"est trop loin")

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_shoot_if_no_action_point(client, auth):
    auth.login('user@gmail.com', 'test')
    buyArmo(client,3)
    for i in range (0, 2):
        shoot_boat_no_armo(client, 1)
        check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (4/70) et a pris 6 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"vous n\'avez plus assez de point d\'action (0)")

@mock.patch('random.randint', mock_dice_failed.randint )
def test_shoot_if_no_action_point_after_failed(client, auth):
    auth.login('user@gmail.com', 'test')
    buyArmo(client,3)
    for i in range (0, 2):
        shoot_boat_no_armo(client, 1)
        check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 manqu\xc3\xa9e (71/70). Vous avez utilis\xc3\xa9 un tonneau de munition.")
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"vous n\'avez plus assez de point d\'action (0)")

@mock.patch('random.randint', mock_dice_sink.randint )
def test_shoot_and_sank(client, auth):
    auth.login('user@gmail.com', 'test')
    buyArmo(client,3)
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (69/70) et a pris 104 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"La cible admin boat a pris 104 points de structures  (69/70) et a \xc3\xa9t\xc3\xa9 coul\xc3\xa9e. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    check_restes(client, b"Restes de admin boat")


@mock.patch('random.randint', mock_dice_sink.randint )
@mock.patch('canon.calculateDamage', mock_dice_sink.calculateDamage)
def test_shoot_and_sank_and_get_object(client, auth):
    auth.login('user@gmail.com', 'test')
    buyArmo(client,3)
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (69/70) et a pris 104 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"La cible admin boat a pris 104 points de structures  (69/70) et a \xc3\xa9t\xc3\xa9 coul\xc3\xa9e. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    check_restes(client, b"Restes de admin boat")
    response = client.post('/sailTo/o')
    assert response.status_code == 200
    response = client.get('/map')
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">43x10</td>' in response.data
    assert b"Restes de admin boat" in response.data
    response = client.post('/getObject/1')
    check_here(client, b"Restes de admin boat&#34; ont \xc3\xa9t\xc3\xa9 ramass\xc3\xa9s")
    check_here(client, b"Pi\xc3\xa8ces de huit")
    check_here(client, b"admin boat")
    check_here(client, b"Pavillon")

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_shoot_habour_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_harbour(client, 1)
    check_shoot(client, b"Le port Meele Island a \xc3\xa9t\xc3\xa9 touch\xc3\xa9 et a pris 12 points de d\xc3\xa9fense (4/70). Vous avez utilis\xc3\xa9 un tonneau de munition.")

@mock.patch('random.randint', mock_dice_failed.randint )
def test_shoot_habour_failed(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_harbour(client, 1)
    check_shoot(client, b"Le port Meele Island a \xc3\xa9t\xc3\xa9 manqu\xc3\xa9e (71/70). Vous avez utilis\xc3\xa9 un tonneau de munition.")

@mock.patch('random.randint', mock_dice_failed_crit.randint )
def test_shoot_boat_failed_crit(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_boat(client, 1)
    check_shoot(client, b" La cible admin boat a \xc3\xa9t\xc3\xa9 manqu\xc3\xa9e (97/70). Un canon a explos\xc3\xa9. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    check_canon(client, b'<td class="centered">15/18</td>')

@mock.patch('random.randint', mock_dice_failed_crit.randint )
def test_shoot_habour_failed_crit(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_harbour(client, 1)
    check_shoot(client, b"Le port Meele Island a \xc3\xa9t\xc3\xa9 manqu\xc3\xa9e (97/70). Un canon a explos\xc3\xa9. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    check_canon(client, b'<td class="centered">15/18</td>')

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_shoot_if_no_armo(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_boat(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (4/70) et a pris 6 points de structures. Vous avez utilis\xc3\xa9 un tonneau de munition.")
    shoot_boat_no_armo(client, 1)
    check_shoot(client, b"La cible admin boat a \xc3\xa9t\xc3\xa9 touch\xc3\xa9e  (4/70) et a pris 2 points de structures. Vous avez charg\xc3\xa9 le canon avec des fourchettes.")
