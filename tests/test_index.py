import pytest


def test_index(client):
    response = client.get('/')
    assert b"Se connecter" in response.data
    assert response.status_code == 200
