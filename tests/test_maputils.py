import pytest
from .. import maputils
from .. import canon

def test_shoot_get_angle(client):
    angle = maputils.getAngle(3, 8, 3, 6)
    assert angle ==0
    angle = maputils.getAngle(3, 8, 5, 6)
    assert angle ==45
    angle = maputils.getAngle(3, 8, 4, 7)
    assert angle ==45
    angle = maputils.getAngle(3, 8, 5, 8)
    assert angle ==90
    angle = maputils.getAngle(3, 8, 4, 8)
    assert angle ==90
    angle = maputils.getAngle(3, 8, 5, 10)
    assert angle ==135
    angle = maputils.getAngle(3, 8, 4, 9)
    assert angle ==135
    angle = maputils.getAngle(3, 8, 3, 10)
    assert angle ==180
    angle = maputils.getAngle(3, 8, 3, 9)
    assert angle ==180
    angle = maputils.getAngle(3, 8, 1, 10)
    assert angle ==-135
    angle = maputils.getAngle(3, 8, 2, 9)
    assert angle ==-135
    angle = maputils.getAngle(3, 8, 1, 8)
    assert angle ==-90
    angle = maputils.getAngle(3, 8, 2, 8)
    assert angle ==-90
    angle = maputils.getAngle(3, 8, 1, 6)
    assert angle ==-45
    angle = maputils.getAngle(3, 8, 2, 7)
    assert angle ==-45
    angle = maputils.getAngle(3, 8, 2,6)
    assert angle ==-26.565051177077994
    angle = maputils.getAngle(3, 8, 4,6)
    assert angle ==26.565051177077994
    angle = maputils.getAngle(3, 8, 1,7)
    assert angle ==-63.43494882292201
    angle = maputils.getAngle(3, 8, 5,7)
    assert angle ==63.43494882292201
    angle = maputils.getAngle(3, 8, 5,9)
    assert angle ==116.56505117707799
    angle = maputils.getAngle(3, 8, 1,9)
    assert angle ==-116.56505117707799
    angle = maputils.getAngle(3, 8, 4,10)
    assert angle ==153.434948822922
    angle = maputils.getAngle(3, 8, 2,10)
    assert angle ==-153.434948822922

def test_shoot_get_diff(client):
    diff =canon.calculateDiff(0, 0)
    assert diff ==90
    diff =canon.calculateDiff(0, 45)
    assert diff ==45
    diff =canon.calculateDiff(0, 90)
    assert diff ==0
    diff =canon.calculateDiff(0, 135)
    assert diff ==45
    diff =canon.calculateDiff(0, 180)
    assert diff ==90
    diff =canon.calculateDiff(0, -135)
    assert diff ==45
    diff =canon.calculateDiff(0, -90)
    assert diff ==0
    diff =canon.calculateDiff(0, -45)
    assert diff ==45
    diff =canon.calculateDiff(45, 0)
    assert diff ==45
    diff =canon.calculateDiff(45, 45)
    assert diff ==90
    diff =canon.calculateDiff(45, 135)
    assert diff ==0
    diff =canon.calculateDiff(45, 180)
    assert diff ==45
    diff =canon.calculateDiff(45, -135)
    assert diff ==90
    diff =canon.calculateDiff(45, -90)
    assert diff ==45
    diff =canon.calculateDiff(45, -45)
    assert diff ==0
    diff =canon.calculateDiff(90, 0)
    assert diff ==0
    diff =canon.calculateDiff(90, 45)
    assert diff ==45
    diff =canon.calculateDiff(90, 90)
    assert diff ==90
    diff =canon.calculateDiff(90, 135)
    assert diff ==45
    diff =canon.calculateDiff(90, 180)
    assert diff ==0
    diff =canon.calculateDiff(90, -135)
    assert diff ==45
    diff =canon.calculateDiff(90, -90)
    assert diff ==90
    diff =canon.calculateDiff(90, -45)
    assert diff ==45
    diff =canon.calculateDiff(135, 0)
    assert diff ==45
    diff =canon.calculateDiff(135, 45)
    assert diff ==0
    diff =canon.calculateDiff(135, 90)
    assert diff ==45
    diff =canon.calculateDiff(135, 135)
    assert diff ==90
    diff =canon.calculateDiff(135, 180)
    assert diff ==45
    diff =canon.calculateDiff(135, -135)
    assert diff ==0
    diff =canon.calculateDiff(135, -90)
    assert diff ==45
    diff =canon.calculateDiff(135, -45)
    assert diff ==90
    diff =canon.calculateDiff(180, 0)
    assert diff ==90
    diff =canon.calculateDiff(180, 45)
    assert diff ==45
    diff =canon.calculateDiff(180, 90)
    assert diff ==0
    diff =canon.calculateDiff(180, 135)
    assert diff ==45
    diff =canon.calculateDiff(180, 180)
    assert diff ==90
    diff =canon.calculateDiff(180, -135)
    assert diff ==45
    diff =canon.calculateDiff(180, -90)
    assert diff ==0
    diff =canon.calculateDiff(180, -45)
    assert diff ==45

    diff =canon.calculateDiff(-45, 0)
    assert diff ==45
    diff =canon.calculateDiff(-45, 45)
    assert diff ==0
    diff =canon.calculateDiff(-45, 135)
    assert diff ==90
    diff =canon.calculateDiff(-45, 180)
    assert diff ==45
    diff =canon.calculateDiff(-45, -135)
    assert diff ==0
    diff =canon.calculateDiff(-45, -90)
    assert diff ==45
    diff =canon.calculateDiff(-45, -45)
    assert diff ==90
    diff =canon.calculateDiff(-90, 0)
    assert diff ==0
    diff =canon.calculateDiff(-90, 45)
    assert diff ==45
    diff =canon.calculateDiff(-90, 90)
    assert diff ==90
    diff =canon.calculateDiff(-90, 135)
    assert diff ==45
    diff =canon.calculateDiff(-90, 180)
    assert diff ==0
    diff =canon.calculateDiff(-90, -135)
    assert diff ==45
    diff =canon.calculateDiff(-90, -90)
    assert diff ==90
    diff =canon.calculateDiff(-90, -45)
    assert diff ==45
    diff =canon.calculateDiff(-135, 0)
    assert diff ==45
    diff =canon.calculateDiff(-135, 45)
    assert diff ==90
    diff =canon.calculateDiff(-135, 90)
    assert diff ==45
    diff =canon.calculateDiff(-135, 135)
    assert diff ==0
    diff =canon.calculateDiff(-135, 180)
    assert diff ==45
    diff =canon.calculateDiff(-135, -135)
    assert diff ==90
    diff =canon.calculateDiff(-135, -90)
    assert diff ==45
    diff =canon.calculateDiff(-135, -45)
    assert diff ==0
