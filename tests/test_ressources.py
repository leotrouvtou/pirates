import pytest
from flask import session
from .buy_utils import buy_ressources, check_here, sell_ressources, check_not_here, load_people, unload_people, drinks_on_me, check_events, farm_ressource, check_farm

import unittest.mock as mock

class mock_dice(object):
    @classmethod
    def randint(self, number, face):
        return 4

nextid=8

def test_buy_ressource_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    buy_ressources(client, 1, 12)
    check_here(client, b"Vous avez achet\xc3\xa9 12 tonneaux de Nourriture")
    check_here(client, b"12 /50")

def test_buy_ressource_too_much(client, auth):
    auth.login('user@gmail.com', 'test')
    buy_ressources(client, 1, 70)
    check_here(client, b"Vous voulez acheter plus qu&#39;il n&#39;y en a")
    check_here(client, b"0 /50")

def test_buy_ressource_no_action_point(client, auth):
    auth.login('user@gmail.com', 'test')
    for i in range (0, 5):
        buy_ressources(client, 1, 3)
        check_here(client, b"Vous avez achet\xc3\xa9 3 tonneaux de Nourriture")
    buy_ressources(client, 1, 3)
    check_here(client, b"Vous n&#39;avez pas assez de point d&#39;action(0)")
    check_here(client, b"15 /50")

def test_buy_ressource_no_money(client, auth):
    auth.login('user@gmail.com', 'test')
    buy_ressources(client, 1, 49)
    check_here(client, b"Vous n&#39;avez pas assez de pi\xc3\xa8ces de huit")
    check_here(client, b"0 /50")

def test_sell_ressource_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    buy_ressources(client, 1, 12)
    check_here(client, b"Vous avez achet\xc3\xa9 12 tonneaux de Nourriture")
    check_here(client, b"12 /50")
    sell_ressources(client, nextid, 12)
    check_here(client, b"Vous avez vendu 12 tonneaux de Nourriture")
    check_here(client, b"0 /50")

def test_sell_ressource_fail(client, auth):
    auth.login('user@gmail.com', 'test')
    buy_ressources(client, 1, 12)
    check_here(client, b"Nourriture")
    sell_ressources(client, nextid, 14)
    check_here(client, b"Vous voulez vendre plus qu&#39;il n&#39;y en a")
    check_here(client, b"12 /50")


def test_buy_ressource_not_in_harbour(client, auth):
    auth.login('anim@gmail.com', 'test')
    buy_ressources(client, 1, 12)
    check_not_here(client, b"Nourriture")
    check_here(client, b"0 /50")


def test_load_people_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    load_people(client, 1, 12)
    check_here(client, b"Vous avez embarqu\xc3\xa9 12 pirate")
    check_here(client, b"12 /50")

def test_load_people_too_much(client, auth):
    auth.login('user@gmail.com', 'test')
    load_people(client, 1, 600)
    check_here(client, b"Vous voulez embarquer trop de gens (il doit rester 80% de la pop du port complet : 800.0).")
    check_here(client, b"0 /50")

def test_unload_people_succeed(client, auth):
    auth.login('user@gmail.com', 'test')
    load_people(client, 1, 12)
    check_here(client, b"Vous avez embarqu\xc3\xa9 12 pirates")
    check_here(client, b"12 /50")
    unload_people(client, nextid, 12)
    check_here(client, b"Vous avez debarqu\xc3\xa9 12 pirates")
    check_here(client, b"0 /50")
    
def test_unload_people_fail_too_much(client, auth):
    auth.login('user@gmail.com', 'test')
    load_people(client, 1, 12)
    check_here(client, b"Vous avez embarqu\xc3\xa9 12 pirates")
    check_here(client, b"12 /50")
    unload_people(client, nextid, 67)
    check_here(client, b"Vous voulez debarquer plus de gens que vous en avez.")
    check_here(client, b"12 /50")

def test_drinks_on_me(client, auth):
    auth.login('user@gmail.com', 'test')
    drinks_on_me(client, 19)
    check_here(client, b"Vous avez pay\xc3\xa9 une tourn\xc3\xa9e de 19 verres")
    check_events(client, "h1",  b"1 colon a chang\xc3\xa9 d\'all\xc3\xa9geance (pirate)")

def test_drinks_on_me_no_money(client, auth):
    auth.login('user@gmail.com', 'test')
    drinks_on_me(client, 220)
    check_here(client, b"Vous n&#39;avez pas assez de pi\xc3\xa8ces de huit")

def test_drinks_on_me_no_action_point(client, auth):
    auth.login('user@gmail.com', 'test')
    for i in range (0, 5):
        drinks_on_me(client, 19)
        check_here(client, b"Vous avez pay\xc3\xa9 une tourn\xc3\xa9e de 19 verres")
    drinks_on_me(client, 19)
    check_here(client, b"Vous n&#39;avez pas assez de point d&#39;action(0)")

@mock.patch('random.randint', mock_dice.randint )
def test_farmwood_success(client, auth):
    auth.login('anim@gmail.com', 'test')
    farm_ressource(client, '40x10', 'Bois')
    check_farm(client, b'Vous avez remplis 9 tonneaux de Bois')
    check_farm(client, b'5/10')
    check_here(client, b"9 /50")

@mock.patch('random.randint', mock_dice.randint )
def test_farmfood_success(client, auth):
    auth.login('anim@gmail.com', 'test')
    farm_ressource(client, '40x10', 'Nourriture/Eau')
    check_farm(client, b'Vous avez remplis 9 tonneaux de Nourriture/Eau')
    check_farm(client, b'5/10')
    check_here(client, b"9 /50")

@mock.patch('random.randint', mock_dice.randint )
def test_farmfood_fail_not_creek(client, auth):
    auth.login('admin@gmail.com', 'test')
    farm_ressource(client, '41x10', 'Nourriture/Eau')
    check_farm(client, b'Il faut \xc3\xaatre dans une crique')
    check_here(client, b"0 /50")

@mock.patch('random.randint', mock_dice.randint )
def test_farmfood_fail_too_far(client, auth):
    auth.login('admin@gmail.com', 'test')
    farm_ressource(client, '40x10', 'Nourriture/Eau')
    check_farm(client, b'C\'est trop loin')
    check_here(client, b"0 /50")
