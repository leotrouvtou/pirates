import pytest
from flask import session
from .repair_utils import repair_boat, check_repair
from .canon_utils import shoot_harbour
from .buy_utils import buy_ressources, check_here, check_not_here
import unittest.mock as mock

class mock_dice_succeed(object):
    @classmethod
    def randint(self, number, face):
        return 10
class mock_dice_succeed_full(object):
    @classmethod
    def randint(self, number, face):
        return 100

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_repair_boat_succeed_harbour(client, auth):
    auth.login('user@gmail.com', 'test')
    repair_boat(client)
    check_repair(client, b"Vous avez r\xc3\xa9par\xc3\xa9 le bateau de 80 (Chantier Naval) points de structure (150/200)")
    check_repair(client, b'<td class="centered">8/10</td>')

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_repair_boat_succeed_harbour_not_active(client, auth):
    auth.login('user@gmail.com', 'test')
    shoot_harbour(client, 1)
    check_repair(client, b"Le port Meele Island a \xc3\xa9t\xc3\xa9 touch\xc3\xa9 et a pris 30 points de d\xc3\xa9fense (10/70)")
    repair_boat(client)
    check_repair(client, b"Vous avez r\xc3\xa9par\xc3\xa9 le bateau de 10 points de structure (50/200)")
    check_repair(client, b'<td class="centered">0/10</td>')

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_repair_boat_succeed_with_wood(client, auth):
    auth.login('user@gmail.com', 'test')
    buy_ressources(client, 4, 1)
    check_here(client, b"Vous avez achet\xc3\xa9 1 tonneaux de Bois")
    response = client.post('/sailTo/so')
    repair_boat(client)
    check_repair(client, b"Vous avez r\xc3\xa9par\xc3\xa9 le bateau de 40 (vous avez utilis\xc3\xa9 une ressource bois) points de structure (110/200)")
    check_not_here(client, b"Bois")
    check_repair(client, b'<td class="centered">4/10</td>')

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_repair_boat_succeed_no_wood(client, auth):
    auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/so')
    repair_boat(client)
    check_repair(client, b"Vous avez r\xc3\xa9par\xc3\xa9 le bateau de 10 points de structure (80/200)")
    check_not_here(client, b"Bois")
    check_repair(client, b'<td class="centered">6/10</td>')

@mock.patch('random.randint', mock_dice_succeed.randint )
def test_repair_boat_fail_no_action(client, auth):
    auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/so')
    for i in range (0, 2):
        repair_boat(client)
        check_repair(client, b"Vous avez r\xc3\xa9par\xc3\xa9 le bateau de 10 points de structure")
    repair_boat(client)
    check_repair(client, b"Vous n\'avez pas assez de point d\'action(2)")

@mock.patch('random.randint', mock_dice_succeed_full.randint )
def test_repair_boat_fail_full(client, auth):
    auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/so')
    for i in range (0, 2):
        repair_boat(client)
        check_repair(client, b"Vous avez r\xc3\xa9par\xc3\xa9 le bateau de 100 points de structure")
    repair_boat(client)
    check_repair(client, b"Vous n\'avez pas assez de point d\'action(2)")
