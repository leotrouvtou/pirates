def make_round_and_check(client, direction, angle, pm):
    response = client.post('/makeRound', data=dict(angle=angle))
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">43x10</td>' in response.data
    assert b'<span class="boathalo piratehalo"></span><img class="boat" src="/statics/images/fregate.png" style="transform: rotate('+bytes(str(direction), 'utf-8')+b'deg);"/><span class="tooltip notland"><a href="/events/b3">user boat</a>' in response.data
    assert bytes(str(pm), 'utf-8')+b'/15' in response.data
