import pytest
from flask import session
from . import sailutils

def test_sail_land_aigue(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/n')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" in response.data
    assert b'<td class="coord" colspan="4">44x10</td>' in response.data


def test_sail_land_obtu(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/s')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" in response.data
    assert b'<td class="coord" colspan="4">44x10</td>' in response.data

def test_sail_land_corail(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/ne')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">45x9</td>' in response.data


def test_sail_land_landdroit(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/e')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" in response.data
    assert b'<td class="coord" colspan="4">44x10</td>' in response.data

def test_sail_land_bancsable(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/se')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" in response.data
    assert b'<td class="coord" colspan="4">44x10</td>' in response.data


def test_sail_land_deepsea(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/so')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">43x11</td>' in response.data

def test_sail_land_shallowsea(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/o')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">43x10</td>' in response.data

def test_sail_land_lava(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/no')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">43x9</td>' in response.data

def test_sail_make_round(client, auth):
    response = auth.login('user@gmail.com', 'test')
    response = client.post('/sailTo/o')
    assert response.status_code == 200
    assert b'{"success": true}' == response.data
    response = client.get('/map')
    assert response.status_code == 200
    assert b"Un bateau ne va pas sur terre" not in response.data
    assert b'<td class="coord" colspan="4">43x10</td>' in response.data
    assert b'<span class="boathalo piratehalo"></span><img class="boat" src="/statics/images/fregate.png" style="transform: rotate(-90deg);"/>' in response.data
    sailutils.make_round_and_check(client, 45, 'ne', 11)
    sailutils.make_round_and_check(client, 90, 'e', 10)
    sailutils.make_round_and_check(client, 135, 'se', 9)
    sailutils.make_round_and_check(client, 180, 's', 8)
    sailutils.make_round_and_check(client, -135, 'so', 7)
    sailutils.make_round_and_check(client, -90, 'o', 6)
    sailutils.make_round_and_check(client, -45, 'no', 5)
    sailutils.make_round_and_check(client, 0, 'n', 4)

