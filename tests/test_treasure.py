import pytest
from flask import session
from .treasure_utils import give_money, check_here, check_events, check_not_events


def test_give_money_succeed(client, auth):
    auth.login('admin@gmail.com', 'test')
    give_money(client, 2, 12)
    check_here(client, b"Vous avez donn\xc3\xa9 12 pi\xc3\xa8ces de huit \xc3\xa0 anim boat")
    check_here(client, b"6/10")
    check_here(client, b"188")
    check_events(client, b'Vous avez donn\xc3\xa9 12 pi\xc3\xa8ces de huit \xc3\xa0 anim boat')
    auth.login('anim@gmail.com', 'test')
    check_events(client, b'admin boat vous a donn\xc3\xa9 12 pi\xc3\xa8ces de huit')

def test_give_money_too_far_failed(client, auth):
    auth.login('admin@gmail.com', 'test')
    give_money(client, 3, 12)
    check_here(client, b"Ce bateau est trop loin")
    check_here(client, b"8/10")
    check_here(client, b'<td class="centered">200</td>')
    check_not_events(client, b'Vous avez donn\xc3\xa9 12 pi\xc3\xa8ces de huit \xc3\xa0 user boat')
    auth.login('user@gmail.com', 'test')
    check_not_events(client, b'admin boat vous a donn\xc3\xa9 12 pi\xc3\xa8ces de huit')

def test_give_money_too_much_failed(client, auth):
    auth.login('admin@gmail.com', 'test')
    give_money(client, 2, 201)
    check_here(client, b"Vous n\'avez pas assez de pi\xc3\xa8ces de huit")
    check_here(client, b"8/10")
    check_here(client, b'<td class="centered">200</td>')
    check_not_events(client, b'Vous avez donn\xc3\xa9 201 pi\xc3\xa8ces de huit \xc3\xa0 anim boat')
    auth.login('anim@gmail.com', 'test')
    check_not_events(client, b'admin boat vous a donn\xc3\xa9 201 pi\xc3\xa8ces de huit')
