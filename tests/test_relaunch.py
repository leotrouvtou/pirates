import pytest
from flask import session


def test_new_turn_no_food_full(client, auth):
    auth.login('admin@gmail.com', 'test')
    response = client.get('/map')
    assert response.status_code == 200
    assert b'15/15' in response.data
    assert b'8/10' in response.data
    response = client.get('/events')
    assert response.status_code == 200
    assert b'Vous avez d\xc3\xa9clanch\xc3\xa9 un nouveau tour. L\'\xc3\xa9quipage a faim et soif et travaille moins.' in response.data

def test_new_turn_no_food_very_hurted(client, auth):
    auth.login('anim@gmail.com', 'test')
    response = client.get('/map')
    assert response.status_code == 200
    assert b'9/15' in response.data
    assert b'10/10' in response.data
    response = client.get('/events')
    assert response.status_code == 200
    print(response.data)
    assert b'Vous avez d\xc3\xa9clanch\xc3\xa9 un nouveau tour. Le bateau est endomag\xc3\xa9 et avance moins, (-6). Vous avez utilis\xc3\xa9 un tonneau de Nourriture/Eau.' in response.data

def test_new_turn_food_hurted(client, auth):
    auth.login('user@gmail.com', 'test')
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert b'13/15' in response.data
    assert b'10/10' in response.data
    response = client.get('/events')
    assert response.status_code == 200
    print(response.data)
    assert b'Vous avez d\xc3\xa9clanch\xc3\xa9 un nouveau tour. Le bateau est endomag\xc3\xa9 et avance moins, (-2). Dans un port vous ne consommez pas de ressources.' in response.data
