import pytest
from flask import session
from . import messages_utils

def test_createmessage_to_boat__logged_in(client, auth):
    response = auth.login('user@gmail.com', 'test')
    messages_utils.send_message(client, '2', 'Corsaire en vue !', 'Je vous attends pour ouvrir le feu')
    messages_utils.check_message_sended(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'anim boat (corsaire)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    response = auth.login('anim@gmail.com', 'test')
    messages_utils.check_message_received(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'anim boat (corsaire)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')

def test_createmessage_to_side__logged_in(client, auth):
    response = auth.login('user@gmail.com', 'test')
    messages_utils.send_message(client, 'pirates', 'Corsaire en vue !', 'Je vous attends pour ouvrir le feu')
    messages_utils.check_message_sended(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'admin boat (pirate)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    response = auth.login('admin@gmail.com', 'test')
    messages_utils.check_message_received(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'admin boat (pirate)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    response = auth.login('anim@gmail.com', 'test')
    messages_utils.check_message_not_received(client, b'Corsaire en vue !')
    messages_utils.check_message_not_readable(client, 1)

def test_reply_to_boat__logged_in(client, auth):
    response = auth.login('user@gmail.com', 'test')
    messages_utils.send_message(client, '2', 'Corsaire en vue !', 'Je vous attends pour ouvrir le feu')
    messages_utils.check_message_sended(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'anim boat (corsaire)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    response = auth.login('anim@gmail.com', 'test')
    messages_utils.check_message_received(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'anim boat (corsaire)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    messages_utils.reply_message(client, 1, b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')


def test_reply_to_side__logged_in(client, auth):
    response = auth.login('user@gmail.com', 'test')
    messages_utils.send_message(client, 'pirates', 'Corsaire en vue !', 'Je vous attends pour ouvrir le feu')
    messages_utils.check_message_sended(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'admin boat (pirate)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    response = auth.login('admin@gmail.com', 'test')
    messages_utils.check_message_received(client, b'Corsaire en vue !')
    messages_utils.check_message_content(client, 1, b'admin boat (pirate)', b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
    messages_utils.reply_message(client, 1, b'Corsaire en vue !', b'Je vous attends pour ouvrir le feu')
