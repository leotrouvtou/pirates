def buy_ressources(client, id, amount):
    response = client.post("/buyRessources/"+str(id),data=dict(amount=amount))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def sell_ressources(client, id, amount):
    response = client.post("/sellRessources/"+str(id),data=dict(amount=amount))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_here(client, texttocheck):
    response = client.get('/hold')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data

def check_not_here(client, texttocheck):
    response = client.get('/hold')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck not in response.data

def load_people(client, id, amount):
    response = client.post("/loadPeople/"+str(id),data=dict(amount=amount))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def unload_people(client, id, amount):
    response = client.post("/unLoadPeople/"+str(id),data=dict(amount=amount))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def drinks_on_me(client, amount):
    response = client.post("/drinksOnMe",data=dict(amount=amount))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_events(client, ressource,  texttocheck):
    response = client.get('/events/'+ressource)
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data

def farm_ressource(client, coord, ressourceName):
    response = client.post("/farmRessources/"+coord,data=dict(ressourceName=ressourceName))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_farm(client,  texttocheck):
    response = client.get('/map')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data
