def send_message(client, toids, subject, content):
    response = client.post('/sendMessage', data=dict(subject=subject, toids=toids, content=content))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def reply_message(client, id, subject, content):
    response = client.get('/reply/'+str(id))
    assert response.status_code == 200
    assert subject in response.data
    assert content in response.data

def check_message_sended(client, texttocheck):
    response = client.get('/sended')
    assert response.status_code == 200
    assert texttocheck in response.data

def check_message_content(client, messageid, boatname, subject, content):
    response = client.get('/message/'+str(messageid))
    assert response.status_code == 200
    assert boatname in response.data
    assert subject in response.data
    assert content in response.data

def check_message_not_readable(client, messageid):
    response = client.get('/message/'+str(messageid))
    assert response.status_code == 302
    response = client.get('/messages')
    assert b"Ce message n&#39;est pas pour vous" in response.data

def check_message_received(client, texttocheck):
    response = client.get('/messages')
    assert response.status_code == 200
    assert texttocheck in response.data

def check_message_not_received(client, texttocheck):
    response = client.get('/messages')
    assert response.status_code == 200
    assert texttocheck not in response.data
