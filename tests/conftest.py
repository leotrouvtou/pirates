import pytest
from .. import create_app
import shutil
import os

@pytest.fixture()
def app():
    app = create_app()
    shutil.copyfile('tests.db', 'temptests.db')
    app.config.update({
        "TESTING": True,
        "db": "temptests.db",
        "env": "pytest",
        "FLASK_DEBUG": 1
    })

    # other setup can go here

    yield app

    os.unlink(app.config['db'])
    # clean up / reset resources here


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()

class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, email='user@gmail.com', password='test'):
        return self._client.post(
            '/login',
            data=dict(email=email, password=password),
            follow_redirects=True
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def auth(client):
    return AuthActions(client)
