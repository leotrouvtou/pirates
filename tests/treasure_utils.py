def give_money(client, targetid, amount):
    response = client.post("/giveMoney/"+str(targetid),data=dict(amount=amount))
    assert response.status_code == 200
    assert b'{"success": true}' in response.data

def check_here(client, texttocheck):
    response = client.get('/map')
    assert response.status_code == 200
    assert texttocheck in response.data

def check_events(client, texttocheck):
    response = client.get('/events')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck in response.data

def check_not_events(client, texttocheck):
    response = client.get('/events')
    assert response.status_code == 200
    print(response.data)
    assert texttocheck not in response.data
